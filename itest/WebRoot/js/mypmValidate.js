	function $(o){
	  return document.getElementById(o);
	}
	
	function $2(o){
	  return document.getElementsByName(o);
	}

function isWhitespace(value)
{
  return !( /\S/.test(value) );
}

function isDigit(value, allowEmpty)
{
  if (allowEmpty && value.length == 0){
    return true;
    }
    
   if (!allowEmpty && value == "")
    return false;
    
  return /^\d+$/.test(value);
}

function isNumber(value, allowEmpty)
{
  if (allowEmpty == true && value.length == 0)
    return true;

  return /^[-+]?\d+$/.test(value);
}

function isFloat(value, allowEmpty)
{   
   if (allowEmpty == true && value.length == 0)
    return false;
   var strPattern = "^(([+]?|[-]?)[0-9]+([.][0-9]+)?)?$";
   var aryResult = value.match(strPattern);
   if (aryResult == null) {
     return false;
   } else {
     return true;
   }
}

function isDate(value, allowEmpty)
{
  if (allowEmpty == true && value.length == 0)
    return true;

  return /^\d{4}[-]\d{2}[-]\d{2}$/.test(value); 
}

function createDate(sd) {
  if (sd == null)
    return null;
  var ymd = sd.split("-");
  return new Date(ymd[0], ymd[1]-1, ymd[2]);
}

function addDays(date, dd) {   
  date.setDate(date.getDate() + dd);   
}

function daysElapsed(d1, d2){
  var ds = 0;
  if (d1 == "" || d2 == "")
    return ds;
  try {
    var ymd = d1.split("-");
    var date1 = new Date(ymd[0], ymd[1]-1, ymd[2]);
    ymd = d2.split("-");
    var date2 = new Date(ymd[0], ymd[1]-1, ymd[2]);
    ds = parseInt((date2 - date1) / 86400000);
  } catch (e) {
    ds = 0;
  }
  return ds;
} 

function formatDateYMD(d) {
  	var year = d.getFullYear();
  	var month = d.getMonth() + 1;
  	month = (month < 10)? '0' + month : month;
  	var dayOfMonth = d.getDate();
  	dayOfMonth = (dayOfMonth < 10)? '0' + dayOfMonth : dayOfMonth;
  	return (year + '-' + month + '-' + dayOfMonth);
}

function isPhoneCode(value, allowEmpty){
  if (allowEmpty == true && value.length == 0)
    return true;
  if (allowEmpty == true && value.length == 0)
    return true;
  return /^((\(\+?\d+\)|\+?\d+)[ -]?)?((\(\d+\)|\d+)[ -]?)*\d+([ -]?(\(\d+\)|\d+))*$/.test(value); 
}

function isEmailAddress(value, allowEmpty)
{
  var strText = new String(value);
  if (allowEmpty == true && value.length == 0) {
    return true;
  }
  var strPattern = /^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/;
  var strPatt = /[^a-zA-Z0-9-@\._]/;
  if (strText.match(strPatt)) {
     return false;
  }
  aryResult = strText.match(strPattern);
  if(aryResult == null) {
  return false;
  } else {
  return true;
 }
}

function isHttpUrl(value, allowEmpty)
{
  if (allowEmpty == true && value.length == 0)
    return true;

  return /^http:\/\/[\w-.]+(:\d+)?(\/([\w;:@&=%$-.+!*\'(),~]+\/)*[\w;:@&=%$-.+!*\'(),~]*(\?[\w;:@&=%$-.+!*\'(),]*)?)?$/i.test(value);  // RFC 1738 - "http://" host [ ":" port ] [ "/" *[ hsegment "/" ] [ hsegment ] [ "?" search ] ]
}

function isJavaClass(value, allowEmpty)
{
  if (allowEmpty == true && value.length == 0)
    return true;

  return /^[A-Za-z]\w*(\.[A-Za-z]\w*)*$/.test(value);
}

function isLoginId(value, allowEmpty)
{
  if (allowEmpty == true && value.length == 0)
    return true;

  return /^[\w\-\.]+$/.test(value);
}

function trim(string)
{
  return string.replace(/^\s+/, '').replace(/\s+$/, '');
}

function replaceAll(string, substr, alias)
{
  var result = '';

  var last = 0, index = string.indexOf(substr);

  while (index >= 0)
  {
    result += string.substring(last, index) + alias;

    last = index + substr.length; 

    index = string.indexOf(substr, last);
  }

  result += string.substring(last, string.length);

  return result;
}
 
function checkDateExist(day, month, year) {
  var today=new Date();
  year=((!year) ? y2k(today.getYear()):year);
  month=((!month) ? today.getMonth():month-1);
  if(!day){
   return false;
  }
  var test=new Date(year,month,day);
  if((y2k(test.getYear()) == year) && (month == test.getMonth()) && (day == test.getDate())) {
    return true;
  } else {
    return false;
  }
}

function checkDate(obj){
  var dates=trim($(obj).value);
  if (dates == null)
    return false;
  var datePattern=/^\d{4}\-(0[1-9]|1[0-2])\-(3[0-1]|[0-2][0-9])$/;
  if((dates.match(datePattern) && checkDateExist(dates.substring(8, 10), dates.substring(5, 7), dates.substring(0, 4)))) {
    return true;
  }
  return false;
}

function isValidDateString(date){
  var dates = trim($(date).value);
  if (dates == "") {
    return true;
   }
  if (dates.length < 8) 
    return false;
  var datePattern = /^\d{4}(0[1-9]|1[0-2])(3[0-1]|[0-2][0-9])$/;
  if (dates.indexOf("-") > 0) {
    datePattern = /^\d{4}-(0[1-9]|1[0-2])-(3[0-1]|[0-2][0-9])$/;
  } else if (dates.indexOf("/") > 0) {
    if (dates.length == 8)
      dates = "20" + dates;
    datePattern = /^\d{4}\/(0[1-9]|1[0-2])\/(3[0-1]|[0-2][0-9])$/;
  }
  if (dates.match(datePattern)) {
    if ((dates.length == 8 && checkDateExist(dates.substring(6, 8), dates.substring(4, 6), dates.substring(0, 4)))
        || (dates.length == 10 && checkDateExist(dates.substring(8, 10), dates.substring(5, 7), dates.substring(0, 4)))) {
      return true;
    }
  }
  return false;
}

function getTidyDateString(ts){
  var datePattern = /^\d{4}(0[1-9]|1[0-2])(3[0-1]|[0-2][0-9])$/;
  if (ts.match(datePattern)) {
    return ts;
  }
  if (ts.indexOf("-") > 0) {
    datePattern = /^\d{4}-(0[1-9]|1[0-2])-(3[0-1]|[0-2][0-9])$/;
  } else if (ts.indexOf("/") > 0) {
    if (ts.length == 8)
      ts = "20" + ts;
    datePattern = /^\d{4}\/(0[1-9]|1[0-2])\/(3[0-1]|[0-2][0-9])$/;
  }
  if (ts.match(datePattern)) {
    return new Date(ts.substring(0, 4), (ts.substring(5, 7) - 1), ts.substring(8, 10)).format("yyyyMMdd");
  }  else {
    return "";
  }
  
  function getisDigit(number){
  	var c = Math.floor(number);
  	var a = c * 1000;
  	var b = number * 1000;
  	if(a == b)
  		return c;
  	else
  		return number;
  }
}