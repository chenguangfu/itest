	function regBarDefEvent(BarObj,GridObj,dtoName,cusUrl){
		var tBar = pmBar;
		var tGrid = pmGrid;
		if(typeof BarObj != "undefined"&&BarObj!=""){
			tBar = BarObj;
		}
		if(typeof GridObj != "undefined"&&GridObj!=""){
			tGrid = GridObj;
		}
		tBar.attachEvent("onValueChange", function(id, value) {
			if(pageNo == value){
				return;
			}
			pageNo = value;
			pageAction2(tGrid,dtoName,cusUrl);
		});
		tBar.attachEvent("onEnter", function(id, value) {
			if(!isDigit(value, false)){
				tBar.setValue("page", pageNo);
				return;
			}
			if(value==pageNo){
				return;
			}
			var pageNoTemp = parseInt(value);
			if(pageNoTemp < 1){
				pageNoTemp = 1;
			}else if(pageNoTemp > pageCount){
				pageNoTemp = pageCount;
			}
			pageNo = pageNoTemp;
			pageAction2(tGrid,dtoName,cusUrl);
		});
		tBar.attachEvent("onClick", function(id) {
			if(id == 'all'){
				checkAll(pmGrid);
			}else if(id == "filter"){
				var hd_st = pmGrid.hdr.rows[2].style.display;
				if(hd_st == ""){
					tGrid.hdr.rows[2].style.display = "none";
				}else{ 
					tGrid.hdr.rows[2].style.display = "";
				}
				tGrid.setSizes();
			}else if(id == "first"){
				pageNo = 1;
				pageAction2(tGrid,dtoName,cusUrl);
			}else if(id == "last"){
				pageNo = pageCount;
				pageAction2(tGrid,dtoName,cusUrl);
			}else if(id == "next"){
				pageNo = pageNo +1
				pageAction2(tGrid,dtoName,cusUrl);
			}else if(id == "pervious"){
				pageNo = pageNo -1
				pageAction2(tGrid,dtoName,cusUrl);
			}else if(id == "id1" || id == "id2" || id == "id3" || id == "id4" || id == "id5"){
				var pageSizeTemp = parseInt(pmBar.getListOptionText("pageP", id));
				if(pageSize==pageSizeTemp){
					return;
				}
				pageSize = pageSizeTemp;
				pageAction2(tGrid,dtoName,cusUrl);
			}
		});
	}
	function pageAction2(GridObj,dtoName,cusUrl){
		var url = pageBreakUrl
		var dto = pAdto;
		var currGrid = pmGrid;
		if(typeof cusUrl != "undefined" && cusUrl!=""){
			url = cusUrl;
		}else{
			if(typeof dtoName != "undefined"&&dtoName!=""){
				dto = dtoName;
			}
			if(url.indexOf(".isAjax=true")<0){
				url = url +"?"+dto+".isAjax=true";
			}
			url = url + "&"+dto+".pageNo="+ pageNo +"&"+dto+".pageSize=" + pageSize;		
		}
		if(typeof GridObj != "undefined"){
			currGrid = GridObj;
		}
		var rest = dhtmlxAjax.postSync(url, pageBreakForm).xmlDoc.responseText;
		var ajaxRest = rest.split("$");
		if(ajaxRest[1] == ""){
			hintMsg("没查到相关记录")
		}else{
	   		currGrid.clearAll();
	   		ajaxRest[1] = ajaxRest[1].replace(/[\r\n]/g, "");
	   		jsons = eval("(" + ajaxRest[1] +")");
	    	currGrid.parse(jsons, "json");
	   		setPageNoSizeCount(ajaxRest[0]);
	   		setRowNum(currGrid);
	   		if(url.indexOf("userManager/userManagerAction")>0){
			    var currrCount = currGrid.getRowsNum();
				for(var i = 0; i < currrCount; i++){
					if(currGrid.cells2(i,10).getValue()=="2"&&currGrid.cells2(i,4).getValue()!="admin")
						currGrid.cells2(i,4).setTextColor("blue");
				}	   		
	   		}
   		}
	}
	function setUpInfo(updInfo){
		var updInfos = updInfo.split("^");
		for(var i=0; i < updInfos.length; i++){
			var currInfo = updInfos[i].split("=");
			if(currInfo[1] != "null"){
				currInfo[1] =recovJsonKeyWord(currInfo[1]);
				var valueStr = "";
				currInfo[1] =recovJsonKeyWord(currInfo[1]);
				if($(currInfo[0]).type == "radio"||$(currInfo[0]).type == "checkbox"){
					valueStr = "$('"+currInfo[0]+"').checked = currInfo[1]";
				}else{
					valueStr = "$('"+currInfo[0]+"').value = currInfo[1]";
				}
				eval(valueStr);	
			}
		}
	}

	//初始化window,在 initW_ch中被调用
	function initCufmsW(){
		cufmsW = new dhtmlXWindows();
		cufmsW.enableAutoViewport(true);
		cufmsW.setViewport(50, 50, 100, 100);
		cufmsW.vp.style.border = "#909090 1px solid";
		cufmsW.setImagePath("../dhtmlx/windows/codebase/imgs/");
		return cufmsW;
	}
	function initW_ch(obj, divId, mode, w, h){
		if((typeof obj != "undefined") && !obj.isHidden()){
			obj.setDimension(w, h);
			obj.centerOnScreen();
			return obj;
		}else if((typeof obj != "undefined") && obj.isHidden()){
			obj.show();
			obj.setDimension(w,h);
			obj.centerOnScreen();
		}else{
			if(typeof cufmsW == "undefined"){
				cufmsW = initCufmsW();
			}
			obj = cufmsW.createWindow(divId, 110, 110, w, h);
			if(divId != null&&divId!=""){
				obj.attachObject(divId, false);
			}
			hiddenB(obj, mode);
			obj.centerOnScreen();
		}
		obj.setModal(mode);
		
		return obj;
	}
	function formReset(id, focus,hidIds){
		$(id).reset();
		if(hidIds!=""&&typeof hidIds != "undefined"){
			var hidArry = hidIds.split(",");
			for(var l=0; l<hidArry.length; l++){
				$(hidArry[l]).value="";
			}
		}
		if(typeof focus == "undefined" || focus==""){
			var form = document.getElementById(id);
			var elements = form.elements;
			var element;
			for (i = 0; i < elements.length; i++) {
			 	element = elements[i];
			 	if (element.type == "text" || element.type == "textarea"){
			 		try{
			 			element.focus();
			 		}catch(err){
			 		}
			 		return ;
			 	}
			}
		}
		try{
			$(focus).focus();
		}catch(err){
		}
	}
	function getChecked(GridObj){
		selectItems = "";
		var currGrid = pmGrid;
		if(typeof(GridObj)!="undefined"){
			currGrid = GridObj;
		}
		var allItems = currGrid.getAllItemIds();
		var items = allItems.split(',');
		for(var i = 0; i < items.length; i++){
			if(items[i] != "" && currGrid.cells(items[i], 0).getValue() == 1){
				if (selectItems == ""){
					selectItems = items[i];
				}else{
					selectItems += "," + items[i];
				}
			}
		}
		return selectItems;
	}
	function deleteM(url,GridObj){
		if(url==""){
			hintMsg("您没有该操作权限");
		}
		var deleteIds = getChecked(GridObj);
		url = url + deleteIds;
		var result = dhtmlxAjax.postSync(url, "").xmlDoc.responseText;
		if(result == "success"){
			var dItems = deleteIds.split(',');
			for(var i = 0; i < dItems.length; i++){
				if(dItems[i] != ""){
					GridObj.deleteRow(dItems[i]);
				}
			}
			clsoseCfWin();
		}else if(result=="haveBug"){
			hintMsg("所删除用户有待处理BUG不能删除");
		}else{
			hintMsg("删除失败");
		}
	}
	function addUpInit(id, divId,w, h,msgId,url,GridObj,callback){
		var cb = callback;
		if(typeof cb != "undefined" &&(!cb)==true){
			return;
		}
		if(id=="update"&&GridObj.getSelectedId()==null){
			hintMsg('请选择要修改的记录');
			return ;
		}
		var textId = "cUMTxt";
		if(typeof msgId != "undefined" && msgId!=""){
			textId = msgId;
		}
		cuW_ch = initW_ch(cuW_ch, divId, true, w, h);
		formReset("createForm");
		$(textId).innerHTML = "&nbsp;";
		if(id=="new"){		
			cuW_ch.setText("新建");
		}else if(id=="update"){
			cuW_ch.setText("修改");
			url += GridObj.getSelectedId();
			var ajaxRest  = dhtmlxAjax.postSync(url, "").xmlDoc.responseText;
			if(ajaxRest!="failed"){
				try{
					if(url.indexOf("userManagerAction!updateUserInit.action")>=0){
						upInitInfo = ajaxRest;
					}
					setUpInfo(ajaxRest);
				}catch(err){
					$(textId).innerHTML = "查询要修改记录失败";
				}
			}else{
				$(textId).innerHTML = "查询要修改记录失败";
			}
		}
	}
	function addUpSumbit(url,pojoProId,msgId,GridObj,isClose,hidIds,callback){
		var cb = callback;
		if(typeof cb != "undefined" &&(!cb)==true){
			return;
		}
		var textId = "cUMTxt";
		if(typeof msgId != "undefined" && msgId!=""){
			textId = msgId;
		}
		$(textId).innerHTML =  "&nbsp;";
		var ajaxResut = dhtmlxAjax.postSync(url, "createForm").xmlDoc.responseText;
		if(ajaxResut.indexOf("^") != -1){
			ajaxResut = ajaxResut.replace(/[\r\n]/g, "");
			var result = ajaxResut.split("^");
			if(result[0] == "success"){
				if($(pojoProId).value == "" ){ //新增
					GridObj.addRow(result[1],result[2],0);
				}else{
					var rowNum = GridObj.cells(result[1],1).getValue() - 1;
					GridObj.deleteRow(result[1]);
					GridObj._addRow(result[1],result[2],0);
					if(url.indexOf("userManagerAction!userMaintence")>0&&GridObj.cells(result[1],10).getValue()=="2"&&GridObj.cells(result[1],4).getValue()!="admin"){
						GridObj.cells(result[1],4).setTextColor("blue");
					}
				}
				setRowNum(GridObj);
				GridObj.setSelectedRow(GridObj.getRowId(rowNum));
				if(isClose||$(pojoProId).value != ""){
					cuW_ch.hide();
					cuW_ch.setModal(false);
				}else{
					formReset("createForm","",hidIds);
					$(textId).innerHTML =  "操作成功，可以继续操作";
				}
				GridObj.setSizes();
				return;
			}else{
				if(result[0].indexOf("customMsg")>=0)
					$(textId).innerHTML = result[1];
				else
					$(textId).innerHTML =  "操作失败";
			}
		}else{
			if(ajaxResut=="overCount"){
				$(textId).innerHTML = "用户数己达上限";
			}else{
				$(textId).innerHTML = "操作失败";
			}
			
		}
		return;
	}
	function findM(url,divId,GridObj,w, h,callBack){
		if(divId !=""){ 
			fW_ch = initW_ch(fW_ch, divId, false, w, h);
			fW_ch.setText("查找");
			//formReset("findForm");
			return ;
		}
		var ajaxRest = dhtmlxAjax.postSync(url+"&dto.pageSize=" + pageSize, "findForm").xmlDoc.responseText;
	   	var datas = ajaxRest.split("$");
	    if(datas[1] != ""){
	    	datas[1] = datas[1].replace(/[\r\n]/g, "");
	    	GridObj.clearAll();
	    	jsons = eval("(" + datas[1] +")");
	    	GridObj.parse(jsons, "json");
	    	setPageNoSizeCount(datas[0]);
	    	setRowNum(GridObj);
	    	pageBreakUrl = url;
	   		pageBreakForm = "findForm";
	   		fW_ch.hide();
	   		if(url.indexOf("userManager/userManagerAction")>0){
			    var currrCount = GridObj.getRowsNum();
				for(var i = 0; i < currrCount; i++){
					if(GridObj.cells2(i,10).getValue()=="2"&&GridObj.cells2(i,4).getValue()!="admin")
						GridObj.cells2(i,4).setTextColor("blue");
				}	   		
	   		}
	    }else{
	    	hintMsg("没查到相关记录");
	    }
	}
	
	
