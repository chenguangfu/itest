<%@ page language="java" contentType="text/html; charset=utf-8"%>
<%@ taglib prefix="ww" uri="/webwork"%>
<%@ taglib prefix="c" uri="/WEB-INF/c.tld"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
	<script type="text/javascript">
		popContextMenuFlg=0;
		var custContextMenuFlg=0;
	</script>
		<link rel="STYLESHEET" type="text/css" href="<%=request.getContextPath()%>/dhtmlx/dhtmlxTree/codebase/dhtmlxtree.css">
		<script type="text/javascript" src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxcommon.js"></script>
		<script type="text/javascript" src="<%=request.getContextPath()%>/dhtmlx/dhtmlxTree/codebase/dhtmlxtree.js"></script>
		<link rel='STYLESHEET' type='text/css' href='<%=request.getContextPath()%>/css/page.css'>
		<title>查看角色权限</title>
	</head>
	<body style="overflow-y:hidden;" bgcolor="#ffffff">
		<ww:hidden id="authTree" name="dto.authTree"></ww:hidden>
		<div id="treeBox" style="overflow-y:hidden;width:100%;height:95%"></div> 

		<script>
		tree=new dhtmlXTreeObject("treeBox","100%","100%",0);					
		tree.setImagePath(conextPath+"/dhtmlx/dhtmlxTree/codebase/imgs/");
		tree.enableCheckBoxes(0);
		tree.enableHighlighting(1);
		tree.setImageArrays("plus","plus2.gif","plus3.gif","plus4.gif","plus.gif","plus5.gif");
		tree.setImageArrays("minus","minus2.gif","minus3.gif","minus4.gif","minus.gif","minus5.gif");
		tree.setStdImages("book.gif","books_open.gif","books_close.gif");
		var treeNodesResult = $("authTree").value;
		if(treeNodesResult==""){
			parent.broAutW_ch.setModal(false);
			parent.broAutW_ch.hide();
			parent.hintMsg("当前角色无任何权限");
		}else{
			 tree.insertNewItem('0','-1','拥有权限');
		}
		var treeNodeArray = treeNodesResult.split(";");
		for(var i=0;i<treeNodeArray.length;i++){
			var nodeContext= treeNodeArray[i];
			var node = nodeContext.split(",");
			var parentId =node[0] ;
			var id  = node[1];
			var text = node[2];
			if(text.indexOf("#")>=0){ //带#的是占位菜单,暂时不用
				continue;
			}
			tree.insertNewItem(parentId,id,text);
		}
		</script>
		<ww:include value="/jsp/common/dialog.jsp"></ww:include>
	</body>
</html>
