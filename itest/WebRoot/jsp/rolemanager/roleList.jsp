<%@ page contentType="text/html; charset=UTF-8"%>
<%@taglib uri="/webwork" prefix="ww"%>
<%@taglib uri="/WEB-INF/pmButton.tld" prefix="pmTag"%>
<HTML>
<HEAD>
	<TITLE>角色列表</TITLE>
	<META content="text/html; charset=UTF-8" http-equiv=Content-Type>
	<link rel="STYLESHEET" type="text/css" href="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgrid.css">
	<link rel="STYLESHEET" type="text/css" href="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgrid_skins.css">
	<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/dhtmlx/toolbar/codebase/skins/dhtmlxtoolbar_dhx_blue.css"></link>
	<link rel='STYLESHEET' type='text/css' href='<%=request.getContextPath()%>/css/page.css'>
	<script  src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxcommon.js"></script>
	<script  src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgrid.js"></script>		
	<script  src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgridcell.js"></script>
	<script type="text/javascript" src="<%=request.getContextPath()%>/dhtmlx/toolbar/codebase/dhtmlxtoolbar.js"></script>
	<script type="text/javascript" src="<%=request.getContextPath()%>/js/globalVariable.js"></script>
</HEAD>
<BODY bgcolor="#ffffff" style="overflow-x:hidden;">

	<table width="100%" cellspacing="0" cellpadding="0" border="0">
		<tr>
		  <td valign="top" width="100%">
		  <div id="toolbarObj"></div>
		  </td>
		</tr>
		<tr>
		  <td valign="top" width="100%">
		    <div id="gridbox" ></div>
		  </td>
		</tr>				
    </table>
    <input type="hidden" id="listStr" name="listStr" value="${dto.listStr}" />
	<script>
	ininPage("toolbarObj", "gridbox", 442);
	<pmTag:button page="role" find="true"/>
	var isAdmin = "${dto.isAdmin}";
	</script>
	<script type="text/javascript" src="<%=request.getContextPath()%>/jsp/rolemanager/role.js"></script>
	<div id="createD" class="cycleTask gridbox_light" style="border:0px;display:none;">
		  <div class="objbox" style="overflow:auto;width:100%;">
		<ww:form theme="simple" method="post" id="createF" name="createF" namespace="/role" action="">
		  <ww:hidden id="roleId" name="dto.role.roleId"></ww:hidden>
		   <ww:hidden id="insertDate" name="dto.role.insertDate"></ww:hidden>
		   <input type="hidden" id="adminObj" name="dto.isAdmin" value=""/>
			<table class="obj row20px" cellspacing="0" cellpadding="0" border="0" width="100%">
				<tr class="ev_mypm">
					<td colspan="5"><div id="cUMTxt" align="center" style="color:Blue;padding:2px">&nbsp;</div></td>
				</tr>
				<tr class="odd_mypm">
					<td  align="right" class="rightM_center" style="color:Red;">角色名称:</td>
					<td colspan="4">
						<ww:textfield id="roleName" name="dto.role.roleName" cssClass="text_c" cssStyle="width:320;padding:2 0 0 4;" onblur="javascript:if(!isWhitespace(this.value)){$('cUMTxt').innerHTML='&nbsp;'};"></ww:textfield>
	        		</td>
				</tr>
				<tr class="ev_mypm">
					<td align="right" class="rightM_center">可访问IP:</td>
					<td colspan="4">
					  <textarea id="accessIp" name="dto.role.accessIp"  onMouseOver="if(this.value=='多个IP用分号(;)隔开'){select()};" onblur="javascript:if(this.value==''){this.value='多个IP用分号(;)隔开';}" cols="60" rows="3" Class="text_c" style="width:320;padding:2 0 0 4;">
					  </textarea>
					</td>
				</tr>
				<tr class="odd_mypm">
					<td align="right" class="rightM_center" >备注:</td>
					<td  colspan="4">
					  <ww:textarea id="remark" name="dto.role.remark" cssClass="text_c" cssStyle="width:320;padding:2 0 0 4;"></ww:textarea>
					</td>	
				</tr>
	
				<tr class="ev_mypm"><td colspan="5">&nbsp;</td></tr>
				<tr class="odd_mypm">
					<td colspan="5" align="right">
						<a class="bluebtn" href="javascript:void(0);" id="retBtn"onclick="cuW_ch.setModal(false);cuW_ch.hide();"
							style="margin-left: 6px;"><span> 返回</span> </a>
						<a class="bluebtn" href="javascript:void(0);" id="creat2_b"onclick="addUpSub('contF');"
							style="margin-left: 6px;display:none"><span> 确定并继续</span> </a>
						<a class="bluebtn" href="javascript:void(0);" id="creat_b"onclick="addUpSub();"
							style="margin-left: 6px;"><span> 确定</span> </a>
						<a class="bluebtn" href="javascript:void(0);" id="restBtn"onclick="formReset('createF', 'roleName');"
							style="margin-left: 6px;"><span> 重置</span> </a>
					</td>
				</tr>
				<tr class="ev_mypm">
				 <td colspan="5" >&nbsp;</td>
				</tr>
			</table>
		</ww:form>
		</div>
	</div>
		<div id="findD" class="cycleTask gridbox_light" style="border:0px;display:none;">
		  <div class="objbox" style="overflow:auto;width:100%;">
			<ww:form theme="simple" method="post" id="findF" name="findF"
				namespace="/userManager"
				action="">
				<table  class="obj row20px" cellspacing="0" cellpadding="0" border="0" width="100%">
					<tr class="ev_mypm">
						<td colspan="4" style="border-right:0">&nbsp;</td>
					</tr>
					<tr class="odd_mypm">
						<td align="right" class="rightM_center" style="border-right:0;width:70">角色名称:</td>
						<td colspan="3" class="dataM_left"style="border-right:0" >
							<ww:textfield id="roleNameF" name="dto.role.roleName" cssClass="text_c"
								cssStyle="width:100%;padding:2 0 0 4;"></ww:textfield>
						</td>
					</tr>
					<tr class="ev_mypm">
						<td align="right" class="rightM_center" style="border-right:0;width:70" nowrap>角色人员:</td>
						<td colspan="3" class="dataM_left"style="border-right:0" >
							<ww:hidden id="userIdsF" name="dto.role.userIds"></ww:hidden>
							<ww:textfield id="userIdsNameF" name="userIdsNameF" cssClass="text_c" readonly="true"
								cssStyle="width:100%;padding:2 0 0 4;" onfocus="popselWin('userIdsF','userIdsNameF',' ' ,true,10);"></ww:textfield>
						</td>
					</tr>
					<tr class="odd_mypm">
						<td align="right" class="rightM_center" style="border-right:0;width:70">角色权限:</td>
						<td colspan="3" class="dataM_left"style="border-right:0" >
							<ww:hidden id="funIdsF" name="dto.role.funIds"></ww:hidden>
							<ww:textfield id="funNamesF" name="funNamesF" cssClass="text_c" readonly="true"
								cssStyle="width:100%;padding:2 0 0 4;" onfocus="loadAuthTree();"></ww:textfield>
						</td>
					</tr>
					<tr class="ev_mypm">
						<td align="right" class="rightM_center" style="border-right:0;width:70">访问IP:</td>
						<td colspan="3" class="dataM_left"style="border-right:0">
							<ww:textfield id="accIpF" name="dto.role.accessIp" cssClass="text_c" 
								cssStyle="width:100%;padding:2 0 0 4;" ></ww:textfield>
						</td>
					</tr>
					<tr class="odd_mypm">
						<td colspan="4">&nbsp;</td>
					</tr>
					<tr class="ev_mypm">
						<td colspan="4" align="right">
							<a class="bluebtn" href="javascript:void(0);" id="saveBtn"onclick="javascript:fW_ch.setModal(false);fW_ch.hide();"
							style="margin-left: 6px;"><span> 返回</span> </a>
							<a class="bluebtn" href="javascript:void(0);" id="cuU_b"onclick="efind();"
								style="margin-left: 6px;"><span> 查询</span> </a>
							<a class="bluebtn" href="javascript:void(0);" id="saveBtn"onclick="formReset('findF', 'roleNameF');$('funIdsF').value='';$('userIdsF').value='';"
							style="margin-left: 6px;"><span> 重置</span> </a>
						</td>
					</tr>
					<tr class="odd_mypm">
						<td colspan=4" >&nbsp;</td>
					</tr>
				</table>
			</ww:form>
			</div>
		</div>
	<ww:include value="/jsp/common/dialog.jsp"></ww:include>
	<ww:include value="/jsp/common/selCompPerson.jsp"></ww:include>
	<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/dhtmlx/windows/codebase/dhtmlxwindows.css">
	<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/dhtmlx/windows/codebase/skins/dhtmlxwindows_dhx_blue.css">
</BODY>
</HTML>
