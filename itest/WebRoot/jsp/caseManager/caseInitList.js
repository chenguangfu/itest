﻿	pmGrid = new dhtmlXGridObject('gridbox');
	pmGrid.setImagePath(conextPath+"/dhtmlx/grid/codebase/imgs/");
	//最后一隐藏列为是否发布的标识,和模块 id
    pmGrid.setHeader("&nbsp;,编号,用例描述,&nbsp;,&nbsp;,最新状态,类别,优先级,最近处理人,&nbsp;,&nbsp;,编写人,成本,项目名称,编写日期,附件,&nbsp;,&nbsp;");
    pmGrid.setInitWidths("25,80,*,0,0,70,60,80,100,0,0,100,40,120,120,40,0,0");
    pmGrid.setColAlign("center,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left");
    pmGrid.setColTypes("ch,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro");
    pmGrid.setColSorting("int,int,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str");
    pmGrid.enableTooltips("false,true,true,true,true,true,true,true,true,true,true,true,true,true,true,false,false,false");
    pmGrid.setSkin("light");
    pmGrid.enableAutoHeight(true, 700);
    colTypeReset();
    pmGrid.init();
    pmGrid.enableRowsHover(true, "red");
	initGrid(pmGrid,"listStr",pmBar,"noSetRowNum");
    pmGrid.attachEvent("OnCheck",doOnCheck);
    pmGrid.attachEvent("onRowSelect",doOnSelect);  
    function doOnCheck(rowId,cellInd,state){
		
		return true;
	}
    function doOnSelect(rowId,index){
		//pmGrid.cells(rowId, 0).setValue(true);
		var rest = pmGrid.cells(rowId, 5).getValue();
		pmGrid.setSelectedRow(rowId);
		try{
			if((rest=='待审核'||rest=='待修正')&&$("isReview").value=='1'){
				pmBar.enableItem("adtInit");
			}else{
				pmBar.disableItem("adtInit");
			}
			if(rest=='待审核'||rest=='待修正'){
				pmBar.disableItem("exe");
			}else{
				pmBar.enableItem("exe");
			}
		}catch(err){}
	}
	function colTypeReset(){
		pmGrid.setColTypes("ch,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro");
	}
	function sw2Link(){
		pmGrid.setColTypes("ch,link,link,ro,ro,ro,ro,ro,ro,ro,ro,link,ro,ro,ro,ro,ro,ro");
	}
	function getTttle2(rowNum,colIn){
		return pmGrid.cells2(rowNum,colIn).getValue();
		
	}
	loadLink();
	function loadLink(){
		for(var i = 0; i <pmGrid.getRowsNum(); i++){
			pmGrid.cells2(i,1).cell.innerHTML="<a href='javascript:relaBug()' title='关联BUG'>"+getTttle2(i,1)+"</a>";
			if(getTttle2(i,2).indexOf("addUpInit")<0){
				pmGrid.cells2(i,2).cell.innerHTML="<a href='javascript:addUpInit(\"detal\")' title='查看明细-------"+getTttle2(i,2)+"'>"+getTttle2(i,2)+"</a>";
			}
			if(getTttle2(i,11).indexOf("viewCaseHistory")<0){
				pmGrid.cells2(i,11).cell.innerHTML="<a href='javascript:viewCaseHistory()' title='查看历史-------"+getTttle2(i,11)+"'>"+getTttle2(i,11)+"</a>";
			}
			if(getTttle2(i,15)!=""&&getTttle2(i,15).indexOf("openAtta")<0){
				pmGrid.cells2(i,15).cell.innerHTML="<img src='"+conextPath+"/images/button/attach.gif' alt='附件'  title='打开附件' onclick=\"openAtta('"+getTttle2(i,15)+"')\"/>";
			}
			if(!copyCaseModel){
				pmGrid.cells(pmGrid.getRowId(i), 0).setValue(0);
				pmGrid.cells(pmGrid.getRowId(i), 0).setDisabled(true);
			}
		}
		sw2Link(); 	}
