	contin='0';
	pmBar.attachEvent("onClick", function(id){
		if(id =="find"){
	    	findInit();
	   }else if(id == "first"){
			pageAction(1, pageSize);
		}else if(id == "last"){
			pageAction(pageCount, pageSize);
		}else if(id == "next"){
			pageAction(pageNo + 1, pageSize);
		}else if(id == "pervious"){
			pageAction(pageNo -1, pageSize);
		}else if(id == "id1" || id == "id2" || id == "id3" || id == "id4"){
			var pageSizeTemp = parseInt(pmBar.getListOptionText("pageP", id));
			if(pageSize==pageSizeTemp){
				return;
			}
			pageSize = pageSizeTemp;
			pageAction(pageNo, pageSize);
		}
	});
	pmBar.attachEvent("onValueChange", function(id, pageNo) {
		pageAction(pageNo, pageSize);
		return;
	});
	pmBar.attachEvent("onEnter", function(id, value) {
		if(!isDigit(value, false)){
			pmBar.setValue("page", pageNo);
			return;
		}
		var pageNoTemp = parseInt(value);
		if(pageNoTemp < 1){
			pageNoTemp = 1;
		}else if(pageNoTemp > pageCount){
			pageNoTemp = pageCount;
		}
		pageAction(pageNoTemp, pageSize);
		return;
	});
	pmGrid = new dhtmlXGridObject('gridbox');
	pmGrid.setImagePath(conextPath+"/dhtmlx/grid/codebase/imgs/");
    pmGrid.setHeader("&nbsp;,序号,项目编号,项目名称,研发部门,测试阶段,项目PSM,计划开始时间,计划结束时间,测试计划,状态,测试负责人,&nbsp;");
    if(operCmdVar=="fromOutLine")
    	pmGrid.setInitWidths("0,40,80,80,80,70,80,90,90,70,40,*,0");
    else
    	pmGrid.setInitWidths("0,40,80,80,80,70,80,90,90,70,40,150,0");
    pmGrid.setColAlign("center,center,left,left,left,center,left,center,center,center,center,left,left");
    pmGrid.setColTypes("ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro");
    pmGrid.setColSorting("int,str,str,str,str,str,str,str,str,str,str,str,str");
	//pmGrid.attachEvent("onCheckbox",doOnCheck);
	pmGrid.enableAutoHeight(true, 700);
    pmGrid.init();
    pmGrid.enableRowsHover(true, "red");
    pmGrid.enableTooltips("false,true,true,true,true,true,true,true,true,true,true,true,false");
    pmGrid.setSkin("light");
    initGrid(pmGrid,"listStr");
    pmGrid.attachEvent("OnCheck",doOnCheck);
    pmGrid.attachEvent("onRowSelect",doOnSelect);  
    function doOnCheck(rowId,cellInd,state){
		this.setSelectedRow(rowId);
		return true;
	}
    function doOnSelect(rowId,index){
		pmGrid.cells(rowId, 0).setValue(true);
		pmGrid.setSelectedRow(rowId);
	}
	function getTttle2(rowNum,colIn){
		return pmGrid.cells2(rowNum,colIn).getValue();
	}
	function getTttle(rowId,colIn){
		return pmGrid.cells(rowId,colIn).getValue();
	}
	loadLink();
	function loadLink(){
		for(var i = 0; i <pmGrid.getRowsNum(); i++){
			if(getTttle2(i,9)!="")
				pmGrid.cells2(i,9).cell.innerHTML="<img src='"+conextPath+"/images/button/attach.gif' alt='附件'  title='查看测试计划' onclick=\"openAtta('"+getTttle2(i,9)+"')\"/>";
			pmGrid.cells2(i,3).cell.innerHTML="<a href='javascript:nameLinkResps()' title='"+nameLinkRespTip(i)+"'>"+getTttle2(i,3)+"</a>";
				
		}
		sw2Link();
	}
	function nameLinkRespTip(rowNum){
		if(operCmdVar=="myBugList"){
			return "进入项目："+getTttle2(rowNum,3) +" 缺陷跟踪管理";
		}else if(operCmdVar=="caseList"){
			return "进入项目："+getTttle2(rowNum,3) +" 测试用例管理";
		}else if(operCmdVar=="outLineList"||operCmdVar=="fromOutLine"){
			return "进入项目："+getTttle2(rowNum,3) +" 测试需求管理";
		}else if(operCmdVar=="casePkg"){
			return "进入项目："+getTttle2(rowNum,3) +" 测试包";
		}else if(operCmdVar=="caseLib"){
			return "进入项目："+getTttle2(rowNum,3) +" 用例库";
		}else if(operCmdVar=="casePgkScene"){
			return "进入项目："+getTttle2(rowNum,3) +" 测试包场景";
		}
		return "进入项目："+getTttle2(rowNum,3) +" 缺勤跟踪管理";	
	}
	function nameLinkResps(){	
		var url = "/bugManager/bugManagerAction!loadMyBug.action";
		if(operCmdVar=="caseList"){
			url="/caseManager/caseManagerAction!loadCase.action";
		}else if(operCmdVar=="outLineList"||operCmdVar=="fromOutLine"){
			url="/outLineManager/outLineAction!initList.action";
		}else if(operCmdVar=="casePkg"){
			return "进入项目："+getTttle2(rowNum,3) +" 测试包";
		}else if(operCmdVar=="caseLib"){
			return "进入项目："+getTttle2(rowNum,3) +" 用例库";
		}else if(operCmdVar=="casePgkScene"){
			return "进入项目："+getTttle2(rowNum,3) +" 测试包场景";
		}
		if(operCmdVar=="fromOutLine")
			window.location = conextPath+url+"?dto.taskId=" + pmGrid.getSelectedId();
		else
			parent.location = conextPath+url+"?dto.taskId=" + pmGrid.getSelectedId();
	}
	function colTypeReset(){
		pmGrid.setColTypes("ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro");
	}
	function sw2Link(){
		pmGrid.setColTypes("ro,ro,ro,link,ro,ro,ro,ro,ro,link,ro,ro,ro");
	}	

	function pageAction(pageNo, pageSize){
		if(pageNo>pageCount && pageSizec<1){
			pmBar.setValue("page", pageNo);
			return ;
		}
		var url = conextPath+"/singleTestTask/singleTestTaskAction!swTestTaskList.action?dto.pageNo="+ pageNo +"&dto.pageSize=" + pageSize;
		var userJson = dhtmlxAjax.postSync(url, "findForm").xmlDoc.responseText.split("$");
		if(userJson[1] == ""){
			hintMsg("没查到相关记录");
		}else{
	   		pmGrid.clearAll();
	   		colTypeReset();
	   		pmGrid.parse(eval("(" + userJson[1] +")"), "json");
	   		loadLink();
	   		setPageNoSizeCount(userJson[0]);
	   		setRowNum(pmGrid);
   		}

	}	

	function closeMe(winObj){
		winObj.setModal(false);
		winObj.hide();	
		if(typeof(mypmCalendar_ch)!='undefined')
			mypmCalendar_ch.hide();
		if(typeof(comUserSelW_ch)!='undefined'){
			comUserSelW_ch.setModal(false);
			comUserSelW_ch.hide();		
		}
	}

	function findInit(){
		fW_ch = initW_ch(fW_ch, "findDiv", true, 500, 135);
		fW_ch.setText("查询");
		fW_ch.show();
		fW_ch.bringToTop();
	}
	function findExe(){
		var url = conextPath+"/singleTestTask/singleTestTaskAction!swTestTaskList.action";
		var userJson = dhtmlxAjax.postSync(url, "findForm").xmlDoc.responseText.split("$");
		if(userJson[0]=="failed"){
			hintMsg("查询发重错误");
			return;
		}
		if(userJson[1] == ""){
			pmGrid.clearAll();
			setPageNoSizeCount(userJson[0]);
		}else{
	   		pmGrid.clearAll();
	   		colTypeReset();
	   		pmGrid.parse(eval("(" + userJson[1] +")"), "json");
	   		loadLink();
	   		setPageNoSizeCount(userJson[0]);
	   		setRowNum(pmGrid);
	   		fW_ch.setModal(false);
	   		fW_ch.hide();
   		}
	}