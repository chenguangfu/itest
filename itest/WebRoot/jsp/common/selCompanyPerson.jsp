<%@ page contentType="text/html; charset=UTF-8"%>
<div id="selPeopleDiv" class="gridbox_light" style="border:0px;display:none;">
	<div class="objbox" style="overflow:auto;width:100%;">
	  	<form  method="post" id="selPeopleForm" name="selPeopleForm"  action="">
			<table class="obj row20px" cellspacing="0" cellpadding="0" width="100%">
				<tr class="ev_mypm">
				  <td align="center"  class="leftM_center" style="border-right:0;">用户组:</td>
				  <td style="border-right:0;">
				    <select id="sel_groupIds" name="dto.group.id" style="width:80;" class="select_c" onchange="loadSelUser()"> 
				    	<option value="-1"></option> 
				    </select>
				  </td>
				  <td align="center" class="leftM_center" style="border-right:0">姓名:</td>
				  <td style="border-right:0;">
				  	<input type="text" id="userNameSF" name="dto.userName" class="text_c"  style="width:80;"/>
				  </td>
				  <td style="border-right:0;">
				  	<img id="fres_btn" style="cursor:pointer;" src="<%=request.getContextPath()%>/jsp/common/images/search.gif" onclick="loadSelUser();" title="查询人员" />&nbsp;&nbsp;
				    <img id="selPeople_b" style="cursor:pointer;" src="<%=request.getContextPath()%>/jsp/common/images/selected.gif" onclick="selPeople();" title="选择人员" />
				  </td>
				</tr>
				<tr height="290" class="ev_mypm" style="padding-top:2px;">
					<td colspan="5">
						<div id="selGridbox" style="float:left;width:171px;"></div><div style="float:left;width:5px;"></div><div id="seledGridbox" style="float:left;width:172px;"></div>
					</td>
				</tr>
			</table>
		</form>
	</div>
</div>
<script type="text/javascript">
importJs(conextPath+"/jsp/common/selCompanyPerson.js");
</script>