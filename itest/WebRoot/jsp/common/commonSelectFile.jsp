<%@ page contentType="text/html; charset=UTF-8"%>
<%@taglib uri="/webwork" prefix="ww"%>
<%@taglib uri="/WEB-INF/pmButton.tld" prefix="pmTag"%>
<html>
	<head>
	<script type="text/javascript">
		popContextMenuFlg=0;
		var custContextMenuFlg=0;
	</script>
		<title>文档选择</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

		<link rel="STYLESHEET" type="text/css" href="<%=request.getContextPath()%>/dhtmlx/dhtmlxTree/codebase/dhtmlxtree.css">
		<script type="text/javascript" src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxcommon.js"></script>
		<script type="text/javascript" src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgrid.js"></script>
		<script type="text/javascript" src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgridcell.js"></script>
		<script type="text/javascript" src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/excells/dhtmlxgrid_excell_link.js"></script>
		<script type="text/javascript" src="<%=request.getContextPath()%>/dhtmlx/dhtmlxTree/codebase/dhtmlxtree.js"></script>
		<script type="text/javascript" src="<%=request.getContextPath()%>/dhtmlx/dhtmlxTree/codebase/ext/dhtmlxtree_ed.js"></script>
		<link rel="STYLESHEET" type="text/css" href="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgrid.css">
		<link rel="STYLESHEET" type="text/css" href="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgrid_skins.css">
		<link rel="STYLESHEET" type="text/css" href="<%=request.getContextPath()%>/dhtmlx/toolbar/codebase/skins/dhtmlxtoolbar_dhx_blue.css">
		<script type="text/javascript" src="<%=request.getContextPath()%>/dhtmlx/toolbar/codebase/dhtmlxtoolbar.js"></script>
		<script type="text/javascript" src="<%=request.getContextPath()%>/js/globalVariable.js"></script>
		<script type="text/javascript" src="<%=request.getContextPath()%>/js/commonFunction_top.js"></script>
		<link rel='STYLESHEET' type='text/css' href='<%=request.getContextPath()%>/css/page.css'>
	</head>
	<script type="text/javascript">
		popContextMenuFlg=0;
		parent.pmDocSelW_ch.setDimension(860, 550);
	</script>
	<body bgcolor="#FFFFFF" leftmargin="0"  topmargin="3" marginwidth="0" oncontextmenu="return false" style="overflow-x:hide;overflow-y:hidden">
		<ww:hidden id="templetId" name="templetId"></ww:hidden>
		<input type="hidden" id="listStr" name="listStr" value="" />
		<input type="hidden" name="nodeDataStr" value="${nodeDataStr}" id="nodeDataStr" />
		<input type="hidden" name="currNodeId"  id="currNodeId" />
		<input type="hidden" name="currLevel" value="" id="currLevel" />
		<input type="hidden" name="parentNodeId" value="${dto.parentId}" id="parentNodeId" />
		<table width="35%;">
			<tr>
				<td nowrap>
					<a class="bluebtn" href="javascript:void(0);" id="saveBtn"
						onclick="javascript:closeMe();" style="margin-left: 6px;"><span>
							返回</span> </a>
					<a class="bluebtn" href="javascript:void(0);" id="saveBtn1"
						onclick="reFreshNode()" ><span>
							刷新当前目录</span> </a>
				</td>
				
				<td width="20">&nbsp;</td>
				<td width="20">&nbsp;</td>
				<td width="20">&nbsp;</td>
				<td width="20">&nbsp;</td>
				<td width="20">&nbsp;</td>
			</tr>
		</table>
		<div  style="width:100%;height:95%">
		<table style="width: 100%;height:500;" >
			<tr>
				<td width="30%">
					<div id="treeBox" style="overflow:auto;width:100%;height:490;border: 1px solid;border-color : Gray;"></div>
				</td>
				<td width="70%">
					<div id="docGrid" style="width:100%;height:490;border: 1px solid;border-color : Gray;">
					<table width="100%" cellspacing="0" cellpadding="0">
						<tr>
							<td valign="top" style="padding-right: 3px;">
								<div id="toolbarObj"></div>
							</td>
						</tr>
						<tr>
							<td valign="top">
								<div id="gridbox"></div>
							</td>
						</tr>
					</table>
					</div>
				</td>
			</tr>
		</table>
       </div>
       
		<script type="text/javascript">	 
	//ininPage("toolbarObj", "gridbox", 770);
	var fileFieldId = "${dto.fileFieldId}";
	var fileNameFieldId = "${dto.fileNameFieldId}";
	var chkLock =  "${dto.chkLock}";
    var pmBar = new dhtmlXToolbarObject("toolbarObj"); 
    pmBar.setIconsPath(conextPath+"/dhtmlx/toolbar/images/"); 
    pmBar.addButton("find",1 , "", "search.gif"); 
    pmBar.setItemToolTip("find", "查询"); 
    pmBar.addButton("reFreshP",2 , "", "page_refresh.gif"); 
    pmBar.setItemToolTip("reFreshP", "刷新页面"); 
    pmBar.addButton("first",4 , "", "first.gif", "first.gif"); 
    pmBar.setItemToolTip("first", "第一页"); 
    pmBar.addButton("pervious",5, "", "pervious.gif", "pervious.gif"); 
    pmBar.setItemToolTip("pervious", "上一页"); 
    pmBar.addSlider("slider",6, 80, 1, 30, 1, "", "", "%v"); 
    pmBar.setItemToolTip("slider", "滚动条翻页"); 
    pmBar.addButton("next",7, "", "next.gif", "next.gif"); 
    pmBar.setItemToolTip("next", "下一页"); 
    pmBar.addButton("last",8, "", "last.gif", "last.gif"); 
    pmBar.setItemToolTip("last", "末页"); 
    pmBar.addInput("page",9, "", 25); 
    pmBar.addText("pageMessage",10, ""); 
    pmBar.addText("pageSizeText",11, "每页"); 
    var opts = Array(Array('id1', 'obj', '10'), Array('id2', 'obj', '15'), Array('id3', 'obj', '20')); 
    pmBar.addButtonSelect("pageP",12, "page", opts); 
    pmBar.setItemToolTip("pageP", "每页记录数"); 
    pmBar.addText("pageSizeTextEnd",13, "条"); 
    pmBar.setListOptionSelected('pageP','id1');
    pmBar.addSeparator("sep_swTask2",14);  
    pmBar.attachEvent("onClick", function(id) { 	
    	if(id =="reFreshP"){
    		pageAction(pageNo, pageSize); 
    	} 
    });  
    pmBar.attachEvent("onClick", function(id) {
    	if(id =="back"){
    		var reUrl= document.referrer;    
    		if(reUrl!=""&&(reUrl.indexOf('main.jsp')>0||reUrl.indexOf('myHome.jsp')>0)){
    			history.back();    	return;    
    		}    
    		parent.mypmMain.location=reUrl; 
    	}	
    });
    

	</script>
		<div id="findDiv"  class="cycleTask gridbox_light" style="border:0px;display:none;">
		  <div class="objbox" style="overflow:auto;width:100%;">
			<ww:form theme="simple" method="post" id="findForm" name="findForm"
				namespace="/singleTestTask" action="">
				<table class="obj row20px" cellspacing="0" cellpadding="0" width="100%">
					<tr class="ev_mypm">
						<td colspan="2" style="border-right:0;width:440">
							<div id="cUMTxtf" align="center" style="color: Blue; padding: 2px">
						&nbsp;
					</div>
						</td>
					</tr>
				  <tr class="odd_mypm">
				  	 <td class="rightM_center" style="width:80;border-right: 0">
				  	 文件名:
				  	 </td>
				    <td class="dataM_left" style="width:360;border-right: 0">
						<ww:textfield id="fileName" maxlength="60" name="dto.fileName" cssStyle="width:360;padding:2 0 0 4;" cssClass="text_c"> </ww:textfield>			    
				    </td>
				  </tr>	
				  <tr class="ev_mypm">
				  	 <td class="rightM_center" style="width:80;border-right: 0">
				  	 发布人:
				  	 </td>
				    <td class="dataM_left" style="width:360;border-right: 0">
				    	<ww:hidden id="publisherId" name="dto.publisherId"></ww:hidden>
						<ww:textfield id="publisher"  name="publisher" readonly="true" onfocus="popselWin('publisherId','publisher',true);" cssStyle="width:360;padding:2 0 0 4;" cssClass="text_c"> </ww:textfield>			    				    
				    </td>
				  </tr>	
					<tr class="odd_mypm">
						<td colspan="2" align="right">
							<a class="bluebtn" href="javascript:void(0);" id="quBtn"onclick="javascript:fW_ch.setModal(false);fW_ch.hide();"
							style="margin-left: 6px;"><span> 返回</span> </a>
							<a class="bluebtn" href="javascript:void(0);" id="quU_b"onclick="findExe();"
								style="margin-left: 6px;"><span> 查询</span> </a>
							<a class="bluebtn" href="javascript:void(0);" id="qretBtn"onclick="formReset('findForm');$('publisherId').value=''"
							style="margin-left: 6px;"><span> 重置</span> </a>
						</td>
					</tr>
					<tr class="ev_mypm">
						<td colspan="2">&nbsp;</td>
					</tr>
				</table>
			</ww:form>
		</div>
		</div>
		<ww:include value="/jsp/common/docDownLoad.jsp"></ww:include>
		<ww:include value="/jsp/common/dialog.jsp"></ww:include>
		<script type="text/javascript" src="<%=request.getContextPath()%>/jsp/common/commonSelectFile.js"></script>
		<script type="text/javascript">
	    if("0,1,无权查看任何文件,0,1"==$("nodeDataStr").value&&fileFieldId==""){
	    	parent.hintMsg("无权查看任何文件");
	    	parent.forbidDocBro=1;
	    	closeMe();
	    }
	</script>
	</body>
</html>
