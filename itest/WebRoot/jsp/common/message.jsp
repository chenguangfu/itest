<%@ page contentType="text/html; charset=UTF-8"%>
<div id="messageDiv" class="gridbox_light" style="border:0px;display:none;">
	<div class="objbox" style="overflow:auto;width:100%;z-index:99;">
		<table class="obj row20px" cellspacing="0" cellpadding="0" border="0" width="100%">
			<tr class="odd_mypm"><td>&nbsp;</td></tr>
			<tr class="odd_mypm"><td class="mypm_center"><img src="<%=request.getContextPath()%>/jsp/common/images/tip.gif" /></td></tr>
			<tr class="odd_mypm"><td class="mypm_center"><div id="messageText" >&nbsp;</div></td></tr>
			<tr class="odd_mypm"><td>&nbsp;</td></tr>
		</table>
	</div>
</div>
<div id="warningMessageDiv" class="gridbox_light" style="border:0px;display:none;z-index:99;">
	<div class="objbox" style="overflow:auto;width:100%;">
		<table class="obj row20px" cellspacing="0" cellpadding="0" border="0" width="100%">
			<tr class="odd_mypm"><td>&nbsp;</td></tr>
			<tr class="odd_mypm"><td class="mypm_center"><img src="<%=request.getContextPath()%>/jsp/common/images/warning.gif" /></td></tr>
			<tr class="odd_mypm"><td class="mypm_center" style="color:red;"><div id="warningMessageText" >&nbsp;</div></td></tr>
			<tr class="odd_mypm"><td>&nbsp;</td></tr>
		</table>
	</div>
</div>
<div id="deleteMessageDiv" class="gridbox_light" style="border:0px;display:none;z-index:99;">
	<div class="objbox" style="overflow:auto;width:100%;">
		<table class="obj row20px" cellspacing="0" cellpadding="0" border="0" width="100%">
			<tr class="odd_mypm"><td>&nbsp;<input id="customData" name="customData" type="hidden" value="" /> </td></tr>
			<tr class="odd_mypm"><td class="mypm_center"><img src="<%=request.getContextPath()%>/jsp/common/images/question.gif" /></td></tr>
			<tr class="odd_mypm"><td class="mypm_center"><div id="deleteMessageText" >&nbsp;</div></td></tr>
			<tr class="odd_mypm"><td colspan="3" align="right" style="padding-right:8px;"><img src="<%=request.getContextPath()%>/jsp/common/images/save.gif" onclick="customDeleteObject();" title="确定" style="cursor:pointer;" />&nbsp;&nbsp;<img src="<%=request.getContextPath()%>/jsp/common/images/reset.gif" onclick="deleteMessageW_ch.hide();deleteMessageW_ch.setModal(false);" title="取消" style="cursor:pointer;" /></td></tr>
			<tr class="odd_mypm"><td>&nbsp;</td></tr>
		</table>
	</div>
</div>
<script type="text/javascript">
function customDeleteObject(){
	if($3("customData").value == ""){
		deleteObject();
	}else{
		eval($3("customData").value);
		$3("customData").value = "";
	}
}
</script>