	 var peopleId ;
	 var peopleName ;
	 var personLoadStr="";
	 var sepaStr= " ";
	 var  hdlFun="selPeople";
	 var temUserSelW_ch;
	 var seledGrid,selGrid;
	function temUserSelWin(passValId,passNameId,idSepaStr,cusHdl){
		importJs(conextPath+"/dhtmlx/grid/codebase/dhtmlxgrid.js");
		importJs(conextPath+"/dhtmlx/grid/codebase/dhtmlxgridcell.js");
		loadCSS(conextPath+"/dhtmlx/windows/codebase/skins/dhtmlxwindows_dhx_blue.css");
		importJs(conextPath + "/dhtmlx/windows/codebase/dhtmlxwindows.js");
		peopleId = passValId;
		peopleName = passNameId
		if(typeof idSepaStr!="undefined"){
			sepaStr = idSepaStr;
		}
		if(typeof cusHdl!="undefined"){
			hdlFun = cusHdl;
		}
		eval("$('selPeople_b').onclick  ="+hdlFun);
		temUserSelW_ch = initW_ch(temUserSelW_ch, "selPeopleDiv", true, 400, 300);
		temUserSelW_ch.setText("选择人员");
		initSelGrid();
		initSleEdPeople('NoFresh');
		temUserSelW_ch.bringToTop();
	}
	String.prototype.replaceAll  = function(oldText,newText){   
	    return this.replace(new RegExp(oldText,'g'),newText);   
	 };
	function initSleEdPeople(freshFlg){
		//处理可选人员
		if((personLoadStr==""&&freshFlg=="NoFresh")||freshFlg=='fresh'){
			var url = conextPath+"/testTaskManager/testTaskManagerAction!selectProMember.action";
			var ajaxResut =postSub(url, "");
			if(ajaxResut=="failed"){
	  			if(typeof hintMsg!="undefined"){
	  				hintMsg("加载数据发生错误");
	  			}else{
	  				showWarningMessage(true, "提醒", "加载数据发生错误");
	  			}
	  			return;
	  		}
			selGrid.clearAll();
			ajaxResut = ajaxResut.split("$")[1];
		    if(ajaxResut != ""&&ajaxResut!="failed"){
	   			ajaxResut = ajaxResut.replace(/[\r\n]/g, "");
	  			selGrid.parse(eval("(" + ajaxResut +")"), "json");
	  			personLoadStr=ajaxResut;
	  		}else if(ajaxResut == ""){
	  			selGrid.addRow("noData","0,,任务没关联任何资源!请先关联",0);
	  			
	  		}
	  	}else{
	  		selGrid.clearAll();
	  		selGrid.parse(eval("(" + personLoadStr +")"), "json");
	  	}
	  	if(freshFlg=='fresh'){
	  		for(var rowNum=0;rowNum<seledGrid.getRowsNum();rowNum++){
	  			selGrid.deleteRow(seledGrid.getRowId(rowNum));
	  		}
	  		return;
	  	}
	  	seledGrid.clearAll();
  		//只选择一模快时，要从可选中删除己选，并在己选中列出己选
		var seledPeopeIds = $(peopleId).value;
		var seledPeopeNames = $(peopleName).value;
		seledPeopeNames = seledPeopeNames.replaceAll("，",",");
		peopleIdArr = seledPeopeIds.split(sepaStr);
		peopleNameArr = seledPeopeNames.split(",");
		seledGrid.clearAll();
		if(seledPeopeIds==""||seledPeopeNames==""){
			return;
		}
		for(var i=0; i<peopleIdArr.length; i++){
			if(peopleNameArr[i]!=""){
				seledGrid.addRow2(peopleIdArr[i],"0,,"+peopleNameArr[i],i);
				selGrid.deleteRow(peopleIdArr[i]);
			}
		}			
	}
	function initSelGrid(){
	    if(typeof selGrid == "undefined"){
		    selGrid = new dhtmlXGridObject('selGridbox');
			seledGrid = new dhtmlXGridObject('seledGridbox');
			selGrid.setImagePath(conextPath+"/dhtmlx/grid/codebase/imgs/");
			seledGrid.setImagePath(conextPath+"/dhtmlx/grid/codebase/imgs/");
			selGrid.setHeader(",,备选人员--<span style='font-size:11px'>双击选择</span>");
			selGrid.setInitWidths("0,0,*");
	    	selGrid.setColAlign("left,left,left");
	    	selGrid.setColTypes("ch,ro,ro");
	   		selGrid.setColSorting("Str,int,str");
	   		selGrid.enableTooltips("false,false,true");
	    	selGrid.enableAutoHeight(true, 240);
	    	selGrid.setMultiselect(true);
	        selGrid.init();
	        selGrid.enableRowsHover(true, "red");
	    	selGrid.setSkin("light");
		    seledGrid.setHeader(",,己选人员--<span style='font-size:11px'>双击取消</span>");
			seledGrid.setInitWidths("0,0,*");
	    	seledGrid.setColAlign("left,left,left");
	    	seledGrid.setColTypes("ch,ro,ed");
	   		seledGrid.setColSorting("Str,int,str");
	   		seledGrid.enableTooltips("false,false,true");
	   		seledGrid.enableAutoHeight(true, 240);
	   		seledGrid.setMultiselect(true);
	        seledGrid.init();
	        seledGrid.enableRowsHover(true, "red");
	    	seledGrid.setSkin("light");	
	    	seledGrid.attachEvent("onRowDblClicked",function(rowId,cellInd,state){
				selGrid.addRow2(rowId,"0,,"+seledGrid.cells(rowId,2).getValue(),0);
				seledGrid.deleteRow(rowId);
				seledGrid.setSizes();
				//return true;
			});
	    	selGrid.attachEvent("onRowDblClicked",function(rowId,cellInd,state){
	    		if(rowId!="noData"){
				 seledGrid.addRow2(rowId,"0,,"+selGrid.cells(rowId,2).getValue(),0);
				 selGrid.deleteRow(rowId);
				 selGrid.setSizes();
				}
				//return true;
			});
	    }
	}
	function selPeople(){
		if(seledGrid.getRowsNum()==0){
			temUserSelW_ch.hide();
			temUserSelW_ch.setModal(false);
			$(peopleId).value = "";
			$(peopleName).value = "" ;
			return;
		}
		var allItems = seledGrid.getAllItemIds();
		var items = allItems.split(',');
		var ids = "";
		var names = "";
		for(var i=0; i<items.length;i++){
			ids = ids + sepaStr+items[i];
			names = names + ","+ seledGrid.cells(items[i], 2).getValue();
		}
		$(peopleId).value = ids.substring(1);
		$(peopleName).value = names.substring(1) ;
		temUserSelW_ch.hide();
		temUserSelW_ch.setModal(false);
	}
