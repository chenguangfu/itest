
<%@ page contentType="text/html; charset=UTF-8"%>
<%@taglib uri="/webwork" prefix="ww"%>
<%@taglib uri="/WEB-INF/pmButton.tld" prefix="pmTag"%>
<HTML>
	<HEAD>
		<TITLE>意见交流</TITLE>
		<META content="text/html; charset=UTF-8" http-equiv=Content-Type>
		<link rel="STYLESHEET" type="text/css"href="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgrid.css">
		<link rel="STYLESHEET" type="text/css"href="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgrid_skins.css">
		<link rel="STYLESHEET" type="text/css"href="<%=request.getContextPath()%>/dhtmlx/toolbar/codebase/skins/dhtmlxtoolbar_dhx_blue.css">
		<link rel='STYLESHEET' type='text/css'href='<%=request.getContextPath()%>/css/page2.css'>
		<script type="text/javascript"src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxcommon.js"></script>
		<script type="text/javascript"src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgrid.js"></script>
		<script type="text/javascript"src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgridcell.js"></script>
		<script type="text/javascript"src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/excells/dhtmlxgrid_excell_link.js"></script>
		<script type="text/javascript"src="<%=request.getContextPath()%>/dhtmlx/toolbar/codebase/dhtmlxtoolbar.js"></script>
		<script type="text/javascript"src="<%=request.getContextPath()%>/js/globalVariable.js"></script>
		<script type="text/javascript"src="<%=request.getContextPath()%>/js/commonFunction_top.js"></script>
		<script type="text/javascript"src="<%=request.getContextPath()%>/dhtmlx/windows/codebase/dhtmlxwindows.js"></script>
		<link rel="STYLESHEET" type="text/css"href="<%=request.getContextPath()%>/dhtmlx/windows/codebase/dhtmlxwindows.css">
		<link rel="STYLESHEET" type="text/css"href="<%=request.getContextPath()%>/dhtmlx/windows/codebase/skins/dhtmlxwindows_dhx_blue.css">
	</HEAD>
	<BODY  style="overflow-y:hidden;">
		<script type="text/javascript">
	var myName = "${session.currentUser.userInfo.uniqueName}";
	function styleChange(num){
	 	 	switch(num){
				case 1:
					$("msgInfoTab").className="jihuo";
					$("baseInfoTab").className="nojihuo";
					loadMsg();
					break;
				case 2:
					$("msgInfoTab").className="nojihuo";
					$("baseInfoTab").className="jihuo";
					 loadBaseInfo();
					break;
	 		}
	 } 
	 function loadMsg(){
	 	$("msgListDiv").style.display="";
	 	$("baseInfoDiv").style.display="none";
	 	parent.questW_ch.setDimension(850, 535);
	 }
	 var haveLoad = false;
	 function loadBaseInfo(){
	 	if(haveLoad){
			$("baseInfoDiv").style.display="";	 
	 		$("msgListDiv").style.display="none";	
	 		parent.questW_ch.setDimension(850, 500);
	 		return;	 	
	 	}
		var url = conextPath + "/bugManager/bugManagerAction!viewBugDetal.action?dto.isAjax=true&dto.loadType=2&dto.bug.bugId="+$("bugId").value;
		var ajaxRest = postSub(url,"");	 
		if(ajaxRest=="failed"){
			hintMsg('加载基本信息失败');
			return;
		}
		haveLoad=true;
		parent.questW_ch.setDimension(850, 500);
		ajaxRest = ajaxRest.replace(/[\r\n]/g, "");
		setBaseInfo(ajaxRest);
		$("baseInfoDiv").style.display="";	 
	 	$("msgListDiv").style.display="none";
	 	loadFCK();
	 	adjustTable("baseTable","odd");	
	}
	</script>
	<ww:hidden id="reSetMsgLink" name="dto.reSetMsgLink"></ww:hidden>
		<div align="center" id="tabSw">
			<table border="0" style="width:100%" cellpadding="0" cellspacing="0">
				<tr>
					<td class="jihuo" id="msgInfoTab" width="80" class="tdtxt"
						align="right">
						<strong><a onclick="styleChange(1)"
							href="javascript:void(0);">意见交流记录</a> </strong>
					</td>
					<td class="nojihuo" id="baseInfoTab" width="60" class="tdtxt"
						align="left">
						<strong><a onclick="styleChange(2)"
							href="javascript:void(0);">基本信息</a> </strong>
					</td>
					<td colspan="6" width="560">
						&nbsp;
					</td>
				</tr>
			</table>
		</div>
		<div id="msgListDiv" align="center"  valign="middle" style="background-color: #ffffff; width:100%;">
			<input type="hidden" id="listStr" name="listStr"
				value="${dto.listStr}" />
			<table width="100%" cellspacing="0" cellpadding="0">
				<tr>
					<td valign="top">
						<div id="toolbarObj"></div>
					</td>
				</tr>
				<tr>
					<td valign="top">
						<div id="gridbox"></div>
					</td>
				</tr>
				<tr>
					<td class="tdtxt" align="center" width="720" colspan="6">
						<a class="bluebtn"
							href="javascript:parent.questW_ch.setModal(false);parent.questW_ch.hide();"
							style="margin-left: 6px"><span> 返回</span>
						</a>
					</td>
				</tr>
			</table>
		</div>
		<div id="createDiv" align="center" class="cycleTask gridbox_light" style="border:0px;display:none">
		  <div class="objbox" style="overflow:auto;width:100%;">
			<ww:form theme="simple" method="post" id="createForm"
				name="createForm" namespace="" action="">
				<ww:hidden id="bugId" name="dto.shortMsg.bugId"></ww:hidden>
				<table class="obj row20px" cellspacing="0" align="center" cellpadding="0" border="0" width="100%">
					<tr class="ev_mypm">
						<td class="tdtxt" align="left" width="400" 
							colspan="4"></td>
					</tr>
					<tr class="odd_mypm">
						<td  align="left"style="color:#055A78;font-size:12px;font-weight: bold;color:red;border-right:0" width="100">
							我的意见 :
						</td>
						<td class="tdtxt" align="left" width="300" colspan="3">
						<div id="hintMsg" style="color: Blue;">
							&nbsp;
						</div>						
						</td>
					</tr>
					<tr class="ev_mypm">
						<td class="tdtxt" align="left" width="400" colspan="4">
							<textarea name="dto.shortMsg.message" id="questMsg" cols="55"
								rows="2">
							 </textarea>
						</td>
					</tr>
					<tr class="odd_mypm">
						<td style="color:#055A78;font-size:12px;font-weight: bold;color:red" align="left" width="400" colspan="4">
							发送给:
							<ww:select id="sendTo" name="dto.shortMsg.recipCd" cssClass="text_c"
								list="#{-1:'',10:'项目所有成员',1:'测试人员',3:'分析人员',4:'分配人员',5:'开发人员',7:'开发负责人'}"
								headerValue="1" cssStyle="width:95;">
							</ww:select>
						</td>
					</tr>
					<tr class="ev_mypm">
						<td class="tdtxt" align="left" width="400" colspan="4">
							<a class="bluebtn" href="javascript:cuW_ch.setModal(false);cuW_ch.hide();try{parent.collapseLayout('open');}catch(err){)} "
								style="margin-left: 6px"><span> 返回</span>
							</a> 
							<a class="bluebtn" href="javascript:sendBugMsg()"
								style="margin-left: 6px"><span> 确定</span>
							</a>
						</td>
					</tr>
					<tr class="odd_mypm">
						<td class="tdtxt" align="left" width="400" colspan="4">
							
						</td>
					</tr>
				</table>
			</ww:form>
		  </div>
		</div>
		<script type="text/javascript">
		if($("reSetMsgLink").value==1){
			parent.reSetLinkCol(9);
		}
		pmBar = new dhtmlXToolbarObject("toolbarObj");
		pmBar.setIconsPath(conextPath+"/dhtmlx/toolbar/images/");
		pmBar.addButton("new",0 , "", "new.gif");
		pmBar.setItemToolTip("new", "发表意见");
		pmBar.addButton("first", 1, "", "first.gif", "first.gif");
		pmBar.setItemToolTip("first", "第一页");
		pmBar.addButton("pervious",2, "", "pervious.gif", "pervious.gif");
		pmBar.setItemToolTip("pervious", "上一页");
		pmBar.addSlider("slider",3, 80, 1, 30, 1, "", "", "%v");
		pmBar.setItemToolTip("slider", "滚动条翻页");
		pmBar.addButton("next",4, "", "next.gif", "next.gif");
		pmBar.setItemToolTip("next", "下一页");
		pmBar.addButton("last",5, "", "last.gif", "last.gif");
		pmBar.setItemToolTip("last", "末页");
		pmBar.addInput("page",6, "", 25);
		pmBar.addText("pageMessage",7, "");
		pmBar.addText("pageSizeText",8, "每页");
		var opts = Array(Array('id1', 'obj', '10'), Array('id2', 'obj', '15'), Array('id3', 'obj', '20'));
		pmBar.addButtonSelect("pageP",9, "page", opts);
		pmBar.setItemToolTip("pageP", "每页记录数")
		pmBar.addText("pageSizeTextEnd",10, "条");
		pmGrid = new dhtmlXGridObject('gridbox');
		pmGrid.setImagePath(conextPath+"/dhtmlx/grid/codebase/imgs/");
	    pmGrid.setHeader("&nbsp;,序号,发送人,意见/问题,接收人,发送日期");
	    pmGrid.setInitWidths("0,40,100,*,100,100");
	    pmGrid.setColAlign("center,center,left,left,left,left");
	    pmGrid.setColTypes("ra,ro,ro,ro,ro,ro");
	    pmGrid.setColSorting("int,str,str,str,str,str");
		pmGrid.enableAutoHeight(true,400);
	    pmGrid.init();
	    pmGrid.enableTooltips("false,true,true,true,true,true");
	    pmGrid.setSkin("light");  	  
	    initGrid(pmGrid,"listStr");
	    pmGrid.attachEvent("onRowSelect",doOnSelect);  
	    function doOnSelect(rowId,index){
			this.setSelectedRow(rowId);
		}	    
	pmBar.attachEvent("onValueChange", function(id, pageNo) {
		pageAction(pageNo, pageSize);
		return;
	});
	pmBar.attachEvent("onEnter", function(id, value) {
		if(!isDigit(value, false)){
			pmBar.setValue("page", pageNo);
			return;
		}
		var pageNoTemp = parseInt(value);
		if(pageNoTemp < 1){
			pageNoTemp = 1;
		}else if(pageNoTemp > pageCount){
			pageNoTemp = pageCount;
		}
		pageAction(pageNoTemp, pageSize);
		return;
	});
	pmBar.attachEvent("onClick", function(id) {
		if(id == "new"){
			addInit();
		}else if(id == "first"){
			pageNo = 1;
			pageAction(pageNo, pageSize);
		}else if(id == "last"){
			pageNo = pageCount;
			pageAction(pageNo, pageSize);
		}else if(id == "next"){
			pageNo = pageNo +1
			pageAction(pageNo, pageSize);
		}else if(id == "pervious"){
			pageNo = pageNo -1
			pageAction(pageNo, pageSize);
		}else if(id == "id1"){
			if(pageSize==10){
				return;
			}
			pageSize = 10;
			pageNo = 1;
			pageAction(pageNo, pageSize);
		}else if(id == "id2"){
			if(pageSize==15){
				return;
			}
			pageSize = 15;
			pageNo = 1;
			pageAction(pageNo, pageSize);
		}else if(id == "id3"){
			if(pageSize==20){
				return;
			}
			pageSize = 20;
			pageNo = 1;
			pageAction(pageNo, pageSize);
		}else if(id == "id4"){
			if(pageSize==25){
				return;
			}
			pageSize = 25;
			pageNo = 1;
			pageAction(pageNo, pageSize);
		}else if(id == "id5"){
			if(pageSize==30){
				return;
			}
			pageSize = 30;
			pageNo = 1;
			pageAction(pageNo, pageSize);
		}
	});
	function addInit(){
		$('createForm').reset();
		$("hintMsg").innerHTML="";
		if(_isIE){
			cuW_ch=initW_ch(cuW_ch,  "createDiv", true,500, 150);
		}else{
			cuW_ch=initW_ch(cuW_ch,  "createDiv", true,480, 170);
		}
		cuW_ch.setText("发表新意见");
		cuW_ch.setModal(true);
		cuW_ch.bringToTop();
		$("questMsg").focus();
	}
	function sendBugMsg(){
		$("questMsg").value = $("questMsg").value.replace(/\s+$|^\s+/g,"");
		if($("questMsg").value==""){
			$("hintMsg").innerHTML="请填写意见";
			return;
		}else if(checkIsOverLong($("questMsg").value.trim(),100)){
			$("hintMsg").innerHTML="意见最多200个字符";
			return;
		}else if($("sendTo").value=="-1"){
			$("hintMsg").innerHTML="请选择发送对像";
			return;
		}
		var url = conextPath+"/bugManager/bugShortMsgAction!sendMsg.action?dto.isAjax=true";
		var ajaxRest = postSub(url,"createForm");
		if(ajaxRest=="failed"){
			$("hintMsg").innerHTML="发送失败";
		}else{
			cuW_ch.setModal(false);
			cuW_ch.hide();
			var sendToName = $("sendTo").options[$("sendTo").selectedIndex].text;
			pmGrid.addRow(ajaxRest.split("^")[0],"0,,"+myName+","+$("questMsg").value+","+sendToName+","+ajaxRest.split("^")[1],0);
			setRowNum(pmGrid);
		}
	}
	function pageAction(pageNo, pSize){
		if(pageNo>pageCount){
			pmBar.setValue("page", pageNo);
			return ;
		}
		purl = conextPath+"/bugManager/bugShortMsgAction!loadMsgList.action?dto.isAjax=true&dto.loadType=2&dto.shortMsg.bugId="+$("bugId").value +"&dto.pageNo="+ pageNo+"&dto.pageSize=" + pSize;
		var ajaxRest = postSub(purl,"");
		if(ajaxRest=="failed"){
			hintMsg("加载数据发生错误");
		}
		$("listStr").value=ajaxRest;
		initGrid(pmGrid,"listStr");	
   		return;
	}
	function initCufmsW(){
		cufmsW = new dhtmlXWindows();
		cufmsW.enableAutoViewport(true);
		cufmsW.setViewport(50, 50, 100, 100);
		cufmsW.setImagePath(conextPath+"/dhtmlx/windows/codebase/imgs/");
		return cufmsW;
	}
	function initW_ch(obj, divId, mode, w, h,wId){
		if((typeof obj != "undefined") && !obj.isHidden()){
			obj.setDimension(w, h);
			return obj;
		}else if((typeof obj != "undefined") && obj.isHidden()){
			obj.show();
			obj.bringToTop();
		}else{
			if(typeof cufmsW == "undefined"){
				cufmsW = initCufmsW();
			}
			obj = cufmsW.createWindow(divId, 110, 110, w, h);
			obj.attachObject(divId, false);
			hiddenB(obj, mode);
		}
		obj.setModal(mode);
		return obj;
	}  
	var oEditor,reProStep;
	function setBaseInfo(baseInfo){
		var updInfos = baseInfo.split("^");
		for(var i=0; i < updInfos.length; i++){
			var currInfo = updInfos[i].split("=");
			if(currInfo[1] != "null"){
				currInfo[1] =recovJsonKeyWord(currInfo[1]);
				var valueStr = "$('"+currInfo[0]+"').value = currInfo[1]";;
				eval(valueStr);
				if(currInfo[0]=="bugAntimodDate"&&currInfo[1]!=""){
					$("deadLineDateTr").style.display="";
				}else if(currInfo[0]=="geneCaseName"&&currInfo[1]!=""){
					$("geneCauseTxtTd").style.display="";
					$("geneCauseTd").style.display="";
				}else if(currInfo[0]=="reproPersent"&&currInfo[1]!=""){
					$("repropTxtTd").style.display="";
					$("repropTd").style.display="";
				}else if(currInfo[0]=="imphaName"&&currInfo[1]!=""){
					$("impPhaTdtxt").style.display="";
					$("impPhaTr").style.display="";
					$("impPhaTd").style.display="";
					parent.questW_ch.setDimension(850, 520);
				}else if(currInfo[0]=="currRemark"&&currInfo[1]!=""){
					$("remarkTr").style.display="";
				}else if(currInfo[0]=="reProStep"){
					reProStep=currInfo[1];
				}else if(currInfo[0]=="attachUrl"&&currInfo[1]!=""){
					$("currAttach").style.display="";
					$("currAttach").title=currInfo[1].substring(currInfo[1].indexOf("_")+1);			
				}else if(currInfo[0]=="versionLable"&&currInfo[1]!=""){
					$("verLableTd").innerHTML=currInfo[1];
				}else if(currInfo[0]=="currVer"&&currInfo[1]!=""){
					$("verValTd").innerHTML=currInfo[1];
				}	
			}
		}
	}
	function loadFCK(){
		if(typeof oEditor != "undefined"){
			oEditor.SetData(reProStep) ;	
			return;
		}
		importJs(conextPath+"/pmEditor/fckeditor.js");
    	var pmEditor = new FCKeditor('reProStep') ;
    	pmEditor.BasePath = conextPath+"/pmEditor/" ;
    	pmEditor.Height = 200;
    	pmEditor.ToolbarSet = "Basic" ; 
    	pmEditor.ToolbarStartExpanded=false;
    	pmEditor.ReplaceTextarea();
	}
	function FCKeditor_OnComplete(editorInstance){
		oEditor = FCKeditorAPI.GetInstance('reProStep') ;
		oEditor.SetData(reProStep) ;	
		return;
	}
	  </script>
		<div id="baseInfoDiv"align="center" class="cycleTask gridbox_light" style="border:0px;display:none">
		  <div class="objbox" style="overflow:auto;width:100%;">
		  <input type="hidden" id="versionLable" name="versionLable"/>
		   <input type="hidden" id="currVer" name="currVer"/>
			<ww:form theme="simple" method="post" id="baseForm" name="baseForm" namespace="" action="">
				<ww:hidden id="attachUrl" name="dto.bug.attachUrl"></ww:hidden>
				<table  border="0" id="baseTable" class="obj row20px" cellspacing="0" align="center" cellpadding="0" border="0" width="100%">
					<tr>
						<td width="80" class="rightM_center" align="right"  style="border-right:0">
							模块:
						</td>
						<td class="tdtxt" align="left" colspan="5" width="640"
							style="padding: 2 0 0 4;border-right:0;">
							<input type="text" id="modelName" Class="text_c" name="dto.moduleName" style="width:640;padding:2 0 0 4;"value="" readonly="true"/>
						</td>
					</tr>
					<tr>
						<td width="80" class="rightM_center" align="right"  style="border-right:0">
							当前状态:
						</td>
						<td class="tdtxt" align="left" width="160"
							style="padding: 2 0 0 4;border-right:0;">
						<input type="text" id="stateName" Class="text_c" name="dto.stateName" value="" readonly="true"/>
						</td>
						<td width="80" class="rightM_center" align="right"  style="border-right:0">
							作者:
						</td>
						<td class="tdtxt" align="left" width="160"
							style="padding: 2 0 0 4;border-right:0;">
						<input type="text" id="authorName" Class="text_c" name="authorName" value="" readonly="true"/>
						</td>
						<td colspan="2"></td>
					</tr>
					<tr id="testOwnerTr">
						<td width="80" class="rightM_center" align="right"  style="border-right:0">
							测试人员:
						</td>
						<td class="tdtxt" width="160" style="padding: 2 0 0 4;border-right:0;">
							<input type="text" id="testName" Class="text_c" name="testName" value="" readonly="true"/>
						</td>
						<td width="80" class="rightM_center" align="right"  style="border-right:0">
							开发人员:
						</td>
						<td class="tdtxt" width="160" style="padding: 2 0 0 4;border-right:0;">
							<input type="text" id="devName" Class="text_c" name="devName" value="" readonly="true"/>
						</td>
						<td colspan="2"></td>
					</tr>
					<tr id="deadLineDateTr" style="display:none">
						<td width="80" class="rightM_center" align="right"  style="border-right:0" nowrap>
							修改截止日期:
						</td>
						<td class="tdtxt" width="100" style="padding: 2 0 0 4;">
							<input type="text" id="bugAntimodDate" Class="text_c" name="bugAntimodDate" value="" readonly="true"/>
						</td>
						<td width="100" class="rightM_center" align="right"  style="border-right:0">
							工作量(小时):
						</td>
						<td class="tdtxt" width="80" style="padding: 2 0 0 4;border-right:0;">
							<input type="text" id="planAmendHour" Class="text_c" name="planAmendHour" value="" readonly="true"/>
						</td>
					</tr>
					<tr>
						<td width="80" class="rightM_center" align="right"  style="border-right:0">
							Bug描述:
						</td>
						<td class="tdtxt" colspan="5" width="640"
							style="padding: 2 0 0 4;border-right:0;">
							<input type="text" id="bugDesc" Class="text_c" style="width:640;padding:2 0 0 4;"name="bugDesc" value="" readonly="true"/>
						</td>
					</tr>
					<tr>
						<td width="80" class="rightM_center" align="right"  style="border-right:0">
							类型:
						</td>
						<td class="tdtxt" width="160" style="padding: 2 0 0 4;border-right:0;">
							<input type="text" id="bugTypeName" Class="text_c" name="dto.bug.bugType.typeName"
								style="width:160;padding:2 0 0 4;" readonly="true"/>
						</td>
						<td width="80" class="rightM_center" align="right"  style="border-right:0">
							等级:
						</td>
						<td class="tdtxt" width="160" style="padding: 2 0 0 4;border-right:0;">
							<input type="text" id="bugGradeName" Class="text_c" name="bugGradeName"style="width:160;padding:2 0 0 4;" readonly="true"/>
						</td>
						<td width="80" class="rightM_center" align="right"  style="border-right:0">
							再现平台:
						</td>
						<td class="tdtxt" width="160" style="padding: 2 0 0 4;border-right:0;">
							<input type="text" id="pltfomName" Class="text_c" name="pltfomName"style="width:160;padding:2 0 0 4;" readonly="true"/>
						</td>
					</tr>
					<tr>
						<td width="80" class="rightM_center" align="right"  style="border-right:0">
							来源:
						</td>
						<td class="tdtxt" width="160" style="padding: 2 0 0 4;border-right:0;">
							<input type="text" id="sourceName" Class="text_c" name="sourceName"style="width:160;padding:2 0 0 4;" readonly="true"/>
						</td>
						<td width="80" class="rightM_center" align="right"  style="border-right:0">
							时机:
						</td>
						<td class="tdtxt" width="160" style="padding: 2 0 0 4;border-right:0;">
							<input type="text" id="occaName" Class="text_c" name="occaName"style="width:160;padding:2 0 0 4;" readonly="true"/>
						</td>
						<td width="80"  class="rightM_center" align="right"  style="border-right:0">
						  <div id="geneCauseTxtTd" style="display:none;">
							引入原因:
						  </div>
						</td>
						<td class="tdtxt" width="160" style="border-right:0">
						  <div id="geneCauseTd" style="display:none;">
							<input type="text" id="geneCaseName" Class="text_c" name="geneCaseName"style="width:160;padding:2 0 0 4;" readonly="true"/>
						 <div>
						</td>
					</tr>
					<tr>
						<td width="80" class="rightM_center" align="right" style="border-right:0">
							优先级:
						</td>
						<td class="tdtxt" width="160" style="padding: 2 0 0 4;;border-right:0">
						<input type="text" id="priName" Class="text_c" name="priName"style="width:160;padding:2 0 0 4;" readonly="true"/>
						</td>
						<td width="80" class="rightM_center" align="right" style="border-right:0">
							频率:
						</td>
						<td class="tdtxt" width="160" style="padding: 2 0 0 4;border-right:0">
						<input type="text" id="bugFreqName" Class="text_c" name="bugFreqName"style="width:160;padding:2 0 0 4;" readonly="true"/>
						</td>
						<td width="80"  class="rightM_center" align="right" >
							<div id="repropTxtTd" style="display:none">
							再现比例:
							</div>
						</td>
						<td  class="tdtxt" align="left" style="width: 160;border-right:0">
						  <div id="repropTd" style="display:none;">
						 <input type="text" id="reproPersent" Class="text_c" name="reproPersent"style="width:160;padding:2 0 0 4;" readonly="true"/>
						  </div>
						</td>
					</tr>
					<tr id="impPhaTr" style="display:none">
						<td id="impPhaTdtxt" width="80" class="rightM_center" align="right" style="display:none;;border-right:0">
							引入阶段:
						</td>
						<td id="impPhaTd" class="tdtxt" colspan="5" width="640"
							style="padding: 2 0 0 4;display:none;;border-right:0">
						<input type="text" id="imphaName" Class="text_c" name="imphaName"style="width:160;padding:2 0 0 4;display:none" readonly="true"/>	
						</td>
					</tr>
					<tr>
						<td width="80" class="rightM_center" align="right" style="border-right:0"nowrap>
							再现步骤:
						</td>
						<td width="640" class="tdtxt" colspan="5"
							style="background-color: #f7f7f7; hight: 200; width: 640;border-right:0">
							<textarea name="dto.bug.reProStep" id="reProStep" cols="50"
								rows="30" style="width:640;hight:200;padding:2 0 0 4;" Class="text_c">
   			      </textarea>
						</td>
					</tr>
					<tr>
						<td class="rightM_center" align="right" width="80" style="border-right:0">
							发现日期:
						</td>
						<td class="tdtxt" align="left" width="160"
							style="width: 160; padding: 2 0 0 4;border-right:0">
						<input type="text" id="reptDate" Class="text_c" name="reptDate"style="width:160;padding:2 0 0 4;" readonly="true"/>	
						</td>
						<td class="rightM_center" align="right" " width="80" style="border-right:0">
							发现版本:
						</td>
						<td class="tdtxt" align="left" width="160"
							style="width: 160; padding: 2 0 0 4;border-right:0">
							<input type="text" id="bugReptVer" Class="text_c" name="bugReptVer"style="width:160;padding:2 0 0 4;" readonly="true"/>	
						</td>
						<td id="verLableTd" class="rightM_center" align="right" width="80" style="border-right:0">
						</td>
						<td id="verValTd"  class="tdtxt" align="left" width="160"
							style="width: 160; padding: 2 0 0 4;border-right:0">
						</td>
					</tr>
					<tr style="display:none" id="remarkTr">
						<td width="80" class="rightM_center" align="right" style="border-right:0">
							备注:
						</td>
						<td class="tdtxt" align="left" colspan="5" width="640"
							style="padding: 2 0 0 4;border-right:0">
						<input type="text" id="currRemark" Class="text_c" name="currRemark" style="width:640;padding:2 0 0 4;"value="" readonly="true"/>
						</td>
					</tr>
					  <tr class="odd_mypm">
					  	 <td class="rightM_center" width="90" id="attachTd2" style="border-right:0">
					  	 附件:
					  	 </td>
					    <td class="dataM_left"  colspan="5" width="630" style="border-right:0">
							<img src="<%=request.getContextPath()%>/images/button/attach.gif" id="currAttach" style="display:none" alt="当前附件" title="" onclick="openAtta()"/>
					    </td>
					  </tr>	
				</table>
			</ww:form>
		  </div>
		</div>
		<ww:include value="/jsp/common/dialog.jsp"></ww:include>
		<ww:include value="/jsp/common/downLoad.jsp"></ww:include>
		<link rel="STYLESHEET" type="text/css"href="<%=request.getContextPath()%>/dhtmlx/windows/codebase/dhtmlxwindows.css">
		<link rel="STYLESHEET" type="text/css"href="<%=request.getContextPath()%>/dhtmlx/windows/codebase/skins/dhtmlxwindows_dhx_blue.css">
	</BODY>
</HTML>
