
<%@ page contentType="text/html; charset=UTF-8"%>
<%@taglib uri="/webwork" prefix="ww"%>
<%@taglib uri="/WEB-INF/pmButton.tld" prefix="pmTag"%>
<HTML>
	<HEAD>
		<TITLE>关联用例</TITLE>
		<META content="text/html; charset=UTF-8" http-equiv=Content-Type>
		<link rel="STYLESHEET" type="text/css"href="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgrid.css">
		<link rel="STYLESHEET" type="text/css"href="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgrid_skins.css">
		<link rel="STYLESHEET" type="text/css"href="<%=request.getContextPath()%>/dhtmlx/toolbar/codebase/skins/dhtmlxtoolbar_dhx_blue.css">
		<script type="text/javascript"src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxcommon.js"></script>
		<script type="text/javascript"src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgrid.js"></script>
		<script type="text/javascript"src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgridcell.js"></script>
		<script type="text/javascript"src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/excells/dhtmlxgrid_excell_link.js"></script>
		<script type="text/javascript"src="<%=request.getContextPath()%>/dhtmlx/toolbar/codebase/dhtmlxtoolbar.js"></script>
		<script type="text/javascript"src="<%=request.getContextPath()%>/js/globalVariable.js"></script>
		<script type="text/javascript"src="<%=request.getContextPath()%>/js/commonFunction_top.js"></script>
		<link rel='STYLESHEET' type='text/css'href='<%=request.getContextPath()%>/css/page2.css'>
	</HEAD>
	<BODY bgcolor="white" style="overflow-y:hidden;">
	<script type="text/javascript">
	<pmTag:priviChk urls="caseManagerAction!exeCase" varNames="canExe"/>
	var testCaseIds = "${dto.testCaseIds}";
	function styleChange(num){
	 	 	switch(num){
				case 1:
					$("caseInfoTab").className="jihuo";
					$("baseInfoTab").className="nojihuo";
					loadCase();
					break;
				case 2:
					$("caseInfoTab").className="nojihuo";
					$("baseInfoTab").className="jihuo";
					 loadBaseInfo();
					break;
	 		}
	 } 
	 function loadCase(){
	 	$("caseListDiv").style.display="";
	 	$("baseInfoDiv").style.display="none";
	 	parent.relCaseW_ch.setDimension(850, 535);
	 }
	 var haveLoad = false;
	 function loadBaseInfo(){
	 	if(haveLoad){
			$("baseInfoDiv").style.display="";	 
	 		$("caseListDiv").style.display="none";	
	 		parent.relCaseW_ch.setDimension(850, 500);
	 		return;	 	
	 	}
		var url = conextPath + "/bugManager/bugManagerAction!viewBugDetal.action?dto.isAjax=true&dto.loadType=2&dto.bug.bugId="+$("bugId").value;
		var ajaxRest = postSub(url,"");	 
		if(ajaxRest=="failed"){
			hintMsg('加载基本信息失败');
			return;
		}
		parent.relCaseW_ch.setDimension(850, 500);
		haveLoad=true;
		ajaxRest = ajaxRest.replace(/[\r\n]/g, "");
		setBaseInfo(ajaxRest);
		$("baseInfoDiv").style.display="";	 
	 	$("caseListDiv").style.display="none";
	 	adjustTable("baseTable","odd");
	 	loadFCK();	
	}
	</script>
		<div align="center" id="tabSw">
			<table border="0" style="width:100%" cellpadding="0" cellspacing="0">
				<tr>
					<td class="jihuo" id="caseInfoTab" width="80" class="tdtxt"
						align="right">
						<strong><a onclick="styleChange(1)"
							href="javascript:void(0);">用例信息</a> </strong>
					</td>
					<td class="nojihuo" id="baseInfoTab" width="60" class="tdtxt"
						align="left">
						<strong><a onclick="styleChange(2)"
							href="javascript:void(0);">基本信息</a> </strong>
					</td>
					<td colspan="6" width="560">
						&nbsp;
					</td>
				</tr>
			</table>
		</div>
		<div id="caseListDiv" align="center"  valign="middle" style="background-color: #ffffff; width:100%;">
			<input type="hidden" id="listStr" name="listStr" value="${dto.listStr}"/>
			<table width="100%" cellspacing="0" cellpadding="0">
				<tr>
					<td valign="top">
						<div id="toolbarObj"></div>
					</td>
				</tr>
				<tr>
					<td valign="top">
						<div id="gridbox"></div>
					</td>
				</tr>
				<tr>
					<td class="tdtxt" align="center" width="720" colspan="6">
						<a class="bluebtn"
							href="javascript:parent.relCaseW_ch.setModal(false);parent.relCaseW_ch.hide();try{parent.collapseLayout('open');}catch(err){}"
							style="margin-left: 6px"><span> 返回</span>
						</a>
						<a class="bluebtn" id="saveRela"href="javascript:relaCase();"
							style="margin-left: 6px;;display:none""><span> 关联</span>
						 </a>
						<a class="bluebtn" id="upRela"href="javascript:relaCase();"
							style="margin-left: 6px;display:none"><span> 更新关联</span>
						 </a>
					</td>
				</tr>
			</table>
		</div>
		<div id="createDiv" align="center" class="cycleTask gridbox_light" style="border:0px;display:none">
		  <div class="objbox" style="overflow:auto;width:100%;">
			<ww:hidden id="moduleIdTmp" name="dto.moduleId"></ww:hidden>
			<input type="hidden" id="initOpd" name="initOpd"/>
	  		<ww:hidden id="isReview" name="dto.isReview"></ww:hidden>
	  		<ww:hidden id="isTestLeader" name="dto.isTestLeader"></ww:hidden>
	  		<ww:hidden id="testCaseIds" name="dto.testCaseIds"></ww:hidden>
	  		<input type="hidden" id="currVer" name="currVer" value=""/>
	  		<input type="hidden" id="versionLable" name="versionLable"/>
			<ww:form theme="simple" method="post" id="createForm" enctype="multipart/form-data"
				name="createForm" namespace="" action="">
				<ww:hidden id="mdPath" name="dto.mdPath"></ww:hidden>
				<ww:hidden id="bugId" name="dto.bugId"></ww:hidden>
			    <ww:hidden id="moduleId" name="dto.testCaseInfo.moduleId"></ww:hidden>
			    <ww:hidden id="createrId" name="dto.testCaseInfo.createrId"></ww:hidden>
			    <ww:hidden id="testCaseId" name="dto.testCaseInfo.testCaseId"></ww:hidden>
			    <ww:hidden id="isReleased" name="dto.testCaseInfo.isReleased"></ww:hidden>
				<ww:hidden id="creatdate" name="dto.testCaseInfo.creatdate"></ww:hidden>
				<ww:hidden id="attachUrl" name="dto.testCaseInfo.attachUrl"></ww:hidden>
				<ww:hidden id="auditId" name="dto.testCaseInfo.auditId"></ww:hidden>
			    <ww:hidden id="testStatus" name="dto.testCaseInfo.testStatus"></ww:hidden>
			    <ww:hidden id="testData" name="dto.testCaseInfo.testData"></ww:hidden>
			    <ww:hidden id="moduleNum" name="dto.testCaseInfo.moduleNum"></ww:hidden>
			    <ww:hidden id="remark" name="dto.testCaseInfo.remark"></ww:hidden>
				<ww:hidden id="taskId" name="dto.testCaseInfo.taskId" value=""></ww:hidden>
			    <ww:hidden id="expResultOld" ></ww:hidden>
			    <table id="createTable"class="obj row20px" cellspacing="0" align="center" cellpadding="0" border="0" width="100%">
    			  <tr>
    			    <td colspan="5" class="tdtxt" align="center" style="border-right:0">
    			      <div id="cUMTxt" align="center" style="color: Blue; padding: 2px"></div>
    			    </td>
    			  </tr>
    			  <tr>
    			    <td width="50" class="rightM_center" align="right"  style="border-right:0;color:red;">类型:</td>
    			    <td  class="tdtxt" width="75">
    			      <select id="caseTypeId" Class="text_c"name="dto.testCaseInfo.caseTypeId" style="width:75" readonly="true">
    			       <option   value="-1">请选择</option> 	        
    			      </select>
    			    </td>
    			    <td  width="50" class="rightM_center" align="right"  style="border-right:0;color:red;" nowrap>优先级:</td>
    			    <td  class="tdtxt" width="75">
    			      <select id="priId" Class="text_c"name="dto.testCaseInfo.priId" style="width: 75">
    			       <option   value="-1">请选择</option> 
    			      </select>
    			    </td>
    			    <td  class="rightM_center" align="right"  style="border-right:0" nowrap>&nbsp;执行成本:
    			       <ww:textfield id="weight" name="dto.testCaseInfo.weight" cssClass="text_c" cssStyle="width:70;padding:2 0 0 4;" onkeypress="javascript:return numChk(event);"></ww:textfield>
    			       <font color="Blue">一个成本单位代表5分钟</font> 
    			    </td>
    			  </tr>
    			  <tr>
    			    <td width="90" class="rightM_center" align="right"  style="border-right:0;color:red;">用例描述:</td>
    			    <td  class="tdtxt" colspan="4">
    			      <ww:textfield id="testCaseDes" cssClass="text_c" name="dto.testCaseInfo.testCaseDes" cssStyle="width:560;padding:2 0 0 4;" ></ww:textfield>
    			    </td>
    			  </tr>
    			  <tr>
    			    <td width="90" class="rightM_center" align="right"  style="border-right:0;color:red;">过程及数据:</td>
    			    <td width="570" class="tdtxt" colspan="4" style="background-color: #f7f7f7;">
    			      <textarea name="dto.testCaseInfo.operDataRichText" id="operDataRichText" cols="50" rows="15" value="liuyg"style="width:570;padding:2 0 0 4;">
    			        <ww:property value="dto.testCaseInfo.operDataRichText" escape="false"/>
    			      </textarea>
    			    </td>
    			  </tr>
    			  <tr>
    			    <td width="90" class="rightM_center" align="right"  style="border-right:0">预期结果:</td>
    			    <td  class="tdtxt" colspan="4">
    			      <textarea id="expResult" Class="text_c"name="dto.testCaseInfo.expResult" onMouseOver="select()" onblur="javascript:if(this.value==''){this.value=$('expResultOld').value;}"  style="width:560;padding:2 0 0 4;" cols="100" rows="3">
    			      </textarea>
    			    </td>
    			  </tr>
				</ww:form>
			    <ww:form enctype="multipart/form-data" theme="simple" name="fileform" id="fileform" action="" method="post" target="target_upload">
				  <tr class="odd_mypm">
				  	 <td class="rightM_center" width="90" id="attachTd">
				  	 附件/插图片:
				  	 </td>
				    <td class="dataM_left"  colspan="4" width="570" style="border-right:0">
						<input name="currUpFile" id="currUpFile" type="file" style="padding:2 0 0 4;width:210" Class="text_c">
						<img src="<%=request.getContextPath()%>/images/button/attach.gif" style="display:none"id="currAttach" alt="当前附件" title="打开当前附件" onclick="openAtta()" />
						<img src="<%=request.getContextPath()%>/dhtmlx/toolbar/images/img_insert.png" id="insertImg" alt="当前位置插入图片" title="当前位置插入图片" onclick="upLoadAndSub('saveCase','subChk',1,caseEditor)" />
				    </td>
				  </tr>	
				  <tr class="ev_mypm">
				    <td width="90" align="right" >&nbsp;</td>
					<td width="570" style="width:200;padding:2 0 0 4;" colspan="4"><div id="upStatusBar"></div></td>		  
				  </tr>		
			  </ww:form>	
 		  	 <iframe id="target_upload" name="target_upload" src="" frameborder="0" scrolling="no" width="0" height="0"></iframe>
				<tr>
					<td class="tdtxt" align="left" width="650" colspan="5">
						<a class="bluebtn"href="javascript:eraseAttach('eraseAllImg');dW_ch.setModal(false);dW_ch.hide();"
						style="margin-left: 6px"><span> 返回</span>
					    </a>
						<a class="bluebtn" id="saveBtn"href="javascript:upLoadAndSub('saveCase','subChk',0,caseEditor)"
						style="margin-left: 6px;display:none"><span> 确定</span>
					    </a>
					    
					    <a class="bluebtn" href="javascript:void(0);" id="reSetAttaBtn" onclick="$('currUpFile').value='';if(_isIE)$('currUpFile').outerHTML=$('currUpFile').outerHTML" ><span>取消选取的文件</span> </a>
					</td>
				</tr>
				<tr>
					<td class="tdtxt" align="left"  width="650" colspan="5"></td>
				</tr>
    		</table>
    		</div>
		</div>
	<script type="text/javascript">
	pmBar = new dhtmlXToolbarObject("toolbarObj");
	pmBar.setIconsPath(conextPath+"/dhtmlx/toolbar/images/");
	pmBar.addButton("new",0 , "", "new.gif");
	pmBar.setItemToolTip("new", "新增用例");
	pmBar.addButton("first", 1, "", "first.gif", "first.gif");
	pmBar.setItemToolTip("first", "第一页");
	pmBar.addButton("pervious",2, "", "pervious.gif", "pervious.gif");
	pmBar.setItemToolTip("pervious", "上一页");
	pmBar.addSlider("slider",3, 80, 1, 30, 1, "", "", "%v");
	pmBar.setItemToolTip("slider", "滚动条翻页");
	pmBar.addButton("next",4, "", "next.gif", "next.gif");
	pmBar.setItemToolTip("next", "下一页");
	pmBar.addButton("last",5, "", "last.gif", "last.gif");
	pmBar.setItemToolTip("last", "末页");
	pmBar.addInput("page",6, "", 25);
	pmBar.addText("pageMessage",7, "");
	pmBar.addText("pageSizeText",8, "每页");
	var opts = Array(Array('id1', 'obj', '10'), Array('id2', 'obj', '15'), Array('id3', 'obj', '20'));
	pmBar.addButtonSelect("pageP",9, "page", opts);
	pmBar.setItemToolTip("pageP", "每页记录数")
	pmBar.addText("pageSizeTextEnd",10, "条");
	pmGrid = new dhtmlXGridObject('gridbox');
	pmGrid.setImagePath(conextPath+"/dhtmlx/grid/codebase/imgs/");
    pmGrid.setHeader("&nbsp;,编号,用例描述,状态,类型,优先级");
    pmGrid.setInitWidths("40,80,*,80,100,100");
    pmGrid.setColAlign("center,left,left,left,left,left");
    pmGrid.setColTypes("ch,ro,ro,ro,ro,ro");
    pmGrid.setColSorting("int,str,str,str,str,str");
	pmGrid.enableAutoHeight(true,400);
	pmGrid.enableRowsHover(true, "red");
    pmGrid.init();
    pmGrid.enableTooltips("false,true,true,true,true,true");
    pmGrid.setSkin("light");  	  
    initGrid(pmGrid,"listStr",pmBar,"noSetRowNum");
    pmGrid.attachEvent("onRowSelect",doOnSelect); 
    pmGrid.attachEvent("onCheck",doOnCheck);
    function doOnSelect(rowId,index){
		this.setSelectedRow(rowId);
	}	
    function doOnCheck(rowId,cellInd,state){
    	if(pmGrid.cells(rowId,3).getValue()=="待审核"){
    		pmGrid.cells(rowId, 0).setValue(false);
    		hintMsg("待审核不能关联");
    		return false;
    	}
		this.setSelectedRow(rowId);
		if($("testCaseIds").value!=""){
			if(state){
				$("testCaseIds").value+=" "+rowId;
			}else{
				var idTmp = $("testCaseIds").value.split(rowId);
				$("testCaseIds").value =idTmp[0].replace(/\s+$|^\s+/g,"")+" "+idTmp[1].replace(/\s+$|^\s+/g,"");
			}			
		}else if($("testCaseIds").value==""&&state){
			$("testCaseIds").value=rowId;
		}
		return true;
	}  
	loadLink();
	function loadLink(){
		for(var i = 0; i <pmGrid.getRowsNum(); i++){
			pmGrid.cells2(i,1).cell.innerHTML="<a href=\"javascript:detalViewInit('view')\" title='用例明细'>"+getTttle2(i,1)+"</a>";
			if($("testCaseIds").value!=""&&$("testCaseIds").value.indexOf(pmGrid.getRowId(i))>=0){
				pmGrid.cells(pmGrid.getRowId(i),0).setValue(true);
			}
		}
		sw2Link();
	}
	function getTttle2(rowNum,colIn){
		return pmGrid.cells2(rowNum,colIn).getValue();
		
	}
	function colTypeReset(){
		pmGrid.setColTypes("ch,ro,ro,ro,ro,ro");
	}
	function sw2Link(){
		pmGrid.setColTypes("ch,link,ro,ro,ro,ro");
	}
	if($("testCaseIds").value==""){
		$("saveRela").style.display="";
		$("upRela").style.display="none";
	}else{
		$("saveRela").style.display="none";
		$("upRela").style.display="";	
	}
	var oEditor,reProStep,caseEditor,fckLoadName,pmEditor,pmEditor1;
	function loadFCK(){
		fckLoadName = "bug";
		if(typeof oEditor != "undefined"){
			oEditor.SetData(reProStep) ;	
			return;
		}
		importJs(conextPath+"/pmEditor/fckeditor.js");
    	pmEditor = new FCKeditor('reProStep') ;
    	pmEditor.BasePath = conextPath+"/pmEditor/" ;
    	pmEditor.Height = 250;
    	pmEditor.ToolbarSet = "Basic" ; 
    	pmEditor.ToolbarStartExpanded=false;
    	pmEditor.ReplaceTextarea();
	}
	function loadCaseFCK(){
		fckLoadName = "caseFlg";
		if(typeof caseEditor != "undefined"){
			if($("testCaseId").value!=""){
				caseEditor.SetData($("initOpd").value);
			}else{
				caseEditor.SetData("<strong>" +$("mdPath").value+"</strong>:");
			}
			return;
		}
		importJs(conextPath+"/pmEditor/fckeditor.js");
    	pmEditor1 = new FCKeditor('operDataRichText') ;
    	pmEditor1.BasePath = conextPath+"/pmEditor/" ;
    	pmEditor1.Height = 200;
    	pmEditor1.ToolbarSet = "Basic" ; 
    	pmEditor1.ToolbarStartExpanded=false;
    	pmEditor1.ReplaceTextarea();
	}
	function FCKeditor_OnComplete(editorInstance){
		if(fckLoadName=="caseFlg"){
			caseEditor = FCKeditorAPI.GetInstance('operDataRichText') ;
			if($("testCaseId").value!=""){
				caseEditor.SetData($("initOpd").value);
			}else{
				caseEditor.SetData("<strong>" +$("mdPath").value+"</strong>:");
			}		
		}else{
			oEditor = FCKeditorAPI.GetInstance('reProStep') ;
			oEditor.SetData(reProStep);			
		}
		return;
	}
	  </script>
		<div id="baseInfoDiv"align="center" class="cycleTask gridbox_light" style="border:0px;display:none">
		  <div class="objbox" style="overflow:auto;width:100%;">
			<ww:form theme="simple" method="post" id="baseForm" name="baseForm"
				namespace="" action="">
				<table  border="0" id="baseTable" class="obj row20px" cellspacing="0" align="center" cellpadding="0" border="0" width="100%">
					<tr>
						<td width="80" class="rightM_center" align="right"  style="border-right:0">
							测试需求:
						</td>
						<td class="tdtxt" align="left" colspan="5" width="640"
							style="padding: 2 0 0 4;border-right:0;">
							<input type="text" id="modelName" Class="text_c" name="dto.moduleName" style="width:640;padding:2 0 0 4;"value="" readonly="true"/>
						</td>
					</tr>
					<tr>
						<td width="80" class="rightM_center" align="right"  style="border-right:0">
							当前状态:
						</td>
						<td class="tdtxt" align="left" width="160"
							style="padding: 2 0 0 4;border-right:0;">
						<input type="text" id="stateName" Class="text_c" name="dto.stateName" value="" readonly="true"/>
						</td>
						<td width="80" class="rightM_center" align="right"  style="border-right:0">
							作者:
						</td>
						<td class="tdtxt" align="left" width="160"
							style="padding: 2 0 0 4;border-right:0;">
						<input type="text" id="authorName" Class="text_c" name="authorName" value="" readonly="true"/>
						</td>
						<td colspan="2"></td>
					</tr>
					<tr id="testOwnerTr">
						<td width="80" class="rightM_center" align="right"  style="border-right:0">
							测试人员:
						</td>
						<td class="tdtxt" width="160" style="padding: 2 0 0 4;border-right:0;">
							<input type="text" id="testName" Class="text_c" name="testName" value="" readonly="true"/>
						</td>
						<td width="80" class="rightM_center" align="right"  style="border-right:0">
							开发人员:
						</td>
						<td class="tdtxt" width="160" style="padding: 2 0 0 4;border-right:0;">
							<input type="text" id="devName" Class="text_c" name="devName" value="" readonly="true"/>
						</td>
						<td colspan="2"></td>
					</tr>
					<tr id="deadLineDateTr" style="display:none">
						<td width="80" class="rightM_center" align="right"  style="border-right:0" nowrap>
							修改截止日期:
						</td>
						<td class="tdtxt" width="100" style="padding: 2 0 0 4;">
							<input type="text" id="bugAntimodDate" Class="text_c" name="bugAntimodDate" value="" readonly="true"/>
						</td>
						<td width="100" class="rightM_center" align="right"  style="border-right:0">
							工作量(小时):
						</td>
						<td class="tdtxt" width="80" style="padding: 2 0 0 4;border-right:0;">
							<input type="text" id="planAmendHour" Class="text_c" name="planAmendHour" value="" readonly="true"/>
						</td>
					</tr>
					<tr>
						<td width="80" class="rightM_center" align="right"  style="border-right:0">
							Bug描述:
						</td>
						<td class="tdtxt" colspan="5" width="640"
							style="padding: 2 0 0 4;border-right:0;">
							<input type="text" id="bugDesc" Class="text_c" style="width:640;padding:2 0 0 4;"name="bugDesc" value="" readonly="true"/>
						</td>
					</tr>
					<tr>
						<td width="80" class="rightM_center" align="right"  style="border-right:0">
							类型:
						</td>
						<td class="tdtxt" width="160" style="padding: 2 0 0 4;border-right:0;">
							<input type="text" id="bugTypeName" Class="text_c" name="dto.bug.bugType.typeName"
								style="width:160;padding:2 0 0 4;" readonly="true"/>
						</td>
						<td width="80" class="rightM_center" align="right"  style="border-right:0">
							等级:
						</td>
						<td class="tdtxt" width="160" style="padding: 2 0 0 4;border-right:0;">
							<input type="text" id="bugGradeName" Class="text_c" name="bugGradeName"style="width:160;padding:2 0 0 4;" readonly="true"/>
						</td>
						<td width="80" class="rightM_center" align="right"  style="border-right:0">
							再现平台:
						</td>
						<td class="tdtxt" width="160" style="padding: 2 0 0 4;border-right:0;">
							<input type="text" id="pltfomName" Class="text_c" name="pltfomName"style="width:160;padding:2 0 0 4;" readonly="true"/>
						</td>
					</tr>
					<tr>
						<td width="80" class="rightM_center" align="right"  style="border-right:0">
							来源:
						</td>
						<td class="tdtxt" width="160" style="padding: 2 0 0 4;border-right:0;">
							<input type="text" id="sourceName" Class="text_c" name="sourceName"style="width:160;padding:2 0 0 4;" readonly="true"/>
						</td>
						<td width="80" class="rightM_center" align="right"  style="border-right:0">
							时机:
						</td>
						<td class="tdtxt" width="160" style="padding: 2 0 0 4;border-right:0;">
							<input type="text" id="occaName" Class="text_c" name="occaName"style="width:160;padding:2 0 0 4;" readonly="true"/>
						</td>
						<td width="80"  class="rightM_center" align="right"  style="border-right:0">
							<div id="geneCauseTxtTd" style="display:none;">
							引入原因:
							</div>&nbsp;
						</td>
						<td class="tdtxt"  width="160" style="border-right:0">
							<div id="geneCauseTd" style="display:none" > 
							<input type="text" id="geneCaseName" Class="text_c" name="geneCaseName"style="width:160;padding:2 0 0 4;" readonly="true"/>
							</div>&nbsp;
						</td>
					</tr>
					<tr>
						<td width="80" class="rightM_center" align="right" style="border-right:0">
							优先级:
						</td>
						<td class="tdtxt" width="160" style="padding: 2 0 0 4;;border-right:0">
						<input type="text" id="priName" Class="text_c" name="priName"style="width:160;padding:2 0 0 4;" readonly="true"/>
						</td>
						<td width="80" class="rightM_center" align="right" style="border-right:0">
							频率:
						</td>
						<td class="tdtxt" width="160" style="padding: 2 0 0 4;border-right:0">
						<input type="text" id="bugFreqName" Class="text_c" name="bugFreqName"style="width:160;padding:2 0 0 4;" readonly="true"/>
						</td>
						<td width="80"  class="rightM_center" align="right" >
							<div id="repropTxtTd" style="display:none">
							再现比例:
							</div>
						</td>
						<td  class="tdtxt" align="left" style="width: 160; padding: 2 0 0 4;border-right:0">
							<div id="repropTd"style="display:none">
								<input type="text" id="reproPersent" Class="text_c" name="reproPersent"style="width:160;padding:2 0 0 4;" readonly="true"/>
							</div>
						</td>
					</tr>
					<tr id="impPhaTr" style="display:none">
						<td id="impPhaTdtxt" width="80" class="rightM_center" align="right" style="display:none;;border-right:0">
							引入阶段:
						</td>
						<td id="impPhaTd" class="tdtxt" colspan="5" width="640"
							style="padding: 2 0 0 4;display:none;;border-right:0">
						<input type="text" id="imphaName" Class="text_c" name="imphaName"style="width:160;padding:2 0 0 4;display:none" readonly="true"/>	
						</td>
					</tr>
					<tr>
						<td width="80" class="rightM_center" align="right" style="border-right:0"nowrap>
							再现步骤:
						</td>
						<td width="640" class="tdtxt" colspan="5"
							style="background-color: #f7f7f7; hight: 200; width: 640;border-right:0">
							<textarea name="dto.bug.reProStep" id="reProStep" cols="50"
								rows="30" style="width:640;hight:200;padding:2 0 0 4;" Class="text_c">
   			      </textarea>
						</td>
					</tr>
					<tr>
						<td class="rightM_center" align="right" width="80" style="border-right:0">
							发现日期:
						</td>
						<td class="tdtxt" align="left" width="160" style="width: 160; padding: 2 0 0 4;border-right:0">
						<input type="text" id="reptDate" Class="text_c" name="reptDate"style="width:160;padding:2 0 0 4;" readonly="true"/>	
						</td>
						<td class="rightM_center" align="right" " width="80" style="border-right:0">
							发现版本:
						</td>
						<td class="tdtxt" align="left" width="160"
							style="width: 160; padding: 2 0 0 4;border-right:0">
							<input type="text" id="bugReptVer" Class="text_c" name="bugReptVer"style="width:160;padding:2 0 0 4;" readonly="true"/>	
						</td>
						<td id="verLableTd" class="rightM_center" align="right" width="80" style="border-right:0">
						</td>
						<td id="verValTd"  class="tdtxt" align="left" width="160"
							style="width: 160; padding: 2 0 0 4;border-right:0">
						</td>
					</tr>
					<tr style="display:none" id="remarkTr">
						<td width="80" class="rightM_center" align="right" style="border-right:0">
							备注:
						</td>
						<td class="tdtxt" align="left" colspan="5" width="640"
							style="padding: 2 0 0 4;border-right:0">
						<input type="text" id="currRemark" Class="text_c" name="currRemark" style="width:640;padding:2 0 0 4;"value="" readonly="true"/>
						</td>
					</tr>
					  <tr class="odd_mypm">
					  	 <td class="rightM_center" width="90" id="attachTd2" style="border-right:0">
					  	 附件:
					  	 </td>
					    <td class="dataM_left"  colspan="5" width="630" style="border-right:0">
							<img src="<%=request.getContextPath()%>/images/button/attach.gif" id="currAttach2" style="display:none" alt="当前附件" title="" onclick="openAtta()"/>
					    </td>
					  </tr>	
				</table>
			</ww:form>
		  </div>
		</div>
		<ww:include value="/jsp/common/dialog.jsp"></ww:include>
		<ww:include value="/jsp/common/downLoad.jsp"></ww:include>
		<script type="text/javascript"src="<%=request.getContextPath()%>/jsp/bugManager/relaCase.js"></script>
		<link rel="STYLESHEET" type="text/css"href="<%=request.getContextPath()%>/dhtmlx/windows/codebase/dhtmlxwindows.css">
		<link rel="STYLESHEET" type="text/css"href="<%=request.getContextPath()%>/dhtmlx/windows/codebase/skins/dhtmlxwindows_dhx_blue.css">
		<script type="text/javascript" src="<%=request.getContextPath()%>/dhtmlx/windows/codebase/dhtmlxwindows.js"></script>
	</BODY>
	<script type="text/javascript">
		importJs(conextPath+"/jsp/common/upload.js");
	</script>
</HTML>
