<%@ page contentType="text/html; charset=UTF-8"%>
<%@taglib uri="/webwork" prefix="ww"%>
<%@taglib uri="/WEB-INF/pmButton.tld" prefix="pmTag"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<HTML>
	<HEAD>
		<TITLE>BUG管理</TITLE>
		<META content="text/html; charset=UTF-8" http-equiv=Content-Type>
		<script type="text/javascript"src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxcommon.js"></script>
		<link rel="STYLESHEET" type="text/css"href="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgrid.css">
		<link rel="STYLESHEET" type="text/css" href="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgrid_skins.css">
		<script type="text/javascript"src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgrid.js"></script>
		<script type="text/javascript"src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgridcell.js"></script>
		<link rel="STYLESHEET" type="text/css" href="<%=request.getContextPath()%>/dhtmlx/toolbar/codebase/skins/dhtmlxtoolbar_dhx_blue.css">
	    <script type="text/javascript" src="<%=request.getContextPath()%>/dhtmlx/toolbar/codebase/dhtmlxtoolbar.js"></script>
	     <script type="text/javascript" src="<%=request.getContextPath()%>/dhtmlx/windows/codebase/dhtmlxwindows.js"></script>
	    <script type="text/javascript" src="<%=request.getContextPath()%>/dhtmlx/windows/codebase/ext/dhtmlxwindows_wtb.js"></script>		
		<script type="text/javascript" src="<%=request.getContextPath()%>/js/globalVariable.js"></script>
		<script type="text/javascript"src="<%=request.getContextPath()%>/js/commonFunction_top.js"></script>
		<link rel='STYLESHEET' type='text/css' href='<%=request.getContextPath()%>/css/page2.css'>
	</HEAD>
	<BODY  onload="loadFCK()" style="overflow-y:hidden;">
	<script type="text/javascript">
	var flwPageNo=1,flwPageCount=0,oEditor,flwBar;
	function loadFCK(){
		if(typeof oEditor != "undefined"){
			oEditor.SetData(reProStep) ;	
			return;
		}
		importJs(conextPath+"/pmEditor/fckeditor.js");
    	var pmEditor = new FCKeditor('reProStep') ;
    	pmEditor.BasePath = conextPath+"/pmEditor/" ;
    	pmEditor.Height = 200;
    	pmEditor.ToolbarSet = "Basic" ; 
    	pmEditor.ToolbarStartExpanded=false;
    	pmEditor.ReplaceTextarea();
	}
	function FCKeditor_OnComplete( editorInstance ){
		oEditor = FCKeditorAPI.GetInstance('reProStep');
		oEditor.SetData(recovJsonKeyWord(reProStep,"notRepComma"));	
		return;
	} 
	</script>
	<div align="center">
	<table border="0" style="width:100%" cellpadding="0" cellspacing="0" >
	  <tr>
    	<td class="jihuo" id="baseInfoTab" width="60" class="tdtxt" align="right"><strong><a onclick="styleChange(1)"  href="javascript:void(0);">基本信息</a></strong></td>
    	<td class="nojihuo" id="flowInfoTab" width="60" class="tdtxt" align="left"><strong><a onclick="styleChange(2)"  href="javascript:void(0);">处理历史</a></strong></td>
	    <td colspan="6" width="560">&nbsp;</td>
	  </tr>
	</table>
	</div>
		<div id="baseInfoDiv" align="center" class="cycleTask gridbox_light" style="border:0px;">
		  <div class="objbox" style="overflow:auto;width:100%;">
		  <ww:form theme="simple" method="post" id="createForm" name="createForm" namespace="" action="">
			<input type="hidden" id="listStr" name="listStr" value=""/>
			<ww:hidden id="attachUrl" name="dto.bug.attachUrl"></ww:hidden>
			<ww:hidden id="bugId" name="dto.bug.bugId"></ww:hidden>
			 <table   border="0" id="baseTable" class="obj row20px" cellspacing="0" align="center" cellpadding="0" border="0" width="100%">
			    <tr>
			      <td width="80" class="rightM_center" style="border-right:0" align="right" >测试需求:</td>
			      <td  class="tdtxt"  align="left" colspan="5"  width="640" style="padding:2 0 0 4;border-right:0">
			      <ww:hidden id="moduleId" name="dto.bug.moduleId"></ww:hidden>
    			     <ww:textfield id="moduleName" cssClass="text_c"  name="dto.moduleName" cssStyle="width:640;padding:2 0 0 4;border-right:0" readonly="true"></ww:textfield>
    			  </td>
			    </tr>
			    <tr>
	 		     <td width="80" class="rightM_center" style="border-right:0" align="right">当前状态:</td>
	 			 <td  class="tdtxt"  align="left"   width="160" style="padding:2 0 0 4;border-right:0">
	 			   <ww:property value="dto.stateName" id="stateName"/>
	 		     </td>
	 		     <td width="80" class="rightM_center" align="right" style="padding:2 0 0 4;border-right:0" nowarp>
	 			   <ww:property value="dto.currOwner" escape="false"/>
	 		     </td>	
	 		     <td width="80" class="rightM_center" style="border-right:0" align="right">作者:</td>
	 			 <td  class="tdtxt"  align="left"  width="160" style="padding:2 0 0 4;border-right:0">
	 			   <ww:property value="dto.bug.author.uniqueName"/>
	 		     </td>		    
			    </tr>
				<tr id="testOwnerTr">
				     <td  width="80" class="rightM_center" style="border-right:0" align="right" >测试人员:</td>
				     <td  class="tdtxt" width="160" style="padding:2 0 0 4;border-right:0">
				     <ww:textfield id="testOwnName" cssClass="text_c"  name="dto.bug.testOwner.uniqueName" cssStyle="width:160;padding:2 0 0 4;"   readonly="true"></ww:textfield>
				     </td>
				     <c:if test="${not empty dto.bug.devOwnerId}">
				       <td  width="80" class="rightM_center" style="border-right:0" align="right" >开发人员:</td>
				       <td  class="tdtxt" width="160" style="padding:2 0 0 4;border-right:0">
				        <ww:textfield id="devOwnerName" cssClass="text_c" name="dto.bug.devOwner.uniqueName" cssStyle="width:160;padding:2 0 0 4;" readonly="true"></ww:textfield>
				      </td>
				     </c:if>
				     <c:if test="${empty dto.bug.devOwnerId}">
				       <td  width="80" class="rightM_center"style="border-right:0"  align="right" >开发人员:</td>
				       <td  class="tdtxt" id="devOwnerTd" width="160" style="padding:2 0 0 4;border-right:0">
				          暂未指定
				      </td>
				     </c:if>
				     <c:if test="${not empty dto.bug.chargeOwner}">
			 		     <td width="80"class="rightM_center" style="border-right:0" align="right" >责任人:</td>
		 			 <td  class="tdtxt" width="160" style="padding:2 0 0 4;border-right:0">
		 			 <ww:hidden id="chargeOwner" name="dto.bug.chargeOwner"></ww:hidden>
		 			  <ww:textfield id="chargeOwnerName" cssClass="text_c" name="dto.bug.chargeOwnerName" cssStyle="width:160;padding:2 0 0 4;"  readonly='true' ></ww:textfield>
		 		     </td>	
			 		     </td>					     
				     </c:if>
				     <c:if test="${empty dto.bug.chargeOwner}">
			 		     <td width="80"class="rightM_center" style="border-right:0" align="right" >责任人:</td>
			 			 <td  class="tdtxt" width="160" style="padding:2 0 0 4;border-right:0">
			 			   暂未指定
			 		     </td>					     
				     </c:if>
				</tr>
			     <c:if test="${not empty dto.bug.bugAntimodDate}">
			     <tr>
				       <td  width="80" class="rightM_center"style="border-right:0"  align="right" >修改截止日期:</td>
				       <td  class="tdtxt" width="160" style="padding:2 0 0 4;border-right:0">
				       <fmt:formatDate value="${dto.bug.bugAntimodDate}" pattern="yyyy-MM-dd" />
				      </td>
				       <td  width="160" class="rightM_center" style="border-right:0" align="right" >工作量:</td>
				       <td  class="tdtxt" width="80" style="padding:2 0 0 4;border-right:0">
				       <ww:property value="dto.bug.planAmendHour" />(小时)
				      </td>
				  </tr>
			     </c:if>
			    <tr>
			      <td width="80" class="rightM_center" style="border-right:0" align="right" >Bug描述:</td>
			      <td  class="tdtxt" colspan="5" width="640" style="padding:2 0 0 4;border-right:0">
    			      <ww:textfield id="bugDesc" cssClass="text_c"  name="dto.bug.bugDesc" cssStyle="width:640;padding:2 0 0 4;border-right:0" readonly="true"></ww:textfield>
    			  </td>
			    </tr>
			   <tr>
	 		     <td width="80" class="rightM_center" style="border-right:0" align="right">类型:</td>
	 			 <td  class="tdtxt" width="160" style="padding:2 0 0 4;border-right:0">
	 			 <ww:hidden id="bugTypeId" name="dto.bug.bugTypeId" ></ww:hidden>
	 			   <ww:textfield id="bugTypeName" cssClass="text_c"  name="dto.bug.bugType.typeName" cssStyle="width:160;padding:2 0 0 4;" readonly="true"></ww:textfield>
	 		     </td>	
	 		     <td width="80" class="rightM_center" style="border-right:0" align="right">等级:</td>
	 			 <td  class="tdtxt" width="160" style="padding:2 0 0 4;border-right:0">
	 			 <ww:hidden id="bugGradeId" name="dto.bug.bugGradeId"></ww:hidden>
	 			   <ww:textfield id="bugGradeName" cssClass="text_c"  name="dto.bug.bugGrade.typeName" cssStyle="width:160;padding:2 0 0 4;border-right:0" readonly="true"></ww:textfield>
	 		     </td>
	 		     <td width="80" class="rightM_center" style="border-right:0" align="right" >再现平台:</td>
	 			 <td  class="tdtxt" width="160" style="padding:2 0 0 4;border-right:0">
	 			 <ww:hidden id="platformId" name="dto.bug.platformId"></ww:hidden>
	 			  <ww:textfield id="pltfomName" cssClass="text_c" name="dto.bug.occurPlant.typeName" cssStyle="width:160;padding:2 0 0 4;border-right:0" readonly="true"></ww:textfield>
	 		     </td>	
			   </tr>
			   <tr>
	 		     <td width="80" class="rightM_center" style="border-right:0" align="right" >来源:</td>
	 			 <td  class="tdtxt" width="160" style="padding:2 0 0 4;border-right:0">
	 			  <ww:hidden id="sourceId" name="dto.bug.sourceId"></ww:hidden>
	 			  <ww:textfield id="sourceName" cssClass="text_c" name="dto.bug.bugSource.typeName" cssStyle="width:160;padding:2 0 0 4;border-right:0" readonly="true"></ww:textfield>
	 		     </td>
	 		     <td width="80" class="rightM_center" style="border-right:0" align="right" >发现时机:</td>
	 			 <td  class="tdtxt" width="160" style="padding:2 0 0 4;border-right:0">
	 			  <ww:hidden id="bugOccaId" name="dto.bug.bugOccaId"></ww:hidden>
	 			  <ww:textfield id="occaName" cssClass="text_c"  name="dto.bug.bugOpotunity.typeName" cssStyle="width:160;padding:2 0 0 4;border-right:0" readonly="true"></ww:textfield>
	 		     </td>	
	 		     <ww:if test="${dto.bug.geneCauseId}==null||${dto.bug.geneCauseId}==''">
		 		     <td width="80" id="geneCauseTd" style="border-right:0" class="rightM_center" align="right" >测试时机:</td>
		 			 <td  class="tdtxt" id="geneCauseF" width="160" style="padding:2 0 0 4;border-right:0">
		 			 <ww:hidden id="geneCauseId" name="dto.bug.geneCauseId"></ww:hidden>
		 			 <ww:textfield id="geneCaseName" cssClass="text_c" readonly="true" name="dto.bug.geneCause.typeName" cssStyle="width:160;padding:2 0 0 4;border-right:0" ></ww:textfield>
		 		     </td>	
	 		      </ww:if>	
	 		       <ww:else>
		 		     <td width="80"  class="rightM_center" align="right" style="border-right:0">
		 		     <div id="geneCauseTd" style="display: none">
		 		       引入原因:
		 		      </div> 
		 		      </td>
		 			 <td  class="tdtxt"  width="160" style="padding:2 0 0 4;border-right:0">
		 			 <ww:hidden id="geneCauseId" name="dto.bug.geneCauseId"></ww:hidden>
		 			  <div id="geneCauseF" style="display: none">
		 			    <ww:textfield id="geneCaseName" readonly="true" name="dto.bug.geneCause.typeName" cssStyle="width:160;padding:2 0 0 4;border-right:0" ></ww:textfield>
		 		     </div>
		 		     </td>		 		       
	 		       </ww:else>	   
			   </tr>
			   <tr>
	 		     <td width="80" class="rightM_center"style="border-right:0" align="right">优先级:</td>
	 			 <td  class="tdtxt" width="160" style="padding:2 0 0 4;border-right:0">
	 			 <ww:hidden id="priId" name="dto.bug.priId"></ww:hidden>
	 			  <ww:textfield id="priName" cssClass="text_c"  name="dto.bug.bugPri.typeName" cssStyle="width:160;padding:2 0 0 4;border-right:0" readonly="true"></ww:textfield>
	 		     </td>	
	 		     <td width="80" class="rightM_center" style="border-right:0" align="right">频率:</td>
	 			 <td  class="tdtxt" width="160" style="padding:2 0 0 4;border-right:0">
	 			 <ww:hidden id="bugFreqId" name="dto.bug.bugFreqId"></ww:hidden>
	 			  <ww:textfield id="bugFreqName" cssClass="text_c"  name="dto.bug.bugFreq.typeName" cssStyle="width:160;padding:2 0 0 4;border-right:0" readonly="true"></ww:textfield>
	 		     </td>	
	 		     <c:if test="${not empty dto.bug.reproPersent}">
	 		     	 <td width="80"  class="rightM_center" style="border-right:0" align="right" >
	 		     	     再现比例:
	 		     	 </td>
		 		      <td  class="tdtxt" align="left"  style="width:160;padding:2 0 0 4;border-right:0"> 
		 		        <ww:textfield id="reproPersent" cssClass="text_c"  name="dto.bug.reproPersent" readonly="true" cssStyle="width:160;padding:2 0 0 4;" ></ww:textfield>
				      </td>	
			      </c:if> 
	 		      <c:if test="${empty dto.bug.reproPersent}">
	 		     	 <td width="80"  class="rightM_center" style="border-right:0" align="right" >
	 		     	   <div id="repropTd"  style="display:none;"> 
	 		     	     再现比例:
	 		     	   </div>
	 		     	 </td>
		 		      <td  class="tdtxt" align="left"  style="width:160;padding:2 0 0 4;border-right:0">
		 		       <div id="reprop"  style="display:none;"> 
		 		        <ww:textfield id="reproPersent" cssClass="text_c"  name="dto.bug.reproPersent" readonly="true" cssStyle="width:160;padding:2 0 0 4;" ></ww:textfield>
				      </div>
				      </td>	
			      </c:if> 
			   </tr>
			   <ww:if test="${dto.bug.genePhaseId}!=null||${dto.bug.genePhaseId}!=''">
			   <tr>
		 		     <td id="impPhaTdtxt" width="80" class="rightM_center" style="border-right:0" align="right"  >引入阶段:</td>
		 			 <td id="impPhaTd" class="tdtxt" colspan="5" width="640" style="padding:2 0 0 4;border-right:0">
		 			 <ww:hidden id="genePhaseId" name="dto.bug.genePhaseId"></ww:hidden>
		 			 <ww:property value="dto.bug.importPhase.typeName"/>
		 		     </td>		 		     
			   </tr>
			    </ww:if>
			   <tr>
   			    <td width="80" class="rightM_center" style="border-right:0"align="right" nowrap>再现步骤:</td>
   			    <td width="640" class="tdtxt" colspan="5" style="background-color: #f7f7f7; hight:200;width:640;border-right:0">
   			      <textarea name="dto.bug.reProStep" class="text_c"  id="reProStep" cols="50" rows="30"style="width:640;hight:200;padding:2 0 0 4;">
   			      </textarea>
   			    </td>			   
			   </tr>
			   <tr>
			   <td  class="rightM_center" align="right" style="border-right:0" width="80" >发现日期:</td>
			   <td  class="tdtxt" align="left" width="160" style="width:160;padding:2 0 0 4;border-right:0">
			   
			   <fmt:formatDate value="${dto.bug.reptDate}" pattern="yyyy-MM-dd HH:mm:ss"/>
			   </td>
			   <td  class="rightM_center" align="right" style="border-right:0" width="80">发现版本:</td>
			   <td  class="tdtxt" align="left" width="160"style="width:160;padding:2 0 0 4;border-right:0">
			     <ww:property value="dto.bug.reptVersion.versionNum"/>
			   </td>
			   <c:if test="${not empty dto.bug.versionLable}">
			   <td class="rightM_center" style="border-right:0" align="right" width="80">
			   <ww:property value="dto.bug.versionLable"/>:
			   </td>
			   <td  class="tdtxt" align="left" width="160"style="width:160;padding:2 0 0 4;border-right:0">
			     <ww:property value="dto.bug.currVersion.versionNum" />
			   </td>
			   </c:if>
			   <c:if test="${ empty dto.bug.versionLable}">
			   <td class="rightM_center" style="border-right:0" align="right" width="80">
			   </td>
			   <td  class="tdtxt" align="left" width="160"style="width:160;padding:2 0 0 4;border-right:0">
			   </td>			   
			   </c:if>
			   </tr>
			   <c:if test="${not empty dto.bug.currRemark}">
			    <tr>
			      <td width="80" class="rightM_center" align="right" style="border-right:0">备注:</td>
			      <td  class="tdtxt"  align="left" colspan="5"  width="640" style="padding:2 0 0 4;border-right:0">
    			     <ww:textfield id="currRemark" cssClass="text_c" name="dto.bug.currRemark" cssStyle="width:640;padding:2 0 0 4;" readonly="true"></ww:textfield>
    			  </td>
			    </tr>
			   </c:if>
			  <tr class="odd_mypm">
			  	 <td class="rightM_center" width="90" id="attachTd" style="border-right:0">
			  	 附件:
			  	 </td>
			    <td class="dataM_left"  colspan="5" width="630" style="border-right:0">
			    	<c:if test="${not empty dto.bug.attachUrl}">
					<img src="<%=request.getContextPath()%>/images/button/attach.gif" id="currAttach" alt="当前附件" title="${dto.bug.attachUrl2}" onclick="openAtta()"/>
			   		</c:if>
			    </td>
			  </tr>	
			   <tr>
				<td  class="tdtxt" align="center" width="720" colspan="6" style="border-right:0">
				    <a class="bluebtn" href="javascript:closeMe();" style="margin-left: 6px"><span> 返回</span></a>
				    <a class="bluebtn" id="nextOne"href="javascript:parent.handBug('next');" style="margin-left: 6px;display:none"><span> 下一个</span></a>
				 	<a class="bluebtn" id="preOne"href="javascript:parent.handBug('up');" style="display:none"><span> 上一个</span></a>
    			 </td> 	   
			   </tr>
			   <tr>
			    <td colspan="6" style="border-right:0">&nbsp;
			    </td>
			   </tr>
			 </table>
			</ww:form>
	    </div>
	  </div>
	  <div id="flowInfoDiv" align="center"  valign="middle" style="background-color: #ffffff;display:none; width:100%;">
		<input type="hidden" id="listStr" name="listStr" value="" />
		<table width="100%" cellspacing="0" cellpadding="0">
			<tr>
				<td valign="top">
					<div id="toolbarObj1"></div>
				</td>
			</tr>
			<tr>
				<td valign="top">
					<div id="gridbox1" ></div>
				</td>
			</tr>
		</table>  
	  </div>
	<ww:include value="/jsp/common/downLoad.jsp"></ww:include>
	<script type="text/javascript">
	adjustTable("baseTable","odd");
	parent.detlW_ch.setDimension(850, 510);
	function closeMe(){
		parent.detlW_ch.setModal(false);
		parent.detlW_ch.hide();
		return;
	}	
	var reProStep ="${dto.bug.reProStep2}";
	var flwStr ="${dto.testFlow}";
	var roleStr ="${dto.roleInTask}";
	parent.roleStr="${dto.roleInTask}";
	parent.flwStr="${dto.testFlow}";     
	var wtTitle = parent.detlW_ch.getText();
	if(wtTitle.split(">")[1]=="</span"){
		parent.detlW_ch.setText("软件问题报告明细");
	}
	pmGrid = new dhtmlXGridObject('gridbox1');
	pmGrid.setImagePath(conextPath+"/dhtmlx/grid/codebase/imgs/");
    pmGrid.setHeader("&nbsp;,序号,处理人,处理结果,处理说明,处理日期");
    pmGrid.setInitWidths("0,40,80,260,*,80");
    pmGrid.setColAlign("center,center,left,left,left,left");
    pmGrid.setColTypes("ra,ro,ro,ro,ro,ro");
    pmGrid.setColSorting("int,str,str,str,str,str");
	pmGrid.enableAutoHeight(true,500);
    pmGrid.init();
    pmGrid.enableTooltips("false,true,true,true,true,true");
    pmGrid.setSkin("light");   
	flwBar = new dhtmlXToolbarObject("toolbarObj1");
	flwBar.setIconsPath(conextPath+"/dhtmlx/toolbar/images/");
	flwBar.addButton("first", 1, "", "first.gif", "first.gif");
	flwBar.setItemToolTip("first", "第一页");
	flwBar.addButton("pervious",2, "", "pervious.gif", "pervious.gif");
	flwBar.setItemToolTip("pervious", "上一页");
	flwBar.addSlider("slider",3, 80, 1, 30, 1, "", "", "%v");
	flwBar.setItemToolTip("slider", "滚动条翻页");
	flwBar.addButton("next",4, "", "next.gif", "next.gif");
	flwBar.setItemToolTip("next", "下一页");
	flwBar.addButton("last",5, "", "last.gif", "last.gif");
	flwBar.setItemToolTip("last", "末页");
	flwBar.addInput("page",6, "", 25);
	flwBar.addText("pageMessage",7, "");
	flwBar.addText("pageSizeText",8, "每页");
	flwBar.addText("pageSizeTextEnd",9, "20条");
	try{
	 	if(parent.pmGrid.getRowsNum()>1&&parent.opreType=="bugHand"){
			$("preOne").style.display="";
			$("nextOne").style.display="";	
		}
	}catch(err){
	}
	 </script>
	<script type="text/javascript"src="<%=request.getContextPath()%>/jsp/bugManager/history.js"></script>
	</BODY>
</HTML>
