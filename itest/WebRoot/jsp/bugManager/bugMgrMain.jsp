<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="/WEB-INF/c.tld"%>
<%@ taglib prefix="ww" uri="/webwork"%>
<HTML>
<HEAD>
	<TITLE>MYPM项目管理平台</TITLE>
	<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/dhtmlx/dhtmlxTree/codebase/dhtmlxtree.css">
	<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/dhtmlx/layout/codebase/dhtmlxlayout.css">
	<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/dhtmlx/layout/codebase/skins/dhtmlxlayout_dhx_blue.css">
	<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/dhtmlx/windows/codebase/dhtmlxwindows.css">
	<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/dhtmlx/windows/codebase/skins/dhtmlxwindows_dhx_blue.css">
	<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgrid.css">
	<link rel='STYLESHEET' type='text/css'href='<%=request.getContextPath()%>/css/page.css'>
	<script type="text/javascript" src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxcommon.js"></script>
	<script type="text/javascript" src="<%=request.getContextPath()%>/dhtmlx/layout/codebase/dhtmlxlayout.js"></script>
	<script type="text/javascript" src="<%=request.getContextPath()%>/dhtmlx/windows/codebase/dhtmlxwindows.js"></script>
	<script type="text/javascript" src="<%=request.getContextPath()%>/dhtmlx/dhtmlxTree/codebase/dhtmlxtree.js"></script>
</HEAD>
<BODY style="overflow-y:hidden;overflow-x:hidden;">
   <ww:hidden id="repTree" name="analysisDto.treeStr"></ww:hidden>
	<table cellspacing="0" cellpadding="0" align="center" border="0">
		<tr><td><div id="reqMainDiv" style="width:600px; height:590px"></div></td></tr>
	</table>
<ww:include value="/jsp/common/dialog.jsp"></ww:include>
</BODY>
<script type="text/javascript">
	function cusBack(){
		window.location=conextPath+"/bugManager/bugManagerAction!loadAllMyBug.action?dto.allTestTask=true";
	}
	var swWin;
	function popSwTaskList(){
		swWin = initW_ch(swWin, "", true, 890, 500,'swWin');
		swWin.attachURL(conextPath+"/singleTestTask/singleTestTaskAction!swTestTaskList.action?dto.operCmd=myBugTreeView");
		swWin.setText("请选择测试项目");	
    	swWin.show();
    	swWin.bringToTop();
    	swWin.centerOnScreen();
    	swWin.setModal(true);			
	}
	var projectSelectW_ch, parameterW_ch,cuW_ch;
	$("reqMainDiv").style.width = clientWidth - 12;
	$("reqMainDiv").style.height = document.body.clientHeight - 20;
	var mypmLayout = new dhtmlXLayoutObject("reqMainDiv", "2U");
	var views = mypmLayout.listViews();
	mypmLayout.items[0].setText("测试需求");
	mypmLayout.items[0].setWidth(220);
	mypmLayout.items[1].hideHeader();
	mypmLayout.items[1].attachURL(conextPath+"/bugManager/bugManagerAction!loadMyBugWithModule.action");
	var mypmTree = mypmLayout.items[0].attachTree(0);
	mypmTree.setImagePath(conextPath+"/dhtmlx/dhtmlxTree/codebase/imgs/");
	mypmTree.enableHighlighting(1);
	mypmTree.setImageArrays("plus","plus2.gif","plus3.gif","plus4.gif","plus.gif","plus5.gif");
	mypmTree.setImageArrays("minus","minus2.gif","minus3.gif","minus4.gif","minus.gif","minus5.gif");
	mypmTree.setStdImages("book.gif","books_open.gif","books_close.gif");
	//mypmTree.setOnClickHandler(onclickHdl);

	function hintReSelMsg(){
		hintMsg("所选|当前项目未提交测试需求,请切换到其他项目");
	}
	var relBugW_ch,cuW_ch;
	function relaBug(url){
		if(navigator.userAgent.indexOf("Chrome")>0)
			relBugW_ch = initW_ch(relBugW_ch, "", true, 890, 540,'relBugW_ch');
		else
			relBugW_ch = initW_ch(relBugW_ch, "", true, 890, 535,'relBugW_ch');
		relBugW_ch.attachURL(url);
		relBugW_ch.setText("关联BUG---点击序号查看明细---可跨页选择后关联");	
	    relBugW_ch.show();
	    relBugW_ch.bringToTop();
	    relBugW_ch.setModal(true);	
	}
	var treeDisModel = "complex";
	function viewHisty(){
		if(mypmLayout.items[1]._frame.contentWindow.$("currNodeId").value==""){
			//loadTree();
			hintMsg("请从需求分解树中选择测试需求项");
			return;
		}
		cuW_ch = initW_ch(cuW_ch, "", true, 960, 520,'cuW_ch')
		cuW_ch.setText("执行记录---点击序号关联BUG---点击用例描例查看明细");
		var url = conextPath+"/caseManager/caseManagerAction!viewHistory.action?dto.currNodeId="+mypmLayout.items[1]._frame.contentWindow.$("currNodeId").value;
		cuW_ch.attachURL(url);
	}
</script>
</html>