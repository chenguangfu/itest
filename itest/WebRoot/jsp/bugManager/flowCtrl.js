	function closeMe(){
		if(typeof parent.fW_ch != "undefined"){
			parent.fW_ch.hide();
		}
		parent.detlW_ch.setModal(false);
		parent.detlW_ch.hide();
		if(typeof parent.treeW_ch != "undefined"){
			parent.treeW_ch.hide();
		}
		try{parent.collapseLayout('open');}catch(err){}
		return;
	}
	var popW_ch;
	var popGrid;
	var tyId,tpNa;
	function handSubCheck(){
		var currState = $("stateIdList").options[$("stateIdList").selectedIndex].value;
		if($("moduleId").value==""){
			hintMsg("请选择对应测试需求");
			return false;
		}else if(isAllNull($("bugDesc").value)){
			hintMsg("请填写问题描述");
			$("bugDesc").focus();
			return false;
		}else if($("bugTypeId").value==""){
			hintMsg("请选择类型");
			return false;
		}else if($("bugGradeId").value==""){
			hintMsg("请选择等级");
			return false;
		}else if($("platformId").value==""){
			hintMsg("请选择平台");
			return false;
		}else if($("sourceId").value==""){
			hintMsg("请选择来源");
			return false;
		}else if($("bugOccaId").value==""){
			hintMsg("请选择发现时机");
			return false;
		}else if($("geneCauseF").style.display==""&& $("geneCauseId").value==""){
			hintMsg("请选测试时机");
			return false;		
		}else if($("bugFreqId").value==""){
			hintMsg("请选择频率");
			return false;
		}else if($("reprop").style.display==""&&$("reproPersent").value==""){
			hintMsg("请填写再现比例");
			$("reprop").focus();
			return false;		
		}else if($("impPhaTd").style.display==""&&$("genePhaseId").value==""&&currState=="13"){
			$("impPhaTdtxt").style.color="red";
			hintMsg("请选择引入原因");
			return false;
		}else if(chkSelNextOwn!=""&&$("nextOwnName").value==""){
			hintMsg("请选择要转交的下一处理人");
			return false;		
		}else if(exist("bugAntimodDate")&&$("bugAntimodDate").value!=""&&$("planAmendHour").value==""){
			hintMsg("修改日期和工作量必须同时设置");
			return false;			
		}else if(exist("planAmendHour")&&(!isFloat($("planAmendHour").value,false)||parseInt($("planAmendHour").value)<1)){
			hintMsg("工作量为非零数值");
			return false;
		}else{
			$("reProStep").value=oEditor.GetXHTML().replace(/\s+$|^\s+/g,"");
			if($("reProStep").value=="<br />"||$("reProStep").value==""||$("reProStep").value=="<strong><br></strong>"){
				hintMsg("请填写再现步骤");
				oEditor.Focus();
				return false;			
			}
			for(var i=1; i<31;i++){
				$("reProStep").value = $("reProStep").value.trim("<br>");
				$("reProStep").value = $("reProStep").value.trim("&nbsp;");	
				$("reProStep").value = $("reProStep").value.replace(/\s+$|^\s+/g,"");	
				$("reProStep").value = $("reProStep").value.trim("\\");			
			}
			if(parent.checkIsOverLong($("reProStep").value,3000)){
				hintMsg("再现步骤不能超过1000个字符");
				return false;			
			}
			var repTxt = $("reProStep").value.replace(/\s+$|^\s+/g,"");
			var mdPath = "<strong>"+$("moduleName").value +"</strong>:";
			if(repTxt==mdPath){
				hintMsg("请填写再现步骤");
				oEditor.Focus();
				return false;			
			}
			$("reProTxt").value=html2PlainText("reProStep");
		}
		if(parent.checkIsOverLong($("bugDesc").value,150)){
			hintMsg("描述不能超过150个字符");
			return false;			
		}
		return true;
	}


    var nextCd ="";
    var chkSelNextOwn="",appendVerStr="";
    var stateArr,memSel,ctrInfoArr,handName,selStr,ownIdField,popName,innerStr,initOwnName="",verFieldChk="";
	function trigerCtrl(val){
		//var stateArr,memSel,ctrInfoArr,handName,selStr,ownIdField,popName,innerStr,initOwnName="";
		if(val=="3"){//重复
			$("repeBtn").style.display="";
		}else{
			$("repeBtn").style.display="none";
			$("withRepteId").value="";
		}
		if($("withRepteId").value!=""){
			$("repeDetBtn").style.display="";
		}else{
			$("repeDetBtn").style.display="none";
		}
		if($("impPhaTd").style.display==""&&$("genePhaseId").value==""&&val=="13"){
			$("impPhaTdtxt").style.color="red";	
		}else{
			$("impPhaTdtxt").style.color="#055A78";
		}	
		parent.detlW_ch.setDimension( 865, 550);
		if(val=="-1"){
			$("currStateId").value=$("initState").value;
			$("nextOwnTd1").style.display="none";
			$("nextOwnTd2").style.display="none";
			$("nextOwnTd3").style.display="none";
			$("nextOwnTr").style.display="none";
			$("repeDetBtn").style.display="none";	
			$("testOwnerId").value=testOwnInit;	
			$("analyseOwnerId").value=anasOwnInit;
			$("assinOwnerId").value=asinOwnInit;
			$("devOwnerId").value=devOwnInit;
			$("intercessOwnerId").value=inteOwnInit;
			$("withRepteId").value="";
			$("verLableTd").innerHTML=verLableInit;
			$("verValTd").innerHTML=verValInit;
			$("fixVer").value=fixVerInit;
			$("verifyVer").value=verifyVerInit;
			chkSelNextOwn="";
			verFieldChk="";
			appendVerStr="";
			$("nextOwnerId").value=nextOwnerIdInit;
			nextCd="";
			if(remarkInit==""&&testOwnInit==$("testOwnerId").value&&devOwnInit==$("devOwnerId").value){
				$("reamrkTr").style.display="none";
			}
			if(testOwnInit!=$("testOwnerId").value||devOwnInit!=$("devOwnerId").value){
				$("currRemark").readOnly=false;
			}else{
				$("currRemark").readOnly=true;
				$("currRemark").value=remarkInit;
			}
			adjustTable("baseTable","odd");
			return;
		}
		var matchF= false;
		$("reamrkTr").style.display="";
		adjustTable("baseTable","odd");
		$("currRemark").readOnly=false;
		for(var i=0 ;i<ctrlInfo.length; i++){
			ctrInfoArr = ctrlInfo[i].split("=");
			stateArr=ctrInfoArr[0].split(",");
			for(var l=0 ;l<stateArr.length ;l++){
				if(stateArr[l]==val){
					matchF=true;
					break;
				}
			}
			if(!matchF){
				continue;
			}
			nextCd=ctrInfoArr[1].split("^")[0];
			memSel=ctrInfoArr[1].split("^")[1];
			handName="处理";
			if(memSel!="0"){
				//alert("memSel="+memSel+"**nextCd="+nextCd +"**stateStr="+ctrInfoArr[0] +"**val="+val);
				$("nextOwnTd1").style.display="";
				$("nextOwnTd2").style.display="";
				$("nextOwnTd3").style.display="";
				$("nextOwnTr").style.display="";
				adjustTable("baseTable","odd");
				if(nextCd=="1"){
					ownIdField="testOwnerId";
					selStr="testSelStr";
					popName="测试人员";
					if(testOwnName!=""){
						initOwnName = testOwnName;
						//下面这个处理是为了处理，BUG提交人不是测试人员时，要转线测试人员处理
						if(parent.$("testSelStr").value.indexOf(testOwnName)<0&&(val=="13"||val=="26")){
							initOwnName = "";
						}
					}else{
						initOwnName = "";
					}
					handName="确认/处理";
				}else if(nextCd=="2"){
					ownIdField="testOwnerId";
					selStr="testChkIdSelStr";
					handName="互验";
					popName="测试互验人员"
					//要重新指定互验人员，所以不设置缺省的
				}else if(nextCd=="3"){
					ownIdField="analyseOwnerId";
					selStr="analySelStr";
					handName="分析";
					popName="分析人员";
					if(anasName!=""){
						initOwnName = anasName;
					}else{
						initOwnName = "";
					}
				}else if(nextCd=="4"){
					ownIdField="assinOwnerId";
					selStr="assignSelStr";
					handName="分配";
					popName="分配人员";
					if(asinName!=""){
						initOwnName = asinName;
					}else{
						initOwnName = "";
					}
				}else if(nextCd=="5"){
					ownIdField="devOwnerId";
					selStr="devStr";
					handName="修改";
					popName="修改人员";
					if(devOwnName!=""){
						initOwnName = devOwnName;
					}else{
						initOwnName = "";
					}
				}else if(nextCd=="6"){
					ownIdField="devOwnerId";
					selStr="devChkIdSelStr";
					handName="互检";
					popName="开发互检人员";
					if(devOwnName!=""){
						initOwnName = devOwnName;
					}else{
						initOwnName = "";
					}
				}else if(nextCd=="7"){
					ownIdField="intercessOwnerId";
					selStr="interCesSelStr";
					handName="审批/仲裁";
					popName="仲裁人员";
					if(inteOwnName!=""){
						initOwnName = inteOwnName;
					}else{
						initOwnName = "";
					}
				}else if(nextCd=="8"){
					ownIdField="testOwnerId";
					selStr="testSelStr";
					handName="确认";
					popName="测试人员";
					if(testOwnName!=""){
						initOwnName = testOwnName;
						//下面这个处理是为了处理，BUG提交人不是测试人员时，要转线测试人员处理
						if(parent.$("testSelStr").value.indexOf(testOwnName)<0&&(val=="13"||val=="26")){
							initOwnName = "";
						}
					}else{
						initOwnName = "";
					}
				}
				if(initOwnName!="")
					$("nextOwnerId").value=$(ownIdField).value;
				else
					$("nextOwnerId").value="";
				$("nextOwnTd3").innerHTML=handName;
				innerStr = "<input type='text' readonly='true' Class='text_c' onfocus=\"popSelWin('"+ownIdField+"','nextOwnName','"+selStr+"','"+popName+"')\" style='padding: 2px 0pt 0pt 4px; width: 160px;border-right:0' id='nextOwnName' value='"+initOwnName+"' name='nextOwnName'/>";
				if(handName=="修改")
					$("assFromMdTd").innerHTML="<a class=\"graybtn\" href=\"javascript:void(0);\" onclick=\"getMdPerson('1')\"><span>从测试需求指定人员中指派</span> </a>";
				else if(handName=="分配")
					$("assFromMdTd").innerHTML="<a class=\"graybtn\" href=\"javascript:void(0);\" onclick=\"getMdPerson('3')\"><span>从测试需求指定人员中指派</span> </a>";
				else
					$("assFromMdTd").innerHTML="";
				$("nextOwnTd2").innerHTML=innerStr;
				parent.detlW_ch.setDimension(865, 560);
				chkSelNextOwn="chk";
			}else{
				$("nextOwnTd1").style.display="none";
				$("nextOwnTd2").style.display="none";
				$("nextOwnTd3").style.display="none";	
				$("nextOwnTr").style.display="none";
				$("testOwnerId").value=testOwnInit;	
				$("analyseOwnerId").value=anasOwnInit;
				$("assinOwnerId").value=asinOwnInit;
				$("devOwnerId").value=devOwnInit;
				$("intercessOwnerId").value=inteOwnInit;
				adjustTable("baseTable","odd");
				chkSelNextOwn="";	
				$("nextOwnerId").value="";
			}
			break;
		}
		adjustTable("baseTable","odd");
		$("currStateId").value=val;
		verCtrl(val);
		
	}
	function verCtrl(stateVal){
		var verLable = "";
		var verValTdStr = "";
		verFieldChk="";
		if((initStateName!="分析"&&initStateName!="分配"&&initStateName!="重分配"&&initStateName!="分歧")&&(stateVal=="6"||stateVal=="7"||stateVal=="8"||stateVal=="9"||stateVal=="10"||stateVal=="11")){
			verLable = "校验在版本";
			verFieldChk="verifyVer";
		}else if(stateVal=="13"||stateVal=="18"||stateVal=="26"){
			verLable = "修改在版本";
			verFieldChk="fixVer";
		}else if(stateVal=="14"||stateVal=="15"||stateVal=="22"||stateVal=="22"){
			verLable = "关闭在版本";
			verFieldChk="verifyVer";
		}else if(stateVal=="20"||stateVal=="21"){
			verLable = "延迟到版本";
			verFieldChk="fixVer";
		}
		if(verFieldChk==""){
			$("verLableTd").innerHTML=verLableInit;
			$("verValTd").innerHTML=verValInit;
			$("fixVer").value=fixVerInit;
			$("verifyVer").value=verifyVerInit;	
			appendVerStr="";
			return;	
		}
		$(verFieldChk).value="";
		$("verLableTd").innerHTML=verLable;
		verValTdStr = "<input type='text' readonly='true' Class='text_c' onfocus=\"popSelWin('"+verFieldChk+"','currVerName','verSelStr','"+verLable+"')\" style='padding: 2px 0pt 0pt 4px; width: 160px;' id='currVerName' value='' name='currVerName'/>";
		$("verValTd").innerHTML=verValTdStr;
		appendVerStr=verLable;
	}
	function handChk(){
		if(!remarkChk()){
			return false ;
		}
		if($("currStateId").value=="3"&&$("withRepteId").value==""){
			hintMsg('请关联重复的BUG');
			return false;			
		}
		var reportId = parent.pmGrid.cells(parent.pmGrid.getSelectedId(),20).getValue();
		if(myId=!reportId&&roleStr.indexOf("1")<0&&roleStr.indexOf("2")<0&&roleStr.indexOf("8")<0){
			oEditor.SetData(reProStep) ;
		}
		if(!handSubCheck()){
			return false;
		}
		if(verFieldChk!=""){
			if($(verFieldChk).value==""){
				hintMsg("请选择" +$("verLableTd").innerHTML);
				return false;
			}
		}
		if(nextCd!=""&&$("currUserId").value==$("currHandlerId").value){//这时为本人修改之前的状态
			if($("currFlowCd").value!=nextCd){
				$("nextFlowCd").value=nextCd;
			}
			if($("currFlowCd").value!=nextCd&&nextCd!=nextFlwCdInit){
				$("currFlowCd").value=nextFlwCdInit;
			}	
		}else if(nextCd!=""&&$("currUserId").value!=$("currHandlerId").value){//处理别人处理过的问题
			$("currFlowCd").value=nextFlwCdInit;
			$("nextFlowCd").value=nextCd;
		}
		return true;
	}
	
	function handSub(contin){
		if(handChk()){
			var url=conextPath+"/bugManager/bugManagerAction!handSub.action?dto.isAjax=true";
			var processLog = constructLog();
			if(processLog!=""){
				$("processLog").value=processLog;
				//直接在URL中写操作日志,得要求tomcat配置url用utf8 改为表单提交就没这问题了
				//url +="&dto.bug.processLog="+processLog;
			}
			if(processLog==""&&!chgChk()){
				hintMsg("您没有做任何改动");
				return; 
			}
			if(reProTxtInit!=$("reProTxt").value){//一定要在handChk()后做这处理,因为没handChk前新的reProTxt没产生
				processLog = processLog +"---另外还更改了BUG再现步骤,原始描述为:  " +reProTxtInit;
				$("processLog").value = processLog;
			}
			var ajaxResut = postSub(url,"hanfForm");
			if(ajaxResut!="success"){
				hintMsg("提交数据发生错误");
				return;
			}
			var newRowNum = parent.getNextRowNum($("bugId").value);
			var isLastRow = parent.isLastRow($("bugId").value);
			adjustGrid();
			if(typeof contin!="undefined"){
				if(isLastRow){
					parent.handBug("next","nextPage");
					return;
				}else{
					parent.selRowByNum(newRowNum);
					parent.handBug();				
				}
				verFieldChk="";
				appendVerStr="";
				return;
			}
			verFieldChkChk=""
			appendVerStr="";
			parent.detlW_ch.setModal(false);
			parent.detlW_ch.hide();
			try{parent.collapseLayout('open');}catch(err){}
		}
	}
	function adjustGrid(){
		var jsonData=$("bugId").value+"^0,"+$("bugId").value +","+$("bugDesc").value;
		var currStateName = $("stateIdList").options[$("stateIdList").selectedIndex].text;
		var tester = $("testOwnName").value;
		//tester = tester.substring(0,tester.indexOf("("));
		if(popName=="测试人员"){
			tester = $("nextOwnName").value;
			//tester = tester.substring(0,tester.indexOf("("));
		}
		if(currStateName!=""&&currStateName!=initStateName){
			//jsonData+=","+currStateName+","+$("bugGradeName").value+","+$("bugFreqName").value+","+$("occaName").value;
			jsonData+=","+currStateName+","+$("bugGradeName").value+","+parent.pmGrid.cells($("bugId").value,5).getValue()+","+$("occaName").value;
		}else{
			//jsonData+=","+initStateName+","+$("bugGradeName").value+","+$("bugFreqName").value+","+$("occaName").value;
			jsonData+=","+initStateName+","+$("bugGradeName").value+","+parent.pmGrid.cells($("bugId").value,5).getValue()+","+$("occaName").value;
		}
		jsonData+=","+$("bugTypeName").value+","+$("priName").value+","+tester;
		if(exist("devOwnerName")){
			jsonData+=","+$("devOwnerName").value;
		}else if(popName=="修改人员"){
			jsonData+=","+$("nextOwnName").value;
		}else{
			jsonData+=","+"";
		}
		jsonData+=","+repDate+","+$("msgFlag").value+","+$("relaCaseFlag").value+","+$("moduleId").value;
		jsonData+=","+$("bugReptId").value+","+$("currFlowCd").value+","+myId+","+$("currStateId").value+","+$("nextFlowCd").value+", ";
		parent.upRow($("bugId").value,jsonData);
	}
	function html2PlainText2(iniText){
		 var textArr = iniText.split(">");
		 var restText = "";
		 for(var i=0; i<textArr.length; i++){
		 	var tmpText = textArr[i];
		 	if(textArr[i].indexOf("<")==0){
		 		if(textArr[i].indexOf("<img")==0){
		 			restText += "(图略...)";
		 		}
		 		continue;
		 	}
		 	var pic="";
		 	if(tmpText.indexOf("<")>0){
		 		if(tmpText.indexOf("<img")>=0){
	 				pic = "(图略...)";
	 			}
		 		tmpText = tmpText.substring(0,tmpText.indexOf("<"));
		 		tmpText = tmpText.replace("&nbsp;","") ;
		 		tmpText = tmpText.replace("&gt;","") ;
		 		tmpText = tmpText.replace("&lt;","") ;
		 		tmpText = tmpText.replace(/\s+$|^\s+/g,"");
		 		restText =restText + tmpText.replace("&nbsp;","") +" " ;
		 		if(pic !=""){
		 			restText+=pic;
		 		}
		 	}else{
		 		tmpText = tmpText.replace(/\s+$|^\s+/g,"");
		 		restText+=tmpText;
		 	}
		 }
		 return restText;
	}
	function chgChk(){
		if(html2PlainText2($("reProStep").value)!=html2PlainText2(reProStep)){
			return true;
		}else if(moduleInitId!=$("moduleId").value){
			return true;
		}else if(remarkInit!=$("currRemark").value){
			return true;
		}else if(bugDescInit!=$("bugDesc").value){
			return true;
		}else if(impPhaInit!=$("genePhaseId").value){
			return true;
		}else if(reproPerInit!=$("reproPersent").value){
			return true;
		}else if(priIdInit!=$("priId").value){
			return true;
		}else if(bugTypeInit!=$("bugTypeId").value){
			return true;
		}else if(geneCauseInit!=$("geneCauseId").value){
			return true;
		}else if(bugOccaInit!=$("bugOccaId").value){
			return true;
		}else if(sourceInit!=$("sourceId").value){
			return true;
		}else if(platformInit!=$("platformId").value){
			return true;
		}else if(bugGradeInit!=$("bugGradeId").value){
			return true;
		}else if(chargeOwnerInit!=$("chargeOwner").value){
			return true;
		}
		return false;
	}
	function constructLog(){
		var currStateName = $("stateIdList").options[$("stateIdList").selectedIndex].text;
		var proLog = "";
		if(currStateName!=""&&currStateName!=initStateName){
			proLog+=";把状态从:"+initStateName+"修改为:"+currStateName;
		}
		if(chargeOwnerInit==""&&$("chargeOwner").value!=""){
			proLog+=";设置责任人为:"+$("chargeOwnerName").value;
		}else if(chargeOwnerInit!=$("chargeOwner").value){
			proLog+=";把责任人从:"+chargeOwnerNameInit +"改为:"+$("chargeOwnerName").value;
		}else if(testOwnInit==""&&$("testOwnerId").value!=""){
			proLog+=";设置测试人员为:"+getUserNameById($("testOwnerId").value,"testSelStr");
		}else if(testOwnInit!=$("testOwnerId").value){
			proLog+=";把测试人员从:"+testOwnNameInit+"改为:"+getUserNameById($("testOwnerId").value,"testSelStr");
		}
		if(devOwnInit==""&&$("devOwnerId").value!=""){
			proLog+=";设置开发人员为:"+getUserNameById($("devOwnerId").value,"devStr");
		}else if(devOwnInit!=$("devOwnerId").value){
			proLog+=";把开发人员从:"+devOwnNameInit+"改为:"+getUserNameById($("devOwnerId").value,"devStr");
		}
		if(dueDateInit==""&&exist("bugAntimodDate")&&$("bugAntimodDate").value!=""){
			proLog+=";设置修改截止日期为:"+$("bugAntimodDate").value+" 工作量为:"+$("planAmendHour").value;
		}else if(dueDateInit!=""&&exist("bugAntimodDate")&&$("bugAntimodDate").value!=""&&$("bugAntimodDate").value!=dueDateInit){
			proLog+=";把修改截止日期从:"+dueDateInit +"改为"+$("bugAntimodDate").value+" 工作量为:"+$("planAmendHour").value;
		}
		if(inteOwnInit==""&&$("intercessOwnerId").value!=""){
			proLog+=";设置仲裁人为:"+getUserNameById($("intercessOwnerId").value,"interCesSelStr");
		}else if(inteOwnInit!=$("intercessOwnerId").value){
			proLog+=";把仲裁人从:"+inteOwnNameInit+"改为:"+getUserNameById($("intercessOwnerId").value,"interCesSelStr");
		}
		if(anasOwnInit==""&&$("analyseOwnerId").value!=""){
			proLog+=";设置分析人为:"+getUserNameById($("analyseOwnerId").value,"analySelStr");
		}else if(anasOwnInit!=$("analyseOwnerId").value){
			proLog+=";把分析人从:"+anasOwnNameInit+"改为:"+getUserNameById($("analyseOwnerId").value,"analySelStr");
		}
		if(asinOwnInit==""&&$("assinOwnerId").value!=""){
			proLog+=";设置分配人为:"+getUserNameById($("assinOwnerId").value,"assignSelStr");
		}else if(asinOwnInit!=$("assinOwnerId").value){
			proLog+=";把分配人从:"+asinOwnNameInit+"改为:"+getUserNameById($("assinOwnerId").value,"assignSelStr");
		}
		if(proLog!=""){
			proLog = proLog.substring(1,proLog.length);
		}
		if(proLog!=""&&$("flwName").value!=""&&$("flwName").value!="处理问题"&&$("flwName").value!="处理人"){
			proLog="("+$("flwName").value+")" +proLog;
		}
		if(appendVerStr!=""){
			proLog ="("+appendVerStr+":" +$("currVerName").value+")"+proLog;
		}
		return proLog;
	}
	
	function getUserNameById(id,selStrName){
		var selStrValArr = parent.$(selStrName).value.split("$");
		for(var i=0; i<selStrValArr.length; i++){
			if(selStrValArr[i].indexOf(id)>=0){
				return selStrValArr[i].split(";")[1];
			}
		}
		return $("nextOwnName").value;
	}
	function remarkChk(){
		var chgState = $("stateIdList").value;
		if(chgState=="2"||chgState=="4"||chgState=="5"||chgState=="6"||chgState=="8"||chgState=="9"||chgState=="11"||chgState=="12"||chgState=="15"||chgState=="16"||chgState=="17"||chgState=="19"||chgState=="20"||chgState=="22"||chgState=="23"){
			if(remarkInit==$("currRemark").value.replace(/\s+$|^\s+/g,"")){
				$("currRemark").value="";
			}				
			if(isAllNull($("currRemark").value)){
				hintMsg('请填写备注');
				return false;			
			}	
		}
		if(checkIsOverLong($("currRemark").value,200)){
			hintMsg('备注不能超过200字符');
			return false;			
		}
		$("currRemark").value=$("currRemark").value.replace(/\s+$|^\s+/g,"");
		return true;
	}
	var detlW_ch;
	function repeDetl(bugId){
		detlW_ch = initW_ch(detlW_ch, "", true, 800, 520,'detlW_ch');
		var url= conextPath +"/bugManager/bugManagerAction!viewBugDetal.action";
		if(typeof bugId!="undefined"){
			url+="?dto.bug.bugId="+bugId;
		}else{
			url+="?dto.bug.bugId="+$("withRepteId").value;
		}
		detlW_ch.attachURL(url);	
	    detlW_ch.show();
	    detlW_ch.bringToTop();
	    detlW_ch.setModal(true);
	    if(repRela){
	    	detlW_ch.setText("重复BUG明细");
	    }else{
	    	detlW_ch.setText("软件问题报告明细");
	    }
	}
	var repRela=true;
    function repeatRela(repeFind){
    	if(typeof repeFind!="undefined"){
    		repRela = false;
    	}else{
    		 repRela=true;
    	}
    	parent.findInit(window);
    }
    var repeWin,repeGrid,repeBar;
    function popRepeWin(){
    	var selData = parent.$("listStr").value;
    	var datas = selData.split("$");
    	if(datas[1]==""){
    		setPageNoSizeCount(datas[0],repeBar);
    		repeGrid.clearAll();
    		return;
    	}
		if(typeof erpreWin == "undefined"){
			repeWin = initW_ch(repeWin, "", true, 615, 430,'repeWin');
			repeBar = repeWin.attachToolbar();
			repeGrid = repeWin.attachGrid();
			repeGrid.setImagePath(conextPath+"/dhtmlx/grid/codebase/imgs/");
		    repeGrid.setHeader("&nbsp;,编号,Bug描述,状态,&nbsp;,&nbsp;,&nbsp;,&nbsp;,&nbsp;,测试人员,开发人员,报告日期,&nbsp;,&nbsp;,&nbsp;,&nbsp;,&nbsp;,&nbsp;,&nbsp;,&nbsp;,&nbsp;");
		    repeGrid.setInitWidths("0,50,*,80,0,0,0,0,0,80,80,100,0,0,0,0,0,0,0,0,0");
		    repeGrid.setColAlign("center,center,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left");
		    repeGrid.setColTypes("ra,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro");
		    repeGrid.setColSorting("int,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str");
			repeGrid.attachEvent("onCheckbox",doOnCheck);
			repeGrid.enableAutoHeight(true, 280);
			repeGrid.enableRowsHover(true, "red");
		    repeGrid.init();
		    repeGrid.enableTooltips("false,true,true,true,false,false,false,false,false,true,true,true,false,false,false,false,false,false,false,false,false");
		    repeGrid.setSkin("light");
			repeBar.setIconsPath(conextPath+"/dhtmlx/toolbar/images/");
			repeBar.addButton("first", 13, "", "first.gif", "first.gif");
			repeBar.setItemToolTip("first", "第一页");
			repeBar.addButton("pervious",2, "", "pervious.gif", "pervious.gif");
			repeBar.setItemToolTip("pervious", "上一页");
			repeBar.addSlider("slider",3, 80, 1, 30, 1, "", "", "%v");
			repeBar.setItemToolTip("slider", "滚动条翻页");
			repeBar.addButton("next",4, "", "next.gif", "next.gif");
			repeBar.setItemToolTip("next", "下一页");
			repeBar.addButton("last",5, "", "last.gif", "last.gif");
			repeBar.setItemToolTip("last", "末页");
			repeBar.addInput("page",6, "", 25);
			repeBar.addText("pageMessage",7, "");
			repeBar.addText("pageSizeText",8, "每页");
			var opts = Array(Array('id1', 'obj', '10'), Array('id2', 'obj', '15'), Array('id3', 'obj', '20'), Array('id4', 'obj', '25'));
			repeBar.addButtonSelect("pageP",9, "page", opts);
			repeBar.setItemToolTip("pageP", "每页记录数");
			repeBar.addText("pageSizeTextEnd",10, "条");
			repeBar.addSeparator("11",11);
			repeBar.addButton("chgQuery",12, "更改查询条件查询", "");
			
		 	repeBar.attachEvent("onValueChange", function(id, pageNo) {
				parent.pageAction(pageNo, pageSize);
				return;
			});
			repeBar.attachEvent("onEnter", function(id, value) {
				if(!isDigit(value, false)){
					repeBar.setValue("page", pageNo);
					return;
				}
				var pageNoTemp = parseInt(value);
				if(pageNoTemp < 1){
					pageNoTemp = 1;
				}else if(pageNoTemp > pageCount){
					pageNoTemp = pageCount;
				}
				parent.pageAction(pageNoTemp, pageSize);
				return;
			});
			repeBar.attachEvent("onClick", function(id) {
				if(id=="chgQuery"){
					if(repRela){
						repeatRela();
					}else{
						repeatRela('repRela');
					}		
				}else if(id == "first"){
					pageNo = 1;
					parent.pageAction(pageNo, pageSize);
				}else if(id == "last"){
					pageNo = pageCount;
					parent.pageAction(pageNo, pageSize);
				}else if(id == "next"){
					pageNo = pageNo +1
					parent.pageAction(pageNo, pageSize);
				}else if(id == "pervious"){
					pageNo = pageNo -1
					parent.pageAction(pageNo, pageSize);
				}else if(id == "id1" || id == "id2" || id == "id3" || id == "id4" || id == "id5"){
					var pageSizeTemp = parseInt(repeBar.getListOptionText("pageP", id));
					if(pageSize==pageSizeTemp){
						return;
					}
					pageSize = pageSizeTemp;
					parent.pageAction(pageNo, pageSize);
				}
			});			
			
		    repeGrid.attachEvent("OnCheck",function(rowId,index){
				var bugId = repeGrid.getSelectedId();
				if($("bugId").value==bugId){
					repeGrid.setRowColor(bugId,"#339933");
					hintMsg("不能选择自身");
					repeGrid.cells(rowId,0).setValue(false);
					return;
				}
				//yyyy-MM-dd hh:mm:ss
				var discDateStr = repeGrid.cells(bugId,11).getValue();
				var discDateStrArr = discDateStr.split(" ");
				var discDate = new Date(discDateStrArr[0].split("-")[0], discDateStrArr[0].split("-")[1], discDateStrArr[0].split("-")[2], discDateStrArr[1].split(":")[0], discDateStrArr[1].split(":")[1], discDateStrArr[1].split(":")[2]);
				discDateStrArr = myDiscDate.split(" ");
				var currBugdiscDate = new Date(discDateStrArr[0].split("-")[0], discDateStrArr[0].split("-")[1], discDateStrArr[0].split("-")[2], discDateStrArr[1].split(":")[0], discDateStrArr[1].split(":")[1], discDateStrArr[1].split(":")[2]);
				if((currBugdiscDate.getTime() - discDate.getTime())<0){
					hintMsg("所选BUG发现日期在当前处理BUG发现日期之后，应反过来关联",400);
					repeGrid.cells(rowId,0).setValue(false);
					return;				
				}
				$("withRepteId").value=bugId;
				$("repeDetBtn").style.display="";
				repeWin.hide();
			});
		}  
		repeWin.setModal(false);
		if(repRela){
	    	repeWin.setText("重复BUG选择---单击序号查看明细");
	    	repeGrid._setColumnSizeR(0,40);
	    	repeGrid.hdr.rows[1].cells[0].innerHTML="选择";
	    	repeWin.setDimension(655, 360);
	    }else{
	    	repeWin.setText("查询列表---单击序号查看明细");
	    	repeGrid._setColumnSizeR(0,0);
	    	repeGrid.hdr.rows[1].cells[0].innerHTML="";
	    	repeWin.setDimension(615, 360);
	    }
	    colTypeReset();
		repeGrid.clearAll();
    	jsons = eval("(" + datas[1] +")");
    	repeGrid.parse(jsons, "json");
      	setPageNoSizeCount(datas[0],repeBar);
    	//setRowNum(repeGrid);  	
    	loadLink();
    	if(repRela&&$("withRepteId").value!=""&&repeGrid.getRowIndex($("withRepteId").value)>=0){
    		repeGrid.setRowColor($("withRepteId").value, "#CCCCCC");
    		repeGrid.cells($("withRepteId").value,0).setValue(true);
    	}
    	if(repRela&&repeGrid.getRowIndex($("bugId").value)>=0){
    		repeGrid.setRowColor($("bugId").value, "#339933");
    	}
    	if(!repRela&&$("bugId").value!=""&&repeGrid.getRowIndex($("bugId").value)>=0){
    		repeGrid.setRowColor($("bugId").value, "#339933");
    	}
		repeWin.bringToTop(); 
		repeWin.show(); 
    }
	function loadLink(){
		for(var i = 0; i <repeGrid.getRowsNum(); i++){
			repeGrid.cells2(i,1).cell.innerHTML="<a href='javascript:repeDetl("+repeGrid.getRowId(i)+")' title='查看详情'><font color='#339933'>"+getTttle2(i,1)+"</font></a>";
		}
		sw2Link();
	}  
	function getTttle2(rowNum,colIn){
		return repeGrid.cells2(rowNum,colIn).getValue();
	} 
	function colTypeReset(){
		repeGrid.setColTypes("ra,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro");
	}
	function sw2Link(){
		repeGrid.setColTypes("ra,link,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro");
	}
	function getMdPerson(loadType){
		var url = conextPath + "/bugManager/bugManagerAction!loadMdPerson.action?dto.currNodeId="+$("moduleId").value+"&dto.loadType="+loadType;
		var ajaxRest = postSub(url,"");
		if(ajaxRest=="failed"){
			if(loadType=="1")
				hintMsg('初始化测试需求对应开发人员失败');
			else 
			    hintMsg('初始化测试需求对应分配人员失败');
			return;
		}else if(ajaxRest==""){
			if(loadType=="1")
				hintMsg('当前测试需求没指定开发人员');
			else
			    hintMsg('当前测试需求没指定分配人员');
			return;		
		}
		parent.$("moduleDevStr").value=ajaxRest;
		if(loadType=="1")
			popSelWin('devOwnerId','nextOwnName','moduleDevStr','修改人');
		else
			popSelWin('assinOwnerId','nextOwnName','moduleDevStr','分配人');
	}