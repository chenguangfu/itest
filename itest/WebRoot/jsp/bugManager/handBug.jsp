<%@ page contentType="text/html; charset=UTF-8"%>
<%@taglib uri="/webwork" prefix="ww"%>
<%@taglib uri="/WEB-INF/pmButton.tld" prefix="pmTag"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<HTML>
	<HEAD>
		<TITLE>BUG管理</TITLE>
		<META content="text/html; charset=UTF-8" http-equiv=Content-Type>
		<link rel="STYLESHEET" type="text/css" href="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgrid.css">
		<link rel="STYLESHEET" type="text/css" href="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgrid_skins.css">
		<link rel='STYLESHEET' type='text/css' href='<%=request.getContextPath()%>/css/page2.css'>
		<script type="text/javascript" src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxcommon.js"></script>
		<script type="text/javascript" src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgrid.js"></script>
		<script type="text/javascript" src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgridcell.js"></script>
		<script type="text/javascript" src="<%=request.getContextPath()%>/pmEditor/fckeditor.js"></script>
		<link rel="STYLESHEET" type="text/css" href="<%=request.getContextPath()%>/dhtmlx/toolbar/codebase/skins/dhtmlxtoolbar_dhx_blue.css">
	    <script type="text/javascript" src="<%=request.getContextPath()%>/dhtmlx/toolbar/codebase/dhtmlxtoolbar.js"></script>
	    <script type="text/javascript" src="<%=request.getContextPath()%>/dhtmlx/windows/codebase/dhtmlxwindows.js"></script>
	    <script type="text/javascript" src="<%=request.getContextPath()%>/dhtmlx/windows/codebase/ext/dhtmlxwindows_wtb.js"></script>
	   	<link rel="STYLESHEET" type="text/css" href="<%=request.getContextPath()%>/dhtmlx/windows/codebase/dhtmlxwindows.css">
	   <link rel="STYLESHEET" type="text/css" href="<%=request.getContextPath()%>/dhtmlx/windows/codebase/skins/dhtmlxwindows_dhx_blue.css">
	   <script type="text/javascript" src="<%=request.getContextPath()%>/js/globalVariable.js"></script>
	   <script type="text/javascript" src="<%=request.getContextPath()%>/js/commonFunction_top.js"></script>
	</HEAD>
	<BODY  onload="loadFCK()" style="overflow-y:auto;">
	<script type="text/javascript">
	var myId = "${session.currentUser.userInfo.id}";
	var chargeOwnerInit = "${dto.bug.chargeOwner}";
	var chargeOwnerNameInit = "${dto.bug.chargeOwnerName}";
    var testOwnInit = "${dto.bug.testOwnerId}";
    var devOwnInit = "${dto.bug.devOwnerId}";
    var anasOwnInit = "${dto.bug.analyseOwnerId}";
    var asinOwnInit = "${dto.bug.assinOwnerId}";
    var inteOwnInit = "${dto.bug.intercessOwnerId}";
    var testOwnNameInit = "${dto.bug.testOwner.name}";
    var devOwnNameInit = "${dto.bug.devOwner.name}";
    var anasOwnNameInit = "${dto.bug.analysOwner.name}";
    var asinOwnNameInit = "${dto.bug.assinOwner.name}";
    var inteOwnNameInit = "${dto.bug.intecesOwner.name}";
    var verLableInit="${dto.bug.versionLable}";
    var verValInit="${dto.bug.currVersion.versionNum}";
    var fixVerInit ="${dto.bug.fixVer}";
    var verifyVerInit ="${dto.bug.verifyVer}";
    var reProStep ="${dto.bug.reProStep2}";
    var nextFlwCdInit ="${dto.bug.nextFlowCd}";
    var dueDateInit ="<fmt:formatDate value="${dto.bug.bugAntimodDate}" pattern="yyyy-MM-dd" />";
    var repDate ="<fmt:formatDate value="${dto.bug.reptDate}" pattern="yyyy-MM-dd" />";
    var workLoad = "${dto.bug.planAmendHour}";
    var initStateName ="${dto.stateName}";
    var remarkInit ="${dto.bug.currRemark}";
    var moduleInitId ="${dto.bug.moduleId}";
    var bugDescInit = "${dto.bug.bugDesc}";
    var impPhaInit ="${dto.bug.genePhaseId}";
    var reproPerInit="${dto.bug.reproPersent}";
    var priIdInit = "${dto.bug.priId}";
	var geneCauseInit = "${dto.bug.geneCauseId}";
	var bugOccaInit = "${dto.bug.bugOccaId}";
    var sourceInit = "${dto.bug.sourceId}";
    var platformInit = "${dto.bug.platformId}";
    var bugGradeInit = "${dto.bug.bugGradeId}";
    var bugTypeInit = "${dto.bug.bugTypeId}";
    var reProTxtInit = "${dto.bug.reProTxt}";
    var nextOwnerIdInit = "${dto.bug.nextOwnerId}";
	if("${dto.bug.typeSelStr}"!=""){
		parent.$("typeSelStr").value="${dto.bug.typeSelStr}";  
		parent.$("gradeSelStr").value="${dto.bug.gradeSelStr}";     
		parent.$("freqSelStr").value="${dto.bug.freqSelStr}";      
		parent.$("occaSelStr").value="${dto.bug.occaSelStr}";      
		parent.$("geCaseSelStr").value="${dto.bug.geCaseSelStr}";    
		parent.$("sourceSelStr").value="${dto.bug.sourceSelStr}";    
		parent.$("plantFormSelStr").value="${dto.bug.plantFormSelStr}";  
		parent.$("priSelStr").value="${dto.bug.priSelStr}"; 
		parent.$("verSelStr").value="${dto.bug.verSelStr}";    
	}
	if("${dto.bug.genePhaseSelStr}"!=""){
		parent.$("genePhaseSelStr").value="${dto.bug.genePhaseSelStr}";  
	}
	parent.$("assignSelStr").value="${dto.bug.assignSelStr}";      
	parent.$("analySelStr").value="${dto.bug.analySelStr}"; 
	parent.$("testSelStr").value="${dto.bug.testLdIdSelStr}";
	parent.$("devStr").value="${dto.bug.devStr}"; 
	parent.$("devChkIdSelStr").value="${dto.bug.devChkIdSelStr}";      
	parent.$("testChkIdSelStr").value="${dto.bug.testChkIdSelStr}"; 
	parent.$("testLdIdSelStr").value="${dto.bug.testLdIdSelStr}";
	parent.$("interCesSelStr").value="${dto.bug.interCesSelStr}";
	parent.roleStr="${dto.roleInTask}";
	parent.flwStr="${dto.testFlow}";    
	var flwStr ="${dto.testFlow}";
	var roleStr ="${dto.roleInTask}"; 
	var ctrlInfo = "${dto.bug.staFlwMemStr}".split("$");  
 	var flwPageNo=1,flwPageCount=0,oEditor,flwBar;
	function loadFCK(){
		if(typeof oEditor != "undefined"){
			oEditor.SetData(reProStep) ;	
			return;
		}
    	var pmEditor = new FCKeditor('reProStep') ;
    	pmEditor.BasePath = conextPath+"/pmEditor/" ;
    	pmEditor.Height = 200;
    	pmEditor.ToolbarSet = "Basic" ; 
    	pmEditor.ToolbarStartExpanded=false;
    	pmEditor.ReplaceTextarea();
	}
	function FCKeditor_OnComplete( editorInstance ){
		oEditor = FCKeditorAPI.GetInstance('reProStep') ;
		oEditor.SetData(recovJsonKeyWord(reProStep,"notRepComma"));	
		return;
	}
	parent.detlW_ch.setText("处理软件问题报告");	
	</script>
		<div align="center">
		<table border="0" class="obj row20px" cellspacing="0" align="center" cellpadding="0" border="0" width="100%">
		  <tr>
	    	<td class="jihuo" id="baseInfoTab" width="60" class="tdtxt" align="right"><strong><a onclick="styleChange(1)"  href="javascript:void(0);">基本信息</a></strong></td>
	    	<td class="nojihuo" id="flowInfoTab" width="60" class="tdtxt" align="left"><strong><a onclick="styleChange(2)"  href="javascript:void(0);">处理历史</a></strong></td>
		    <td colspan="6" width="560">&nbsp;</td>
		  </tr>
		</table>
		</div>
		<div id="baseInfoDiv" align="center" class="cycleTask gridbox_light" style="border:0px;">
		  <div class="objbox" style="overflow:auto;width:100%;">
			 <ww:hidden id="typeSelStr" name="dto.bug.typeSelStr" ></ww:hidden>
			 <ww:hidden id="gradeSelStr" name="dto.bug.gradeSelStr" ></ww:hidden>
			 <ww:hidden id="freqSelStr" name="dto.bug.freqSelStr" ></ww:hidden>
			 <ww:hidden id="occaSelStr" name="dto.bug.occaSelStr" ></ww:hidden>
			 <ww:hidden id="geCaseSelStr" name="dto.bug.geCaseSelStr"></ww:hidden>
			 <ww:hidden id="sourceSelStr" name="dto.bug.sourceSelStr" ></ww:hidden>
			 <ww:hidden id="plantFormSelStr" name="dto.bug.plantFormSelStr" ></ww:hidden>
			 <ww:hidden id="genePhaseSelStr" name="dto.bug.genePhaseSelStr" ></ww:hidden>
			 <ww:hidden id="priSelStr" name="dto.bug.priSelStr" ></ww:hidden>
			 <ww:hidden id="testSelStr" name="dto.bug.testSelStr" ></ww:hidden>
			 <ww:hidden id="devStr" name="dto.bug.devStr" ></ww:hidden>
			 <ww:hidden id="assignSelStr" name="dto.bug.assignSelStr" ></ww:hidden>
			 <ww:hidden id="analySelStr" name="dto.bug.analySelStr"></ww:hidden>
			 <ww:hidden id="interCesSelStr" name="dto.bug.interCesSelStr" ></ww:hidden>
			 <ww:hidden id="roleInTask" name="dto.roleInTask"></ww:hidden>
			<ww:hidden id="testFlow" name="dto.testFlow"></ww:hidden>
			<input type="hidden" id="listStr" name="listStr" value=""/>
			<ww:hidden id="initReProStep" name="initReProStep"></ww:hidden>
			<ww:hidden id="testPhase" name="dto.bug.testPhase"></ww:hidden>
			<ww:hidden id="flwName" name="dto.bug.modelName"></ww:hidden>
			<ww:hidden id="currUserId" name="currUserId" value="${session.currentUser.userInfo.id}"></ww:hidden>
		  <ww:form theme="simple" method="post" id="hanfForm" name="hanfForm" namespace="" action="">
			 <ww:hidden id="bugId" name="dto.bug.bugId"></ww:hidden>
			 <ww:hidden id="processLog" name="dto.bug.processLog"></ww:hidden>
	     	 <ww:hidden id="reProTxt" name="dto.bug.reProTxt"></ww:hidden>
			 <ww:hidden id="bugReptId" name="dto.bug.bugReptId"></ww:hidden>
			 <ww:hidden id="currHandlerId" name="dto.bug.currHandlerId"></ww:hidden>
			 <ww:hidden id="currStateId" name="dto.bug.currStateId"></ww:hidden>
			 <ww:hidden id="nextFlowCd" name="dto.bug.nextFlowCd"></ww:hidden>
			 <ww:hidden id="currFlowCd" name="dto.bug.currFlowCd"></ww:hidden>
			 <ww:hidden id="currHandlDate" name="dto.bug.currHandlDate"></ww:hidden>
			 <ww:hidden id="bugReptVer" name="dto.bug.bugReptVer"></ww:hidden>
			 <ww:hidden id="fixVer" name="dto.bug.fixVer"></ww:hidden>
			 <ww:hidden id="verifyVer" name="dto.bug.verifyVer"></ww:hidden>
			 <ww:hidden id="reptDate" name="dto.bug.reptDate"></ww:hidden>
			 <ww:hidden id="msgFlag" name="dto.bug.msgFlag"></ww:hidden>
			 <ww:hidden id="relaCaseFlag" name="dto.bug.relaCaseFlag"></ww:hidden>
			 <ww:hidden id="withRepteId" name="dto.bug.withRepteId" ></ww:hidden>
			 <ww:hidden id="initState" name="dto.bug.initState" ></ww:hidden>
		     <ww:hidden id="testOwnerId" name="dto.bug.testOwnerId"></ww:hidden>
		     <ww:hidden id="analyseOwnerId" name="dto.bug.analyseOwnerId"></ww:hidden>
		     <ww:hidden id="assinOwnerId" name="dto.bug.assinOwnerId"></ww:hidden>
		     <ww:hidden id="devOwnerId" name="dto.bug.devOwnerId"></ww:hidden>
		     <ww:hidden id="intercessOwnerId" name="dto.bug.intercessOwnerId"></ww:hidden>
		     <ww:hidden id="attachUrl" name="dto.bug.attachUrl"></ww:hidden>
		     <ww:hidden id="bugReptVerName" name="dto.bug.reptVersion.versionNum"></ww:hidden>
		     <ww:hidden id="nextOwnerId" name="dto.bug.nextOwnerId"></ww:hidden>
			 <table  border="0" id="baseTable" class="obj row20px" cellspacing="0" align="center" cellpadding="0" border="0" width="100%">
			    <tr>
			      <td width="80" class="rightM_center" style="color:red;border-right:0" align="right" >测试需求:</td>
			      <td  class="tdtxt"  align="left" colspan="5"  width="640" style="padding:2 0 0 4;" >
			      <ww:hidden id="moduleId" name="dto.bug.moduleId"></ww:hidden>
			      <c:choose>
			      <c:when test="${dto.bug.bugReptId==session.currentUser.userInfo.id||fn:indexOf(dto.roleInTask,'1')>=0||fn:indexOf(dto.roleInTask,'2')>=0||fn:indexOf(dto.roleInTask,'8')>=0}">
			      	<ww:textfield id="moduleName" cssClass="text_c" name="dto.moduleName" cssStyle="width:640;padding:2 0 0 4;" onblur="javascript:this.readonly='true';" onfocus="parent.loadTree(window)"></ww:textfield>
			      </c:when>
			      <c:when test="${dto.bug.bugReptId!=session.currentUser.userInfo.id&&fn:indexOf(dto.roleInTask,'1')<0&&fn:indexOf(dto.roleInTask,'2')<0&&fn:indexOf(dto.roleInTask,'8')<0}">
			      <ww:textfield id="moduleName" cssClass="text_c" name="dto.moduleName" cssStyle="width:640;padding:2 0 0 4;" readonly="true"></ww:textfield>
			      </c:when>
			      </c:choose>
    			  </td>
			    </tr>
			    <tr>
	 		     <td width="80" class="rightM_center" align="right" style="border-right:0">当前状态:</td>
	 			 <td  class="tdtxt"  align="left"   width="160" style="padding:2 0 0 4;border-right:0">
	 			   <font color="blue"><ww:property value="dto.stateName"/></font>
	 		     </td>
	 		     <td width="80" class="rightM_center" align="right" style="padding:2 0 0 4;border-right:0">
	 			   <ww:property value="dto.currOwner" escape="false"/>
	 		     </td>	
	 		     <td width="80" class="rightM_center" align="right" style="border-right:0">作者:</td>
	 			 <td  class="tdtxt"  align="left"  width="160" style="padding:2 0 0 4;border-right:0">
	 			   <ww:property value="dto.bug.author.uniqueName"/>
	 		     </td>		    
			    </tr>
				<tr id="testOwnerTr">
				     <td  width="80" class="rightM_center" style="color:red;border-right:0" align="right" >测试人员:</td>
				     <td  class="tdtxt" width="160" style="padding:2 0 0 4;border-right:0">
				     <ww:textfield id="testOwnName" cssClass="text_c" name="dto.bug.testOwner.uniqueName" cssStyle="width:160;padding:2 0 0 4;"   onblur="javascript:this.readonly='true';" onfocus="popSelWin('testOwnerId','testOwnName','testSelStr','测试人员')"></ww:textfield>
				     </td>
				     <c:if test="${not empty dto.bug.devOwnerId}">
				       <td  width="80" class="rightM_center" style="color:red;border-right:0" align="right" nowrap>开发人员:</td>
				       <td  class="tdtxt" width="160" style="padding:2 0 0 4;border-right:0">
				        <ww:textfield id="devOwnerName" cssClass="text_c" name="dto.bug.devOwner.uniqueName" cssStyle="width:160;padding:2 0 0 4;"   onblur="javascript:this.readonly='true';" onfocus="popSelWin('devOwnerId','devOwnerName','devStr','修改人')"></ww:textfield>
				      </td>
				     </c:if>
				     <c:if test="${empty dto.bug.devOwnerId}">
				       <td  width="80" class="rightM_center"  style="border-right:0" align="right" nowrap>开发人员:</td>
				       <td  class="tdtxt" id="devOwnerTd" width="160" style="padding:2 0 0 4;border-right:0">
				                    暂未指定
				      </td>
				    </c:if>
				<c:choose>
				 <c:when test="${dto.bug.analyseOwnerId==session.currentUser.userInfo.id||fn:indexOf(dto.roleInTask,'7')>=0||fn:indexOf(dto.roleInTask,'4')>=0||dto.bug.devOwnerId==session.currentUser.userInfo.id||fn:indexOf(dto.roleInTask,'6')>=0}">
		 		     <td width="80"class="rightM_center" style="border-right:0" align="right" >责任人:</td>
		 			 <td  class="tdtxt" width="160" style="padding:2 0 0 4;border-right:0">
		 			 <ww:hidden id="chargeOwner" name="dto.bug.chargeOwner"></ww:hidden>
		 			  <ww:textfield id="chargeOwnerName" cssClass="text_c" name="dto.bug.chargeOwnerName" cssStyle="width:160;padding:2 0 0 4;"  readonly='true' onblur="javascript:this.readonly='true';" onfocus="popselWinInAll('chargeOwner','chargeOwnerName',true)"></ww:textfield>
		 		     </td>	
				  </c:when>
				  <c:when test="${not empty dto.bug.chargeOwner}">
		 		     <td width="80"class="rightM_center" style="border-right:0" align="right" >责任人:</td>
		 			 <td  class="tdtxt" width="160" style="padding:2 0 0 4;border-right:0">
		 			 <ww:hidden id="chargeOwner" name="dto.bug.chargeOwner"></ww:hidden>
		 			  <ww:property value="dto.bug.chargeOwnerName"/>
		 		     </td>					  
				  </c:when>
			      <c:otherwise>
		 		     <td width="80"class="rightM_center" style="border-right:0" align="right" >责任人:</td>
		 			 <td  class="tdtxt" width="160" style="padding:2 0 0 4;border-right:0">
		 			 <ww:hidden id="chargeOwner" name="dto.bug.chargeOwner"></ww:hidden>
		 			  <ww:property value="dto.bug.chargeOwnerName"/>
		 		     </td>				   
			       </c:otherwise>
			   </c:choose> 				   
				</tr>
				<c:choose>
			      <c:when test="${dto.bug.analyseOwnerId==session.currentUser.userInfo.id||fn:indexOf(dto.roleInTask,'7')>=0||fn:indexOf(dto.roleInTask,'4')>=0}">
			      <tr>
				       <td  width="100" class="rightM_center"  style="border-right:0"  nowrap>修改截止日期:</td>
				       <td  class="tdtxt" width="160" style="padding:2 0 0 4;border-right:0">
				       	<ww:textfield id="bugAntimodDate"  name="dto.bug.bugAntimodDate" value=""
								readonly="true" cssStyle="width:140;padding:2 0 0 4;border-right:0"   cssClass="text_c"  onclick="showCalendar(this);"></ww:textfield>
				      </td>
				       <td  width="80" class="rightM_center"  align="right" style="border-right:0" nowrap>工作量:</td>
				       <td  class="tdtxt"  style="padding:2 0 0 4;width:160;border-right:0" nowrap>
				      	<ww:textfield id="planAmendHour" name="dto.bug.planAmendHour" cssClass="text_c"
								 cssStyle="width:120;padding:2 0 0 4;" onkeypress="javascript:return numChk(event);"></ww:textfield>(小时)
				      </td>
				  </tr>
			    </c:when>
			     <c:when test="${not empty dto.bug.bugAntimodDate&&dto.bug.analyseOwnerId!=session.currentUser.userInfo.id&&fn:indexOf(dto.roleInTask,'7')<0&&fn:indexOf(dto.roleInTask,'4')<0}">
			      <tr>
				       <td  width="80" class="rightM_center"  style="border-right:0" nowrap>修改截止日期:</td>
				       <td  class="tdtxt" width="160" style="padding:2 0 0 4;border-right:0">
				       <ww:hidden id="bugAntimodDate" name="dto.bug.bugAntimodDate" ></ww:hidden>
				       <fmt:formatDate value="${dto.bug.bugAntimodDate}" pattern="yyyy-MM-dd" />
				      </td>
				       <td  width="160" class="rightM_center"   style="border-right:0" align="right" nowrap>工作量:</td>
				       <td  class="tdtxt" width="80" style="padding:2 0 0 4;border-right:0">
				       <ww:hidden id="planAmendHour" name="dto.bug.planAmendHour" ></ww:hidden>
				       <ww:property value="dto.bug.planAmendHour" />(小时)
				      </td>
				  </tr>
			    </c:when>
			   <c:otherwise>
			   </c:otherwise>
			   </c:choose> 
			    <tr>
			      <td width="80" class="rightM_center" style="color:red;border-right:0" align="right" >Bug描述:</td>
			      <td  class="tdtxt" colspan="5" width="640" style="padding:2 0 0 4;border-right:0">
			      <c:choose> 
			      <c:when test="${dto.bug.bugReptId==session.currentUser.userInfo.id||fn:indexOf(dto.roleInTask,'1')>=0||fn:indexOf(dto.roleInTask,'2')>=0||fn:indexOf(dto.roleInTask,'8')>=0}">
    			      <ww:textfield id="bugDesc" cssClass="text_c" name="dto.bug.bugDesc" cssStyle="width:640;padding:2 0 0 4;"></ww:textfield>
    			 </c:when>
    			 <c:when test="${dto.bug.bugReptId!=session.currentUser.userInfo.id&&fn:indexOf(dto.roleInTask,'1')<0&&fn:indexOf(dto.roleInTask,'2')<0&&fn:indexOf(dto.roleInTask,'8')<0}">
    			 	<ww:textfield id="bugDesc" cssClass="text_c" name="dto.bug.bugDesc" cssStyle="width:640;padding:2 0 0 4;" readonly="true" ></ww:textfield>
    			 </c:when>
    			 </c:choose>
    			 </td>
			    </tr>
			   <tr>
	 		     <td width="80" class="rightM_center" style="color:red;border-right:0"align="right">类型:</td>
	 			 <td  class="tdtxt" width="160" style="padding:2 0 0 4;border-right:0">
	 			 <ww:hidden id="bugTypeId" name="dto.bug.bugTypeId" onkeypress="javascript:return;"></ww:hidden>
	 			   <ww:textfield id="bugTypeName" cssClass="text_c" name="dto.bug.bugType.typeName" cssStyle="width:160;padding:2 0 0 4;" readonly='true'  onblur="javascript:this.readonly='true';" onfocus="popSelWin('bugTypeId','bugTypeName','typeSelStr','类型')"></ww:textfield>
	 		     </td>	
	 		     <td width="80" class="rightM_center" style="color:red;border-right:0" align="right">等级:</td>
	 			 <td  class="tdtxt" width="160" style="padding:2 0 0 4;border-right:0">
	 			 <ww:hidden id="bugGradeId" name="dto.bug.bugGradeId"></ww:hidden>
	 			   <ww:textfield id="bugGradeName" cssClass="text_c" name="dto.bug.dtoHelper.bugGrade.typeName" cssStyle="width:160;padding:2 0 0 4;" readonly='true'  onblur="javascript:this.readonly='true';" onfocus="popSelWin('bugGradeId','bugGradeName','gradeSelStr','等级')"></ww:textfield>
	 		     </td>
	 		     <td width="80"class="rightM_center" style="color:red;border-right:0" align="right" >再现平台:</td>
	 			 <td  class="tdtxt" width="160" style="padding:2 0 0 4;border-right:0">
	 			 <ww:hidden id="platformId" name="dto.bug.platformId"></ww:hidden>
	 			  <ww:textfield id="pltfomName" cssClass="text_c" name="dto.bug.occurPlant.typeName" cssStyle="width:160;padding:2 0 0 4;"  readonly='true' onblur="javascript:this.readonly='true';" onfocus="popSelWin('platformId','pltfomName','plantFormSelStr','发生平台')"></ww:textfield>
	 		     </td>	
			   </tr>
			   <tr>
	 		     <td width="80" class="rightM_center" style="color:red;border-right:0" align="right" >来源:</td>
	 			 <td  class="tdtxt" width="160" style="padding:2 0 0 4;border-right:0">
	 			  <ww:hidden id="sourceId" name="dto.bug.sourceId"></ww:hidden>
	 			  <ww:textfield id="sourceName" cssClass="text_c" name="dto.bug.bugSource.typeName" cssStyle="width:160;padding:2 0 0 4;"  readonly='true' onblur="javascript:this.readonly='true';" onfocus="popSelWin('sourceId','sourceName','sourceSelStr','来源')"></ww:textfield>
	 		     </td>
	 		     <td width="80" class="rightM_center" style="color:red;border-right:0" align="right" >发现时机:</td>
	 			 <td  class="tdtxt" width="160" style="padding:2 0 0 4;border-right:0">
	 			  <ww:hidden id="bugOccaId" name="dto.bug.bugOccaId"></ww:hidden>
	 			  <ww:textfield id="occaName" cssClass="text_c" name="dto.bug.bugOpotunity.typeName" cssStyle="width:160;padding:2 0 0 4;"  readonly='true' onblur="javascript:this.readonly='true';" onfocus="popSelWin('bugOccaId','occaName','occaSelStr','发现时机')"></ww:textfield>
	 		     </td>	
	 		     <ww:if test="${dto.bug.geneCauseId}==null||${dto.bug.geneCauseId}==''">
		 		     <td width="80" id="geneCauseTd" class="rightM_center" style="color:red;display: none;border-right:0" align="right" >测试时机:</td>
		 			 <td  class="tdtxt" id="geneCauseF" width="160" style="padding:2 0 0 4;border-right:0">
		 			 <ww:hidden id="geneCauseId" name="dto.bug.geneCauseId"></ww:hidden>
		 			 <ww:textfield id="geneCaseName" cssClass="text_c" name="dto.bug.geneCause.typeName" cssStyle="width:160;padding:2 0 0 4;"  readonly='true' onblur="javascript:this.readonly='true';" onfocus="popSelWin('geneCauseId','geneCaseName','geCaseSelStr','测试时机')"></ww:textfield>
		 		     </td>	
	 		      </ww:if>	
	 		       <ww:else>
		 		     <td width="80"  class="rightM_center" style="color:red;border-right:0" align="right">
		 		       <div id="geneCauseTd" style="display:none"> 测试时机:</div>
		 		     </td>
		 			 <td  class="tdtxt"  width="160" style="padding:2 0 0 4;border-right:0">
		 			 <ww:hidden id="geneCauseId" name="dto.bug.geneCauseId"></ww:hidden>
		 			  <div id="geneCauseF"" style="display:none">
		 			   <ww:textfield id="geneCaseName" cssClass="text_c" name="dto.bug.geneCause.typeName" cssStyle="width:160;padding:2 0 0 4;" readonly='true'  onblur="javascript:this.readonly='true';" onfocus="popSelWin('geneCauseId','geneCaseName','geCaseSelStr','测试时机')"></ww:textfield>
		 		     </div>
		 		     </td>		 		       
	 		       </ww:else>	
			   </tr>
			   <tr>
	 		     <td width="80" class="rightM_center" style="color:red;border-right:0" align="right">优先级:</td>
	 			 <td  class="tdtxt" width="160" style="padding:2 0 0 4;border-right:0">
	 			 <ww:hidden id="priId" name="dto.bug.priId"></ww:hidden>
	 			  <ww:textfield id="priName" cssClass="text_c" name="dto.bug.dtoHelper.bugPri.typeName" cssStyle="width:160;padding:2 0 0 4;"   readonly='true' onblur="javascript:this.readonly='true';" onfocus="popSelWin('priId','priName','priSelStr','优先级')"></ww:textfield>
	 		     </td>	
	 		     <td width="80" class="rightM_center" style="color:red;border-right:0" align="right">频率:</td>
	 			 <td  class="tdtxt" width="160" style="padding:2 0 0 4;border-right:0">
	 			 <ww:hidden id="bugFreqId" name="dto.bug.bugFreqId"></ww:hidden>
	 			  <ww:textfield id="bugFreqName" cssClass="text_c" name="dto.bug.dtoHelper.bugFreq.typeName" cssStyle="width:160;padding:2 0 0 4;"  readonly='true' onblur="javascript:this.readonly='true';" onfocus="popSelWin('bugFreqId','bugFreqName','freqSelStr','频率')"></ww:textfield>
	 		     </td>	
	 		    <c:if test="${not empty dto.bug.reproPersent}">
	 		    	<c:if test="${dto.bug.bugReptId==session.currentUser.userInfo.id||fn:indexOf(dto.roleInTask,'1')>=0||fn:indexOf(dto.roleInTask,'2')>=0||fn:indexOf(dto.roleInTask,'8')>=0}">
	 		     	 <td width="80" id="repropTd" class="rightM_center" style="color:red;border-right:0" align="right" >再现比例:</td>
		 		      <td  id="reprop"class="tdtxt" align="left"  style="width:140;padding:2 0 0 4;border-right:0">
		 		      <ww:textfield id="reproPersent" cssClass="text_c" name="dto.bug.reproPersent" cssStyle="width:160;padding:2 0 0 4;border-right:0" ></ww:textfield>
				     </td>	
				    </c:if>
	 		    	<c:if test="${dto.bug.bugReptId!=session.currentUser.userInfo.id&&fn:indexOf(dto.roleInTask,'1')<0&&fn:indexOf(dto.roleInTask,'2')<0&&fn:indexOf(dto.roleInTask,'8')<0}">
	 		     	 <td width="80" id="repropTd" class="rightM_center" style="color:red;border-right:0" align="right" >再现比例:</td>
		 		      <td id="reprop"class="tdtxt" align="left"  style="width:140;padding:2 0 0 4;border-right:0">
		 		      <ww:textfield id="reproPersent" cssClass="text_c" name="dto.bug.reproPersent" readonly="true" cssStyle="width:160;padding:2 0 0 4;" ></ww:textfield>
				     </td>	
				    </c:if>
			    </c:if>
			    <c:if test="${empty dto.bug.reproPersent}">
				   <td width="80"  class="rightM_center" style="color:red;border-right:0" align="right">
				     <div id="repropTd" style="display:none">再现比例:</div>
				     </td>
			      <td  class="tdtxt" align="left"  style="width:160;padding:2 0 0 4;border-right:0">
			        <div id="reprop" style="display:none">
			      	 <ww:textfield id="reproPersent" cssClass="text_c" name="dto.bug.reproPersent" cssStyle="width:160;padding:2 0 0 4;" ></ww:textfield>
			        </div>
			      </td> 	    
			    </c:if>	 		     
			   </tr>		   
			  		   
	 		    <c:if test="${dto.bug.devOwnerId!=session.currentUser.userInfo.id}">
	 		    	<c:if test="${not empty dto.bug.genePhaseId}">
	 		    	<tr>
			 		     <td id="impPhaTdtxt" width="80" class="rightM_center"  align="right" style="border-right:0" >引入原因:</td>
			 			 <td id="impPhaTd" class="tdtxt"  width="160" style="padding:2 0 0 4;border-right:0">
			 			 <ww:hidden id="genePhaseId" name="dto.bug.genePhaseId"></ww:hidden>
			 			 <ww:property value="dto.bug.importPhase.typeName"/>
			 		     </td>
			 		     <td colspan="4"></td>
			 		 </tr>	
		 		    </c:if>	
	 		    	<c:if test="${empty dto.bug.genePhaseId}">
	 		    	 <tr style="display:none" id="impPhaTr">
			 		     <td  width="80" class="rightM_center"  align="right" style="border-right:0" >
			 		       <div id="impPhaTdtxt" style="display:none"> 引入原因:</div>
			 		     </td>
			 			 <td  class="tdtxt"   width="160" style="padding:2 0 0 4;border-right:0">
			 			 <ww:hidden id="genePhaseId" name="dto.bug.genePhaseId"></ww:hidden>
			 			 <div id="impPhaTd"" style="display:none">
			 			   <ww:textfield id="genePhaName" cssClass="text_c" name="dto.bug.importPhase.typeName" cssStyle="width:160;padding:2 0 0 4;" readonly="true"></ww:textfield>
			 		     </div>
			 		     </td>
			 		     <td colspan="4"></td>
			 		 </tr>	
		 		    </c:if>	  		     
	 		     </c:if>
	 		     <c:if test="${dto.bug.devOwnerId==session.currentUser.userInfo.id}">
	 		      <tr>	
		 		     <td class="rightM_center" style="border-right:0" id="impPhaTdtxt" width="80"  align="right"  >引入原因:</td>
		 			 <td class="tdtxt" id="impPhaTd" width="160" style="padding:2 0 0 4;border-right:0">
		 			 <ww:hidden id="genePhaseId" name="dto.bug.genePhaseId"></ww:hidden>
		 			 <ww:textfield id="genePhaName" cssClass="text_c" name="dto.bug.importPhase.typeName" cssStyle="width:160;padding:2 0 0 4;"  readonly='true' onblur="javascript:this.readonly='true';" onfocus="popSelWin('genePhaseId','genePhaName','genePhaseSelStr','引入原因')"></ww:textfield>
		 		     </td>
		 		      <td colspan="4"></td>	 
		 		    </tr>		     
	 		     </c:if>
			   <tr>
   			    <td width="80" class="rightM_center" style="color:red;" align="right"style="border-right:0" nowrap>再现步骤:</td>
   			    <td width="640" class="tdtxt" colspan="5" style="background-color: #f7f7f7; hight:200;width:640;border-right:0">
   			      <textarea name="dto.bug.reProStep" id="reProStep" cols="50" rows="30" readonly='true' Style="width:640;hight:200;padding:2 0 0 4;border-right:0">
   			      </textarea>
   			    </td>			   
			   </tr>
			   <tr>
				<td width="100" class="rightM_center" align="right" style="border-right:0">
					更改状态为:
				</td>
				<td width="100" class="tdtxt" align="left" style="border-right:0">
					<ww:select id="stateIdList" name="stateIdList"
						list="dto.stateList" headerKey="-1" headerValue=""
						listKey="keyObj" listValue="valueObj"
						cssStyle="width:100;padding:2 0 0 4;" cssClass="text_c" onchange="trigerCtrl(this.value)"></ww:select>
				</td>
				<c:if test="${not empty dto.stateList}">
				<td  width="80" class="tdtxt" align="left" style="border-right:0">
				  <div id="repeBtn" style="display:none;" ><a class="graybtn" href="javascript: repeatRela();"><span> 关联重复</span></a>
				  </div>
				</td>
					<td  width="160" class="tdtxt" align="left" style="border-right:0">
					<div id="repeDetBtn" style="display:none;">
					  <a class="graybtn" href="javascript: repeDetl();"><span> 所重BUG明细</span></a>
					 </div>
					</td>	
					<td colspan="2" width="320"style="border-right:0">
					</td> 
				</c:if>
				<c:if test="${empty dto.stateList}">
					<td  width="160" class="tdtxt" align="left" style="border-right:0">
					<div id="repeDetBtn" style="display:none;">
					  <a class="graybtn" href="javascript: repeDetl();"><span> 所重BUG明细</span></a>
					 </div>
					</td>	
				<td colspan="3" width="340"style="border-right:0">
					<div >无状态可选，表明只能修改测试人员或开发人员等信息</div>
					<div id="repeBtn" > </div>
				</td>  				
				</c:if>
			   </tr>
			  <tr style="display:none" id="nextOwnTr">
			   <td  class="rightM_center" style="color:red;" id="nextOwnTd1"  align="right" width="80" style="display:none;border-right:0">转:</td>
			   <td  class="tdtxt" id="nextOwnTd2" width="160" style="padding:2 0 0 4;display:none">
			   </td>
			   <td  id="nextOwnTd3"align="left" width="80" style="display:none;color:#055A78;font-size:12px;border-right:0">处理</td>
			   <td colspan="3" id="assFromMdTd"  style="border-right:0"></td>
			  </tr>
			   <tr>
			   <td  class="rightM_center"  align="right" width="80" style="border-right:0">发现日期:</td>
			   <td  class="tdtxt" align="left" width="160" style="width:160;padding:2 0 0 4;border-right:0" >
			   
			   <fmt:formatDate value="${dto.bug.reptDate}" pattern="yyyy-MM-dd HH:mm:ss" />
			   </td>
			   <td  class="rightM_center" align="right"" width="80" style="border-right:0">发现版本:</td>
			   <td  class="tdtxt" align="left" width="160"style="width:160;padding:2 0 0 4;border-right:0"  >
			     <ww:property value="dto.bug.reptVersion.versionNum"/>
			   </td>
			   <td  id="verLableTd" class="rightM_center" style="border-right:0;color:red" align="right" width="80" nowrap>
			   <ww:property value="dto.bug.versionLable"/>
			   </td>
			   <td  id="verValTd" class="tdtxt" align="left" width="160"style="width:160;padding:2 0 0 4;border-right:0">
   				<ww:property value="dto.bug.currVersion.versionNum" />
			   </td>
			   </tr>
			   <c:if test="${not empty dto.bug.currRemark}">
			   <tr id="reamrkTr">
			      <td width="80" class="rightM_center"  align="right" style="border-right:0">备注:</td>
			      <td  class="tdtxt"  align="left" colspan="5"  width="640" style="padding:2 0 0 4;border-right:0">
    			     <ww:textfield id="currRemark" cssClass="text_c" readonly="true" name="dto.bug.currRemark" cssStyle="width:640;padding:2 0 0 4;" ></ww:textfield>
    			  </td>
			    </tr>
			   </c:if>
			   <c:if test="${empty dto.bug.currRemark}">
			   <tr id="reamrkTr" style="display:none;border-right:0">
			      <td width="80" class="rightM_center" align="right">备注:</td>
			      <td  class="tdtxt"  align="left" colspan="5"  width="640" style="padding:2 0 0 4;border-right:0">
    			     <ww:textfield id="currRemark" cssClass="text_c" readonly="true" name="dto.bug.currRemark" cssStyle="width:640;padding:2 0 0 4;" ></ww:textfield>
    			  </td>
			    </tr>
			   </c:if>
			  <tr class="odd_mypm">
			  	 <td class="rightM_center" width="90" id="attachTd" style="border-right:0">
			  	 附件:
			  	 </td>
			    <td class="dataM_left"  colspan="5" width="630" style="border-right:0">
			    	<c:if test="${not empty dto.bug.attachUrl}">
					<img src="<%=request.getContextPath()%>/images/button/attach.gif" id="currAttach" alt="当前附件" title="${dto.bug.attachUrl2}" onclick="openAtta()"/>
			   		</c:if>
			    </td>
			  </tr>	
			   <tr>
				<td  class="tdtxt" align="center" width="720" colspan="6" style="border-right:0">
				    <a class="bluebtn" href="javascript:closeMe();" style="margin-left: 6px"><span> 返回</span></a>
				    <a class="bluebtn" href="javascript:repeatRela('repeFind');" style="margin-left: 6px"><span> 查看有无相同BUG</span></a>
				    <a class="bluebtn" href="javascript:void(0);" onclick="handSub('next')" id="saveConti" style="margin-left: 6px;display:none"><span> 确定并下一个</span></a>
				    <a class="bluebtn" href="javascript:void(0);" onclick="handSub()" style="margin-left: 6px"><span> 确定</span></a>
				    <a class="bluebtn" id="nextOne"href="javascript:parent.handBug('next');" style="margin-left: 6px;display:none"><span> 下一个</span></a>
				 	<a class="bluebtn" id="preOne"href="javascript:parent.handBug('up');" style="display:none"><span> 上一个</span></a>
    			 </td> 	   
			   </tr>
			   <tr>
			    <td colspan="6" style="border-right:0"></td>
			   </tr>
			 </table>
			</ww:form>
	      </div>
	  </div>
	  <div id="flowInfoDiv" align="center"  valign="middle" style="background-color: #ffffff;display:none; width:100%;">
		<table width="100%" cellspacing="0" cellpadding="0">
			<tr>
				<td valign="top">
					<div id="toolbarObj1"></div>
				</td>
			</tr>
			<tr>
				<td valign="top">
					<div id="gridbox1" ></div>
				</td>
			</tr>
		</table>  
	  </div>
	  <ww:include value="/jsp/common/dialog.jsp"></ww:include>
	  <ww:include value="/jsp/common/downLoad.jsp"></ww:include>
	  <ww:include value="/jsp/common/selSingleAllCompanyPerson.jsp"></ww:include>
	<script type="text/javascript" src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/excells/dhtmlxgrid_excell_link.js"></script>	
	<script type="text/javascript">
	adjustTable("baseTable","odd");
	if(parent.getNextRowNum($("bugId").value)>0 ||!parent.isLastPage()){
		$("saveConti").style.display="";
	}
 	if(parent.pmGrid.getRowsNum()>1){
		$("preOne").style.display="";
		$("nextOne").style.display="";	
	}
	if($("withRepteId").value!=""){
		$("repeDetBtn").style.display="";
	}
	if("${dto.bug.devOwnerId}"!=""&&"${dto.testFlow}".indexOf("4")>0&&"${dto.roleInTask}".indexOf("4")<0&&"${dto.roleInTask}".indexOf("7")<0){
		$("devOwnerName").readOnly=true;
		$("devOwnerName").onfocus="";
	}
	pmGrid = new dhtmlXGridObject('gridbox1');
	pmGrid.setImagePath(conextPath+"/dhtmlx/grid/codebase/imgs/");
    pmGrid.setHeader("&nbsp;,序号,处理人,处理结果,处理说明,处理日期");
    pmGrid.setInitWidths("0,40,80,350,*,80");
    pmGrid.setColAlign("center,center,left,left,left,left");
    pmGrid.setColTypes("ra,ro,ro,ro,ro,ro");
    pmGrid.setColSorting("int,str,str,str,str,str");
	pmGrid.enableAutoHeight(true,400);
    pmGrid.init();
    pmGrid.enableTooltips("false,true,true,true,true,true");
    pmGrid.setSkin("light");  
    var testOwnName ="${dto.bug.testOwner.uniqueName}";
    var devOwnName ="${dto.bug.devOwner.uniqueName}";
    var anasName ="${dto.bug.analysOwner.uniqueName}";
    var asinName ="${dto.bug.assinOwner.uniqueName}";
    var inteOwnName ="${dto.bug.intecesOwner.uniqueName}";
	flwBar = new dhtmlXToolbarObject("toolbarObj1");
	flwBar.setIconsPath(conextPath+"/dhtmlx/toolbar/images/");
	flwBar.addButton("first", 1, "", "first.gif", "first.gif");
	flwBar.setItemToolTip("first", "第一页");
	flwBar.addButton("pervious",2, "", "pervious.gif", "pervious.gif");
	flwBar.setItemToolTip("pervious", "上一页");
	flwBar.addSlider("slider",3, 80, 1, 30, 1, "", "", "%v");
	flwBar.setItemToolTip("slider", "滚动条翻页");
	flwBar.addButton("next",4, "", "next.gif", "next.gif");
	flwBar.setItemToolTip("next", "下一页");
	flwBar.addButton("last",5, "", "last.gif", "last.gif");
	flwBar.setItemToolTip("last", "末页");
	flwBar.addInput("page",6, "", 25);
	flwBar.addText("pageMessage",7, "");
	flwBar.addText("pageSizeText",8, "每页");
	flwBar.addText("pageSizeTextEnd",9, "20条");
	if($("impPhaTd").style.display==""){
		parent.detlW_ch.setDimension(850, 550);
	}
	if($("bugAntimodDate")!=null)
		$("bugAntimodDate").value=dueDateInit;
	var myDiscDate = "<fmt:formatDate value="${dto.bug.reptDate}" pattern="yyyy-MM-dd HH:mm:ss" />";
	if($("geneCauseId").value!=""){
		$("geneCauseTd").style.display="";
		$("geneCauseF").style.display="";
	}
	 </script>
	<script type="text/javascript"src="<%=request.getContextPath()%>/jsp/bugManager/flowCtrl.js"></script>
	<script type="text/javascript"src="<%=request.getContextPath()%>/jsp/bugManager/history.js"></script>
   <link rel="STYLESHEET" type="text/css" href="<%=request.getContextPath()%>/dhtmlx/calendar/codebase/dhtmlxcalendar.css">	
   <script type="text/javascript"src="<%=request.getContextPath()%>/dhtmlx/calendar/codebase/dhtmlxcalendar.js"></script>						
	</BODY>
</HTML>
