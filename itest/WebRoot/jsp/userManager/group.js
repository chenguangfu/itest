	pageBreakUrl = conextPath+"/userManager/userManagerAction!groupList.action";
	pmGrid = new dhtmlXGridObject('gridbox');
	pmGrid.setImagePath(conextPath+"/dhtmlx/grid/codebase/imgs/");
    pmGrid.setHeader("&nbsp;,序号,组名,组员,备注,&nbsp;");
    pmGrid.setInitWidths("40,40,160,*,200,0");
    pmGrid.setColAlign("left,left,left,left,left,left");
    pmGrid.setColTypes("ra,ro,ro,ro,ro,ro");
    pmGrid.setColSorting("int,int,str,str,str,str");
	pmGrid.attachEvent("onCheckbox",doOnCheck);
	pmGrid.enableAutoHeight(true, 700);
    pmGrid.init();
    pmGrid.enableRowsHover(true, "red");
    pmGrid.enableTooltips("false,false,true,true,true,true");
    pmGrid.setSkin("light");
   	pmGrid.attachEvent("onRowSelect",doOnSelect);
    if($("listStr").value != ""){
    	var data = $("listStr").value;
    	data = data.replace(/[\r\n]/g, "");
	    var datas = data.split("$");
	    if(datas[1] != ""){
	    	jsons = eval("(" + datas[1] +")");
	    	pmGrid.parse(jsons, "json");
	    	setPageNoSizeCount(datas[0]);
	    	setRowNum();
	    }
    }
    function doOnSelect(rowId,index){
		pmGrid.cells(rowId, 0).setValue(true);
	} 
    function doOnCheck(rowId,cellInd,state){//勾选
		pmGrid.setSelectedRow(rowId);
		return true;
	}
    function setRowNum(){
    	var rowIdsToDel ="";
		for(var i = 0; i < pmGrid.getRowsNum(); i++){	
			pmGrid.cells2(i,1).setValue((i + 1)+((parseInt(pageNo)-1)*parseInt(pageSize)));
			if(i>(pageSize-1)){
				if(rowIdsToDel==""){
					rowIdsToDel = pmGrid.getRowId(i);
				}else{
					rowIdsToDel = rowIdsToDel +"," +pmGrid.getRowId(i);
				}
			}
		}
		if(rowIdsToDel !=""){
			var ids = rowIdsToDel.split(",");
			for(var l=0; l <ids.length; l++){
			 pmGrid.deleteRow(ids[l]);
			}
		}
    }
	pmBar.attachEvent("onValueChange", function(id, pageNo) {
		pageAction(pageNo, pageSize);
	});
	pmBar.attachEvent("onEnter", function(id, value) {
		if(!isDigit(value, false)){
			pmBar.setValue("page", pageNo);
			return;
		}
		var pageNoTemp = parseInt(value);
		if(pageNoTemp < 1){
			pageNoTemp = 1;
		}else if(pageNoTemp > pageCount){
			pageNoTemp = pageCount;
		}
		pageAction(pageNoTemp, pageSize);
	});
	pmBar.attachEvent("onClick", function(id) {
		if(id == "find"){
			find("findD");
		}else if(id == "new"){
			creater(id, "createD");
		}else if(id == 'update'){
			if(pmGrid.getSelectedId()== null){
				hintMsg("请选择要修改的记录");
			}else{
				creater(id, "createD");
			}
		}else if(id == 'delete'){
			if(pmGrid.getSelectedId()== null){
				hintMsg("请选择要删除的记录");
			}else{
				cfDialog("deleteExe","您确定删除选择的记录?",false);
			}
		}else if(id == "first"){
			pageAction(1, pageSize);
		}else if(id == "last"){
			pageAction(pageCount, pageSize);
		}else if(id == "next"){
			pageAction(pageNo + 1, pageSize);
		}else if(id == "pervious"){
			pageAction(pageNo -1, pageSize);
		}else if(id == "hidden"){
			hideCol();
		}else if(id == "id1" || id == "id2" || id == "id3" || id == "id4" || id == "id5"){
			var pageSizeTemp = parseInt(pmBar.getListOptionText("pageP", id));
			if(pageSize==pageSizeTemp){
				return;
			}
			pageSize = pageSizeTemp;
			pageAction(pageNo, pageSize);
		}
	});
	function deleteExe(){
		var url = conextPath+"/userManager/userManagerAction!delGroup.action?dto.group.id=";
		url += pmGrid.getSelectedId()+"&dto.isAjax=true";
		var ajaxRest = dhtmlxAjax.postSync(url, "findF").xmlDoc.responseText;
		if(ajaxRest=="success"){
			pmGrid.deleteRow(pmGrid.getSelectedId());
			clsoseCfWin();
			return;
		}
		hintMsg("删除时发生错误");
	}
	function pageAction(pageNo, pageSize){
		if(pageNo>pageCount && pageSizec<1){
			pmBar.setValue("page", pageNo);
			return ;
		}
		var url = pageBreakUrl + "?dto.isAjax=true&dto.pageNo="+ pageNo +"&dto.pageSize=" + pageSize;
		var userJson = dhtmlxAjax.postSync(url, "findF").xmlDoc.responseText.split("$");
		if(userJson[1] == ""){
			hintMsg("没查到相关记录");
		}else{
	   		pmGrid.clearAll();
	   		pmGrid.parse(eval("(" + userJson[1] +")"), "json");
	   		setPageNoSizeCount(userJson[0]);
	   		setRowNum();
   		}
   		pageSizec = 0;
	}
	var subUrl ="";
	function creater(id, divId){
		cuW_ch = initW_ch(cuW_ch, divId, true, 470, 150);
		formReset("createF", "name");
		$("userIds").value="";
		$("userIds").valaue="";
		$("groupId").valaue="";
		$("insertDate").valaue="";
		$("userIds").value="";
		$("groupId").value="";
		$("cUMTxt").innerHTML = "&nbsp;";
		if(id=="new"){		
			cuW_ch.setText("新建组");
			subUrl=conextPath+"/userManager/userManagerAction!addGroup.action?dto.isAjax=true";
			$("creat2_b").style.display="";
		}else if(id=="update"){
			cuW_ch.setText("修改组");
			$("creat2_b").style.display="none";
			var url = conextPath+"/userManager/userManagerAction!groupUpInit.action?dto.group.id=";
			url += pmGrid.getSelectedId()+"&dto.isAjax=true";
			var groupInfo  = dhtmlxAjax.postSync(url, "").xmlDoc.responseText;
			subUrl=conextPath+"/userManager/userManagerAction!updGroup.action?dto.isAjax=true";
			if(groupInfo=="failed"){
				hintMsg("初始化数据发生错误");
			}else{
				setUpInfo(groupInfo);
			}
		}
	}
	function setUpInfo(updInfo){
		var updInfos = updInfo.split("^");
		for(var i=0; i <updInfos.length; i++){
			var currInfo = updInfos[i].split("=");
			if(currInfo[1] !="null"){
				var valueStr ="$('"+currInfo[0]+"').value =currInfo[1]";
				eval(valueStr);	
			}
		}
	}
	function addUpSub(contFlg){
		if(isWhitespace($("name").value)){
			hintMsg("请填写组名");
			return;
		}
		var ajaxRest = dhtmlxAjax.postSync(subUrl, "createF").xmlDoc.responseText;
		if(ajaxRest=="failed"){
			hintMsg("保存数据时发生错误");
			return;
		}else if(ajaxRest.indexOf("success")==0){
			var rowData = "0,,"+$("name").value+","+$("userName").value+","+$("remark").value+","+$("userIds").value;
			if($("groupId").value==""){
				pmGrid.addRow2(ajaxRest.split("^")[1],rowData,0);
			}else{
				pmGrid.deleteRow(ajaxRest.split("^")[1]);
				pmGrid.addRow(ajaxRest.split("^")[1],rowData,0);
			}
			setRowNum();
			if(typeof contFlg !="undefined"){
				$("cUMTxt").innerHTML = "操作成功请继续";
				formReset("createF", "name");
				$("userIds").value="";
				$("userIds").valaue="";
				$("groupId").valaue="";
				$("insertDate").valaue="";
				return;
			}
			cuW_ch.setModal(false);
			cuW_ch.hide();
		}else if(ajaxRest=="reName"){
			$("cUMTxt").innerHTML = "组重名";
			return;
		}
		
	}
	String.prototype.replaceAll  = function(s1,s2){   
    	return this.replace(new RegExp(s1,"g"),s2);
    }   
	function find(divId){
		fW_ch = initW_ch(fW_ch, divId, false, 460, 130);
		fW_ch.setText("查询");
		$("nameF").focus();
	}	
	function efind(){
		var ajaxRest = dhtmlxAjax.postSync(conextPath+"/userManager/userManagerAction!groupList.action?dto.isAjax=true", "findF").xmlDoc.responseText;
	   	if(ajaxRest=="failed"){
	   		hintMsg("查询时发生错误");
	   		return;
	   	}
	   	var datas = ajaxRest.split("$");
	    if(datas[1] != ""){
	    	pmGrid.clearAll();
	    	jsons = eval("(" + datas[1] +")");
	    	pmGrid.parse(jsons, "json");
	    	setRowNum();
	    	setPageNoSizeCount(datas[0]);
	    	fW_ch.setModal(false);
	    	fW_ch.hide();
	    }else{
	    	pmGrid.clearAll();
	    	hintMsg("没查到相关记录");
	    }
	}
	function initW_ch(obj, divId, mode, w, h){//初始化 window
		importWinJs();
		if((typeof obj != "undefined") && !obj.isHidden()){
			obj.setDimension(w,h);
			return obj;
		}else if((typeof obj != "undefined") && obj.isHidden()){
			obj.setDimension(w,h);
			obj.show();
		}else{
			if(typeof cufmsW == "undefined"){
				cufmsW = initCufmsW();
			}
			obj = cufmsW.createWindow(divId, 110, 110, w, h);
			if(divId != null)
				obj.attachObject(divId, false);
			hiddenB(obj, mode);
		}
		obj.setModal(mode);
		return obj;
	}	
	function initCufmsW(){//初始化window
		cufmsW = new dhtmlXWindows();
		cufmsW.enableAutoViewport(true);
		cufmsW.setViewport(50, 50, 100, 100);
		cufmsW.vp.style.border = "#909090 1px solid";
		cufmsW.setImagePath(conextPath+"/dhtmlx/windows/codebase/imgs/");
		return cufmsW;
	}