<%@page contentType="text/html; charset=UTF-8"%>
<%@taglib uri="/webwork" prefix="ww"%>
<%@taglib uri="/WEB-INF/pmButton.tld" prefix="pmTag"%>
<HTML>
	<HEAD>
	<script type="text/javascript">
		popContextMenuFlg=0;
		var custContextMenuFlg=0;
	</script>
		<TITLE>个人信息维护</TITLE>
		<META content="text/html; charset=UTF-8" http-equiv=Content-Type>
		<link rel="STYLESHEET" type="text/css" href="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgrid.css">
		<link rel="STYLESHEET" type="text/css" href="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgrid_skins.css">
		<link rel="STYLESHEET" type="text/css"href="<%=request.getContextPath()%>/dhtmlx/toolbar/codebase/skins/dhtmlxtoolbar_dhx_blue.css">
		<link rel='STYLESHEET' type='text/css' href='<%=request.getContextPath()%>/css/page2.css'>
		<script type="text/javascript" src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxcommon.js"></script>
		<script type="text/javascript" src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgrid.js"></script>
		<script type="text/javascript" src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgridcell.js"></script>
		<script type="text/javascript" src="<%=request.getContextPath()%>/js/globalVariable.js"></script>
		<script type="text/javascript" src="<%=request.getContextPath()%>/js/commonFunction_top.js"></script>
	</HEAD>
	<BODY  style="overflow-y:hidden;bgcolor:white">
	<script>
    var myId="${session.currentUser.userInfo.id}";	
    popContextMenuFlg=0;
  function PasswordStrength(showed){
	  this.showed = (typeof(showed) == "boolean")?showed:true;
	  this.styles = new Array();
	  this.styles[0] = {backgroundColor:"#EBEBEB",borderLeft:"solid 1px #FFFFFF",borderRight:"solid 1px #BEBEBE",borderBottom:"solid 1px #BEBEBE"};
	  this.styles[1] = {backgroundColor:"#FF4545",borderLeft:"solid 1px #FFFFFF",borderRight:"solid 1px #BB2B2B",borderBottom:"solid 1px #BB2B2B"};
	  this.styles[2] = {backgroundColor:"#FFD35E",borderLeft:"solid 1px #FFFFFF",borderRight:"solid 1px #E9AE10",borderBottom:"solid 1px #E9AE10"};
	  this.styles[3] = {backgroundColor:"#95EB81",borderLeft:"solid 1px #FFFFFF",borderRight:"solid 1px #3BBC1B",borderBottom:"solid 1px #3BBC1B"};
	  this.labels= ["弱","中","强"];
	  this.divName = "pwd_div_"+Math.ceil(Math.random()*100000);
	  this.minLen = 5;
	  this.width = "122px";
	  this.height = "16px";
	  this.content = "";
	  this.selectedIndex = 0;
	  this.init();
  }
  PasswordStrength.prototype.init = function(){
	  var s = '<table cellpadding="0" id="'+this.divName+'_table" cellspacing="0" style="width:'+this.width+';height:'+this.height+';">';
	  s += '<tr>';
	  for(var i=0;i<3;i++){
	 		s += '<td id="'+this.divName+'_td_'+i+'" width="33%" align="center"><span style="font-size:1px"> </span><span id="'+this.divName+'_label_'+i+'" style="display:none;font-family: Courier New, Courier, mono;font-size: 12px;color: #000000;">'+this.labels[i]+'</span></td>';
	  }
	  s += '</tr>';
	  s += '</table>';
	  this.content = s;
	  if(this.showed){
	  	document.write(s);
	 		this.copyToStyle(this.selectedIndex);
	  }
  }
  PasswordStrength.prototype.copyToObject = function(o1,o2){
	  for(var i in o1){
	  	o2[i] = o1[i];
	  }
  }
  PasswordStrength.prototype.copyToStyle = function(id){
	  this.selectedIndex = id;
	  for(var i=0;i<3;i++){
		  if(i == id-1){
		  	this.$(this.divName+"_label_"+i).style.display = "inline";
		  }else{
		  	this.$(this.divName+"_label_"+i).style.display = "none";
		  }
	  }
	  for(var i=0;i<id;i++){
	  	this.copyToObject(this.styles[id],this.$(this.divName+"_td_"+i).style);
	  }
	  for(;i<3;i++){
	  	this.copyToObject(this.styles[0],this.$(this.divName+"_td_"+i).style);
	  }
  }
  PasswordStrength.prototype.$ = function(s){
  	return document.getElementById(s);
  }
  PasswordStrength.prototype.setSize = function(w,h){
  	this.width = w;
  	this.height = h;
  }
  PasswordStrength.prototype.setMinLength = function(n){
	  if(isNaN(n)){
	  	return ;
	  }
	  n = Number(n);
	  if(n>1){
	  	this.minLength = n;
	  }
  }
  PasswordStrength.prototype.setStyles = function(){
	  if(arguments.length == 0){
	  	return ;
	  }
	  for(var i=0;i<arguments.length && i < 4;i++){
	  	this.styles[i] = arguments[i];
	  }
	  this.copyToStyle(this.selectedIndex);
  }
  PasswordStrength.prototype.write = function(s){
	  if(this.showed){
	  	return ;
	  }
	  var n = (s == 'string') ? this.$(s) : s;
	  if(typeof(n) != "object"){
	  	return ;
	  }
	  n.innerHTML = this.content;
	  this.copyToStyle(this.selectedIndex);
  }
  PasswordStrength.prototype.update = function(s){
	  if(s.length < this.minLen){
	  	this.copyToStyle(0);
	  	return;
	  }
	  var ls = -1;
	  if (s.match(/[a-z]/ig)){
	  	ls++;
	  }
	  if (s.match(/[0-9]/ig)){
	  	ls++;
	  }
	  if (s.match(/(.[^a-z0-9])/ig)){
	  	ls++;
	  }
	  if (s.length < 6 && ls > 0){
	  	ls--;
	  }
	  switch(ls) {
		  case 0:
		  this.copyToStyle(1);
		  break;
		  case 1:
		  this.copyToStyle(2);
		  break;
		  case 2:
		  this.copyToStyle(3);
		  break;
		  default:
		  this.copyToStyle(0);
	  }
  }
  var loginNameInit ="${dto.user.loginName}";
</script>
		<div id="createDiv" class="cycleTask gridbox_light" style="border:0px;">
		  <div class="objbox" style="overflow:auto;width:100%;">
		    <ww:hidden id="oldPwd" name="dto.user.oldPwd"></ww:hidden>
			<ww:form theme="simple" method="post" id="createForm" name="createForm" namespace="/userManager" action="">
				<ww:hidden id="userId" name="dto.user.id"></ww:hidden>
				<ww:hidden id="isAdmin" name="dto.user.isAdmin"></ww:hidden>
				<ww:hidden id="status" name="dto.user.status"></ww:hidden>
				<ww:hidden id="delFlag" name="dto.user.delFlag"></ww:hidden>
				<ww:hidden id="insertDate" name="dto.user.insertDate"></ww:hidden>
				<ww:hidden id="chgPwdFlg" name="dto.user.chgPwdFlg"></ww:hidden>
				<table class="obj row20px" cellspacing="0" cellpadding="0" width="100%" id="createTab">
					<tr>
						<td colspan="4"  style="border-right:0;width:340">
							<div id="cUMTxt" align="center" style="color: Blue; padding: 2px">
								&nbsp;
							</div>
						</td>
					</tr>
					<tr>
						<td align="right" class="rightM_center" style="color:red;border-right:0"nowrap>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;登录帐号:
						</td>
						<td colspan="3" class="dataM_left" style="border-right:0">
							<ww:textfield id="loginName" name="dto.user.loginName" cssClass="text_c"  cssStyle="width:340;padding:2 0 0 4;"
								 maxlength="16" onblur="loginNameChk()"></ww:textfield>
						</td>
					</tr>
					<tr id="inVoldPwdR" style="display:none">
						<td align="right" class="rightM_center" style="color:red;border-right:0">
								原密码:
						</td>
						<td colspan="3" class="dataM_left" style="border-right:0;width:340" >
							<ww:password id="voldPwd" name="dto.user.oldPwd" cssClass="text_c" cssStyle="width:120;padding:2 0 0 4;"
								onblur="javascript:if(!isWhitespace(this.value)&&!_isIE){$('cUMTxt').innerHTML='&nbsp;'};"></ww:password>
						</td>
					</tr>
					<tr class="rightM_center" style="color:red;border-right:0">
						<td align="right" class="rightM_center" style="border-right:0">
								密码强度:
						</td>
						<td colspan="3" align="left" style="border-right:0;" >
							 <script language="javascript">
								var ps = new PasswordStrength();
								ps.setSize("180","20");
								ps.setMinLength(6);
							 </script>
						</td>										
					</tr>
					<tr>
						<td align="right" class="rightM_center" style="color:red;border-right:0">
								登录密码:
						</td>
						<td style="border-right:0" class="dataM_left" >
							<ww:password id="password" name="dto.user.password" cssClass="text_c" cssStyle="width:120;padding:0 0 0 4;" onblur="chgPwd()"
								onfocus="javascript:if(!isWhitespace(this.value)&&!_isIE){$('cUMTxt').innerHTML='&nbsp;'};" onkeyup="ps.update(this.value);" maxlength="20"></ww:password>
						</td>
						<td align="right" class="rightM_center" style="color:red;border-right:0" nowrap>
								确认密码:
						</td>
						<td style="border-right:0" class="dataM_left">
							<ww:password id="vpassword" name="dto.vpassword" cssClass="text_c" cssStyle="width:120;padding:0 0 0 4;"
								onblur="javascript:if(!isWhitespace(this.value)&&!_isIE){$('cUMTxt').innerHTML='&nbsp;'};"></ww:password>
						</td>
					</tr>
					<tr>
						<td align="right" class="rightM_center" style="color:red;border-right:0" >
								真实姓名:
						</td>
						<td style="border-right:0" class="dataM_left">
							<ww:textfield id="name" name="dto.user.name" cssClass="text_c" cssStyle="width:120;padding:2 0 0 4;"
								onblur="javascript:if(!isWhitespace(this.value)&&!_isIE){$('cUMTxt').innerHTML='&nbsp;'};" maxlength="16"></ww:textfield>
						</td>
						<td align="right"  class="rightM_center" style="color:red;border-right:0" >
								电子信箱:
						</td>
						<td style="border-right:0" class="dataM_left">
							<ww:textfield id="email" name="dto.user.email" cssClass="text_c" cssStyle="width:120;padding:2 0 0 4;"
								onblur="mailChk()" maxlength="32"></ww:textfield>
						</td>
					</tr>
					<tr>
						<td align="right" class="rightM_center" style="border-right:0">
							员工编号:
						</td>
						<td style="border-right:0" class="dataM_left">
							<ww:textfield id="employeeId" name="dto.user.employeeId" cssClass="text_c" cssStyle="width:120;;padding:2 0 0 4;" maxlength="16"></ww:textfield>
						</td>
						<td align="right" class="rightM_center" style="border-right:0">
							联系电话:
						</td>
						<td style="border-right:0" class="dataM_left">
							<ww:textfield id="tel" name="dto.user.tel" cssClass="text_c" cssStyle="width:120;padding:2 0 0 4;" maxlength="20"></ww:textfield>
						</td>
					</tr>
					<tr>
						<td align="right" class="rightM_center" style="border-right:0">
							办公电话:
						</td>
						<td style="border-right:0" class="dataM_left">
							<ww:textfield id="officeTel" name="dto.user.officeTel" cssClass="text_c" cssStyle="width:120;padding:2 0 0 4;" maxlength="20"></ww:textfield>
						</td>
						<td align="right"class="rightM_center"  style="border-right:0">
							职务:
						</td>
						<td style="border-right:0" class="dataM_left">
							<ww:textfield id="headShip" name="dto.user.headShip" cssClass="text_c" cssStyle="width:120;padding:2 0 0 4;" maxlength="20"
								onblur="javascript:if(!isWhitespace(this.value)&&!_isIE){$('cUMTxt').innerHTML='&nbsp;'};"></ww:textfield>
						</td>
					</tr>
					<tr>
						<td align="right" class="rightM_center" style="border-right:0">
							所属组:
						</td>
						<td colspan="3" style="border-right:0" class="dataM_left">
							<input type="text" id="groupNames" name="dto.user.groupNames"
								disabled="true"  Class="text_c" style="width:340;padding:2 0 0 4;"></input>
						</td>
						<ww:hidden name="dto.user.groupIds" id="groupIds" />
						
					</tr>
					<tr id="questionR"  class="rightM_center" style="display:none">
						<td align="right" class="rightM_center" style="border-right:0">
							找回密码问题:
						</td>
						<td colspan="3" style="border-right:0" class="dataM_left">
							<ww:textfield id="question" name="dto.user.question" cssClass="text_c" cssStyle="width:340;padding:2 0 0 4;" maxlength="20"></ww:textfield>
						</td>
					</tr>
					<tr id="answerR" style="display:none">
						<td  class="rightM_center" style="border-right:0" >
							找回密码答案:
						</td>
						<td colspan="3"class="dataM_left" style="border-right:0">
							<ww:textarea id="answer" name="dto.user.answer" cssClass="text_c" cssStyle="width:340;padding:2 0 0 4;" rows="2"></ww:textarea>
						</td>
					</tr>
					<tr>
						<td colspan="4" style="border-right:0">
							&nbsp;
						</td>
					</tr>
					<tr>
						<td colspan="4"  class="dataM_center" align="center" style="border-right:0">
							<a class="bluebtn" href="javascript:void(0);" id="saveBtn"onclick="javascript:parent.myInfoW_ch.setModal(false);parent.myInfoW_ch.hide();"
							style="margin-left: 6px;"><span> 返回</span> </a>
							<a class="bluebtn" href="javascript:void(0);" id="cuU2_b"onclick=""
								style="margin-left: 6px;display:none"><span> 确定并继续</span> </a>
							<a class="bluebtn" href="javascript:void(0);" id="cuU_b"onclick="subMyInfo();"
								style="margin-left: 6px;"><span> 确定</span> </a>
							<a class="bluebtn" href="javascript:void(0);" id="saveBtn"onclick="formReset('createForm', 'loginName');$('voldPwd').value='4567KK6slygjk';$('password').value='4567KK6slygjk';$('vpassword').value='4567KK6slygjk';"
							style="margin-left: 6px;"><span> 重置</span> </a>
						</td>
					</tr>
				</table>
			</ww:form>
		</div>
		</div>
        <ww:include value="/jsp/common/dialog.jsp"></ww:include>
	</BODY>
	<script type="text/javascript" src="<%=request.getContextPath()%>/jsp/userManager/upMyInfo.js"></script>
	<script type="text/javascript" src="<%=request.getContextPath()%>/js/commonFunction_botm.js"></script>
	<link rel="STYLESHEET" type="text/css" href="<%=request.getContextPath()%>/dhtmlx/windows/codebase/dhtmlxwindows.css">
	<link rel="STYLESHEET" type="text/css" href="<%=request.getContextPath()%>/dhtmlx/windows/codebase/skins/dhtmlxwindows_dhx_blue.css">
	<script type="text/javascript" src="<%=request.getContextPath()%>/dhtmlx/windows/codebase/dhtmlxwindows.js"></script>
    <script type="text/javascript">
    	addUpInitCB('update');
    </script>
</HTML>
