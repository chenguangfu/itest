	var oEditor ; 
	function viewDetl(){
		var url =conextPath+"/msgManager/commonMsgAction!viewDetal.action?dto.isAjax=true&dto.broMsg.logicId="+pmGrid.getSelectedId();
		url+="&dto.isView=1";
		var ajaxRest = postSub(url,"");
		if(ajaxRest=="failed"){
			hintMsg("加载数据发生错误");
			return;
		}else if(ajaxRest.indexOf("failed^")>=0){
			hintMsg(ajaxRest.split("^")[1]);
			return;			
		}
		$("currAttach").style.display="none";
		setUpInfo(ajaxRest);
		cW_ch=initW_ch(cW_ch,  "createDiv", true,660, 440);
		loadFCK();
		cW_ch.setText("明细");
		adjustTable('baseInfoTab');
		cW_ch.bringToTop();
		cW_ch.setModal(true);
		$("msgTitle").focus();
	}
	
	function findExe(){
		var url =conextPath+"/msgManager/commonMsgAction!loadRecevMsg.action?dto.isAjax=true&dto.pageSize=" + pageSize;
		var ajaxRest = postSub(url,"findForm");
		if(ajaxRest=="failed"){
			hintMsg("加载数据发生错误");
			return;
		}
		var userJson = ajaxRest.split("$");
		if(userJson[1] == ""){
			hintMsg("没查到相关记录");
			return;
		}else{
			$("listStr").value=ajaxRest;
			colTypeReset();
			initGrid(pmGrid,"listStr");	
	   		loadLink();
	   		fW_ch.setModal(false);
	   		fW_ch.hide();
   		}
	}

	function findInit(){
		fW_ch = initW_ch(fW_ch,  "findDiv", true,470, 140);
		fW_ch.bringToTop();
		fW_ch.setModal(true);
		fW_ch.setText("查询");
		$("msgTitleF").focus();
	}

	function setUpInfo(updInfo){
		var updInfos = updInfo.split("^");
		for(var i=0; i < updInfos.length; i++){
			var currInfo = updInfos[i].split("=");
			if(currInfo[1] != "null"){
				var valueStr = "";
				currInfo[1] =recovJsonKeyWord(currInfo[1]);
				valueStr = "$('"+currInfo[0]+"').value = currInfo[1]";
				eval(valueStr);
				if(currInfo[0]=="content"){
					$("initContent").value=currInfo[1];
				}else if(currInfo[0]=="mailFlg"&&currInfo[1]=="true"){
					$("mailFlg").checked=true;
				}else if(currInfo[0]=="mailFlg"&&currInfo[1]!="true"){
					$("mailFlg").checked=false;
				}else if(currInfo[0]=="msgType"&&currInfo[1]=="1"){
					$("recpiUserIdTr").style.display="";
				}else if(currInfo[0]=="msgType"&&currInfo[1]!="1"){
					$("recpiUserIdTr").style.display="none";
				}else if(currInfo[0]=="attachUrl"&&currInfo[1]!=""){
					$("currAttach").style.display="";
					$("currAttach").title="附件";
					//$("currAttach").title=currInfo[1].substring(currInfo[1].indexOf("_")+1);
				}
			}
		}
	}
	function loadFCK(){
		if(typeof oEditor != "undefined"){
			if($("logicId").value ==""){
				oEditor.SetData("") ;
				return;
			}else{
				oEditor.SetData($("initContent").value) ;
				return;
			}
		}
		importFckJs();
    	var pmEditor = new FCKeditor('content') ;
    	pmEditor.BasePath = conextPath+"/pmEditor/" ;
    	pmEditor.Height = 250;
    	pmEditor.ToolbarSet = "Basic" ; 
    	pmEditor.ToolbarStartExpanded=false;
    	pmEditor.ReplaceTextarea();
	}
	function FCKeditor_OnComplete( editorInstance ){
		oEditor = FCKeditorAPI.GetInstance('content') ;
		if($("logicId").value == ""){
			oEditor.SetData("") ;
			return;
		}else{
			oEditor.SetData($("initContent").value) ;
			return;
		}
	}

	pmBar.attachEvent("onValueChange", function(id, pageNo) {
		pageAction(pageNo, pageSize);
		return;
	});
	pmBar.attachEvent("onEnter", function(id, value) {
		if(!isDigit(value, false)){
			pmBar.setValue("page", pageNo);
			return;
		}
		var pageNoTemp = parseInt(value);
		if(pageNoTemp < 1){
			pageNoTemp = 1;
		}else if(pageNoTemp > pageCount){
			pageNoTemp = pageCount;
		}
		pageAction(pageNoTemp, pageSize);
		return;
	});
	pmBar.attachEvent("onClick", function(id) {
			if(id == "first"){
			pageNo = 1;
			pageAction(pageNo, pageSize);
		}else if(id == "last"){
			pageNo = pageCount;
			pageAction(pageNo, pageSize);
		}else if(id == "next"){
			pageNo = pageNo +1
			pageAction(pageNo, pageSize);
		}else if(id == "pervious"){
			pageNo = pageNo -1
			pageAction(pageNo, pageSize);
		}else if(id == "id1" || id == "id2" || id == "id3" || id == "id4" || id == "id5"){
			var pageSizeTemp = parseInt(pmBar.getListOptionText("pageP", id));
			if(pageSize==pageSizeTemp){
				return;
			}
			pageSize = pageSizeTemp;
			pageAction(pageNo, pageSize);
		}
	});
	pmBar.attachEvent("onClick", function(id) {
		if(id == "find"){
			findInit();
		}else if(id == "new"){
			addUpInit("add");
		}else if(id == 'update'){
			if(pmGrid.getSelectedId()==null){
				hintMsg("请选择要修改的记录");
				return;
			}
			addUpInit("update");
		}else if(id == 'delete'){
			if(pmGrid.getSelectedId()==null){
				hintMsg("请选择要删除的记录");
				return;
			}
			cfDialog("delExe","您确定删除选择的记录?",false);
		}
	});
	function pageAction(pageNo, pSize){
		if(pageNo>pageCount&&opreType !="repeFind"){
			pmBar.setValue("page", pageNo);
			return ;
		}
		var url =conextPath+"/msgManager/commonMsgAction!loadRecevMsg.action?dto.isAjax=true&dto.pageNo="+ pageNo +"&dto.pageSize=" + pageSize;
		var ajaxRest = postSub(url,"findForm");
		if(ajaxRest=="failed"){
			hintMsg("加载数据发生错误");
			return;
		}
		var userJson = ajaxRest.split("$");
		if(userJson[1] == ""){
			hintMsg("没查到相关记录");
			return;
		}else{
			$("listStr").value=ajaxRest;
			colTypeReset();
			initGrid(pmGrid,"listStr");	
	   		loadLink();
   		}
   		return;
	}
	function initCufmsW(){
		cufmsW = new dhtmlXWindows();
		cufmsW.enableAutoViewport(true);
		cufmsW.setViewport(50, 50, 100, 100);
		cufmsW.setImagePath(conextPath+"/dhtmlx/windows/codebase/imgs/");
		return cufmsW;
	}
	function initW_ch(obj, divId, mode, w, h,wId){
		importWinJs();
		if((typeof obj != "undefined")&& !obj.isHidden()){
			obj.setDimension(w, h);
			return obj;
		}else if((typeof obj != "undefined") && obj.isHidden()){
			obj.show();
			obj.bringToTop();
		}else{
			if(typeof cufmsW == "undefined"){
				cufmsW = initCufmsW();
			}
			if(typeof wId != "undefined" && wId!=""){
				obj = cufmsW.createWindow(wId, 110, 110, w, h);
			}else{
				obj = cufmsW.createWindow(divId, 110, 110, w, h);
			}
			if(divId != null&&divId !=""){
				obj.attachObject(divId, false);
			}
			hiddenB(obj, mode);
		}
		obj.setModal(mode);
		return obj;
	}


	function openAtta(fileName){
		if(typeof fileName!="undefined"){
			$("downloadFileName").value=fileName;
		}else if($("attachUrl").value!=""){
			$("downloadFileName").value=$("attachUrl").value;
		}else{
			return;
		}
		$("downForm").action=conextPath+"/fileUpload?cmd=download";
		$("downForm").submit();
	}
