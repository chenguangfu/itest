package cn.com.mypm.analysisManager.dto;

import cn.com.mypm.framework.transmission.dto.BaseDto;

public class AnalysisDto extends BaseDto {
	private static final long serialVersionUID = 1L;
	private String blhPar;//&analysisDto.blhPar=
	private String parameter;
	private String rptdesign;
	private Integer status;
	private String projectId;
	private String treeStr;
	private String projectCost;
	private String ids;
	private String canView ;
	
	public String getBlhPar() {
		return blhPar;
	}

	public void setBlhPar(String blhPar) {
		this.blhPar = blhPar;
	}

	public String getParameter() {
		return parameter;
	}

	public void setParameter(String parameter) {
		this.parameter = parameter;
	}

	public String getRptdesign() {
		return rptdesign;
	}

	public void setRptdesign(String rptdesign) {
		this.rptdesign = rptdesign;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getProjectId() {
		return projectId;
	}

	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	public String getTreeStr() {
		return treeStr;
	}

	public void setTreeStr(String treeStr) {
		this.treeStr = treeStr;
	}

	public String getProjectCost() {
		return projectCost;
	}

	public void setProjectCost(String projectCost) {
		this.projectCost = projectCost;
	}

	public String getIds() {
		return ids;
	}

	public void setIds(String ids) {
		this.ids = ids;
	}

	public String getCanView() {
		return canView;
	}

	public void setCanView(String canView) {
		this.canView = canView;
	}
}
