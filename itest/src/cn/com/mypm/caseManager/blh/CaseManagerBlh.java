package cn.com.mypm.caseManager.blh;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import cn.com.mypm.bugManager.dto.BoardVo;
import cn.com.mypm.caseManager.dto.CaseManagerDto;
import cn.com.mypm.caseManager.dto.TestCaseVo;
import cn.com.mypm.caseManager.service.CaseManagerService;
import cn.com.mypm.common.SecurityContextHolderHelp;
import cn.com.mypm.common.service.DrawHtmlListDateService;
import cn.com.mypm.framework.app.blh.BusinessBlh;
import cn.com.mypm.framework.app.view.View;
import cn.com.mypm.framework.common.HtmlListComponent;
import cn.com.mypm.framework.common.ListObject;
import cn.com.mypm.framework.exception.DataBaseException;
import cn.com.mypm.framework.security.filter.SecurityContextHolder;
import cn.com.mypm.framework.transmission.JsonInterface;
import cn.com.mypm.framework.transmission.events.BusiRequestEvent;
import cn.com.mypm.impExpManager.service.ImpExpManagerService;
import cn.com.mypm.object.CaseExeHistory;
import cn.com.mypm.object.OutlineInfo;
import cn.com.mypm.object.SoftwareVersion;
import cn.com.mypm.object.TestCaseInfo;
import cn.com.mypm.object.TestResult;
import cn.com.mypm.object.TestTaskDetail;
import cn.com.mypm.object.TypeDefine;
import cn.com.mypm.object.User;
import cn.com.mypm.outlineManager.service.OutLineManagerService;
import cn.com.mypm.testTaskManager.service.TestTaskDetailService;

public class CaseManagerBlh extends BusinessBlh {

	private CaseManagerService caseService;
	private TestTaskDetailService testTaskService ;
	private OutLineManagerService outLineService;
	private DrawHtmlListDateService drawHtmlListDateService;
	private ImpExpManagerService impExpManagerService;
	
	public View pasteCase(BusiRequestEvent req){
		CaseManagerDto dto = super.getDto(CaseManagerDto.class, req);
		String command = dto.getCommand();
		if(!"cp".equals(command)&&!"ct".equals(command)){
			return super.globalAjax();
		}
		String remark = dto.getRemark();
		if(remark==null||"".equals(remark.trim())){
			super.writeResult("noCaseSel");
			return super.globalAjax();
		}
		String taskId = SecurityContextHolderHelp.getCurrTaksId();
		String hql = "select new OutlineInfo(moduleNum,moduleId) from OutlineInfo where taskId=? and moduleId = ? and moduleState!=1 ";
		List<OutlineInfo> nodeList = outLineService.findByHql(hql, taskId,dto.getCurrNodeId());
		OutlineInfo currNode = nodeList.get(0);
		if(nodeList==null||nodeList.isEmpty()){
			super.writeResult("nodeHaveDel");
			return super.globalAjax();
		}
		
		String[] idStrArr = remark.split("_");
		List<Long> idList = new ArrayList<Long>();
		for(String id :idStrArr){
			if(!"".equals(id)&&id!=null){
				idList.add(new Long(id));
			}
		}
		//hql = "select new TestCaseInfo(testCaseId, testCaseDes,weight, attachUrl, testData, expResult, operDataRichText,caseTypeId, priId) from TestCaseInfo where taskId=:taskId and testCaseId in(:ids) ";
		hql = " from TestCaseInfo where taskId=:taskId and testCaseId in(:ids) ";
		Map praValuesMap = new HashMap(2);
		praValuesMap.put("taskId", taskId);
		praValuesMap.put("ids", idList);
		List<TestCaseInfo> initCaseList = caseService.findByHqlWithValuesMap(hql, praValuesMap, false);
		if(initCaseList==null ||initCaseList.isEmpty()){
			super.writeResult("selCaseDel");
			return super.globalAjax();			
		}
		this.copyCasePrepare(dto, initCaseList, currNode);
		if("cp".equals(command)){
			caseService.copyCase(dto);
			dto.setAttr("delFlg", "cp");
		}else{
			this.setCanDelCaseId(dto, idList);
			caseService.pasteCase(dto);
		}
		initCaseList = null;
		
		idList = null;
		dto.setTaskId(taskId);
		List caseList = caseService.loadCase(dto,currNode.getModuleNum());
		currNode = null;
		if(caseList.size()!=0){
			this.setRelaUser(caseList);
			this.setRelaTaskName(caseList);
			this.setRelaType(caseList);
		}
		dto.setHql(null);
		dto.setHqlParamMaps(null);
		StringBuffer sb = new StringBuffer();
		this.caseListToJson(caseList, sb);
		super.writeResult(dto.getAttr("delFlg")+"$"+sb.toString());
		return super.globalAjax();
	}
	
	
	private void setCanDelCaseId(CaseManagerDto dto,List<Long> idList ){
		String hql = "select new TestResult(resultId,testCaseId) from TestResult where testCaseId in(:ids)" ;
		Map praValuesMap = new HashMap(1);
		praValuesMap.put("ids", idList);
		List<TestResult> restList = caseService.findByHqlWithValuesMap(hql, praValuesMap, false);
		if(restList==null||restList.isEmpty()){
			dto.setAttr("delCaseIds", idList);
			dto.setAttr("delFlg", "delA");
		}else{
			List<Long> delIds = new ArrayList<Long>();
			for(Long id :idList){
				boolean canDel = true;
				for(TestResult tr :restList){
					if(id.intValue()==tr.getTestCaseId().intValue()){
						canDel = false;
						break;
					}	
				}
				if(canDel){
					delIds.add(id);
				}
			}
			if(delIds.isEmpty()){
				dto.setAttr("delCaseIds", null);
				dto.setAttr("delFlg", "noDelA");
			}else{
				dto.setAttr("delCaseIds", delIds);
				dto.setAttr("delFlg", "noDelP");
			}
		}
	}
	private void copyCasePrepare(CaseManagerDto dto,List<TestCaseInfo> initCaseList,OutlineInfo currNode ){
		List<TestCaseInfo> addCaseList = new ArrayList<TestCaseInfo>(initCaseList.size());
		List<CaseExeHistory> exeHistoryList = new ArrayList<CaseExeHistory>(initCaseList.size());
		String myId = SecurityContextHolderHelp.getUserId();
		String taskId = SecurityContextHolderHelp.getCurrTaksId();
		int testStatus = 1;
		if(!this.isReview(taskId)){
			testStatus =1;
		}else{
			testStatus =0;
		}
		for(TestCaseInfo tc :initCaseList){
			TestCaseInfo newTc = new TestCaseInfo();
			newTc.setCreatdate(new Date());
			newTc.setUpddate(newTc.getCreatdate());
			newTc.setCreaterId(myId);
			newTc.setModuleId(currNode.getModuleId());
			newTc.setModuleNum(currNode.getModuleNum());
			newTc.setCaseTypeId(tc.getCaseTypeId());
			newTc.setPriId(tc.getPriId());
			newTc.setTestCaseDes(tc.getTestCaseDes());
			newTc.setTestData(tc.getTestData());
			newTc.setOperDataRichText(tc.getOperDataRichText());
			newTc.setExpResult(tc.getExpResult());
			newTc.setAttachUrl(tc.getAttachUrl());
			newTc.setTaskId(taskId);
			newTc.setIsReleased(0);
			newTc.setTestStatus(testStatus);
			addCaseList.add(newTc);
			
			CaseExeHistory his = new CaseExeHistory();
			his.setTaskId(taskId);
			his.setOperaType(1);
			//his.setTestCaseId(testCase.getTestCaseId());
			his.setModuleId(currNode.getModuleId());
			his.setExeDate(newTc.getUpddate());
			
			his.setTestActor(myId);
			exeHistoryList.add(his);
		}
		dto.setAttr("addCaseList", addCaseList);
		dto.setAttr("exeHistoryList", exeHistoryList);
	}
	public View loadNodedetalData(BusiRequestEvent req){
		CaseManagerDto dto = super.getDto(CaseManagerDto.class, req);
		String[] idsArr =  dto.getCountStr().split("_");
		if(idsArr==null||idsArr.length==0){
			super.writeResult("");
			return super.globalAjax();
		}
		
		List<Long> nodeIds = new ArrayList<Long>();
		for(String id :idsArr){
			if(!"".equals(id)&&id!=null){
				nodeIds.add(new Long(id));
			}
		}
		if(nodeIds.isEmpty()){
			super.writeResult("");
			return super.globalAjax();
		}
		String taskId = SecurityContextHolderHelp.getCurrTaksId();
		if(taskId==null){
			super.writeResult("");
			return super.globalAjax();
		}
		boolean onlyNormal = "onlyNormal".equals(dto.getRemark())?true:false;
		List<OutlineInfo>  loadNodes  = impExpManagerService.getOutLineInfo(taskId, nodeIds, onlyNormal);
		if(loadNodes==null||loadNodes.isEmpty()){
			super.writeResult("");
			loadNodes = null;
			return super.globalAjax();
		}
		List resultSet = impExpManagerService.getOutLineDetailInfo(taskId, loadNodes, onlyNormal);
		if(resultSet==null||resultSet.isEmpty()){
			for(OutlineInfo node :loadNodes){
				//这里用来存用例数
				node.setCaseCount(0);
				//这里用来存BUG数
				node.setScrpCount(0);
			}
			StringBuffer sb = new StringBuffer();
			for (OutlineInfo outLine : loadNodes) {
				sb.append(";").append(outLine.getModuleId());
				sb.append(",").append(outLine.getCaseCount());
				sb.append(",").append(outLine.getScrpCount());
			}
			String ajaxRest = sb.length() > 2 ? sb.substring(1).toString() : "";
			super.writeResult(ajaxRest);
			loadNodes = null;
			return super.globalAjax();
		}
		
		for(OutlineInfo node :loadNodes){
			//这里用来存用例数
			node.setCaseCount(0);
			//这里用来存BUG数
			node.setScrpCount(0);
			Iterator it = resultSet.iterator();
			while (it.hasNext()) {
				Object values[] = (Object[]) it.next();

				if(node.getModuleId().toString().equals(values[0].toString())||(node.getModuleNum()!=null&&values[1]!=null&&values[1].toString().startsWith(node.getModuleNum()))){
					node.setCaseCount(node.getCaseCount()+Integer.parseInt(values[2].toString()));
					node.setScrpCount(node.getScrpCount()+Integer.parseInt(values[3].toString()));
				}else if(node.getModuleNum()==null){
					node.setCaseCount(node.getCaseCount()+Integer.parseInt(values[2].toString()));
					node.setScrpCount(node.getScrpCount()+Integer.parseInt(values[3].toString()));
				}
				
			}
			it = null;
		}
		StringBuffer sb = new StringBuffer();
		for (OutlineInfo outLine : loadNodes) {
			sb.append(";").append(outLine.getModuleId());
			sb.append(",").append(outLine.getCaseCount());
			sb.append(",").append(outLine.getScrpCount());
		}
		String ajaxRest = sb.length() > 2 ? sb.substring(1).toString() : "";
		super.writeResult(ajaxRest);
		resultSet = null;
		loadNodes = null;
		return super.globalAjax();
	}
	public View loadCaseBoard(BusiRequestEvent req){
		CaseManagerDto dto = super.getDto(CaseManagerDto.class, req);
		List<Object[]> countList = caseService.loadCaseBoard(SecurityContextHolderHelp.getCurrTaksId());
		if(countList==null||countList.isEmpty()){
			dto.setListStr("");
		}else{
			List list = new ArrayList(countList.size());
			for(Object[] objs :countList){      
				BoardVo vo = new BoardVo();
				vo.setUserName(objs[0].toString());
				vo.setWhCount(objs[1]==null?0:Integer.parseInt(objs[1].toString()));
				vo.setHCount(objs[2]==null?0:Integer.parseInt(objs[2].toString()));
				vo.setBwhCount(objs[3]==null?0:Integer.parseInt(objs[3].toString()));
				vo.setBhCount(objs[4]==null?0:Integer.parseInt(objs[4].toString()));
				vo.setLoginName(objs[5].toString());
				list.add(vo);
			}
			StringBuffer sb = new StringBuffer();
			this.boardtoJson(list, sb);
			dto.setListStr(sb.toString());
		}
		return super.getView();
	}
	public void boardtoJson(List<BoardVo> list,StringBuffer sb){
		int i =0 ;
		if(list != null && !list.isEmpty()){
			sb.append("{rows: [");
			for(BoardVo obj:list){
				i++ ;
				if(i != list.size()){
					obj.toString2(sb);
					sb.append(",");
				}else{
					obj.toString2(sb);
				}
			}
			sb.append("]}");				
		}		
	    Object pageInfo = SecurityContextHolder.getContext().getAttr("pageInfo");
	    if(pageInfo != null){
	    	 sb.insert(0, pageInfo.toString()).toString();
	    }
	}
	public View quickQuery(BusiRequestEvent req){
		CaseManagerDto dto = super.getDto(CaseManagerDto.class, req);
		TestCaseInfo testCase = caseService.quickQueryCase(dto.getTestCaseInfo().getTestCaseId());
		if(testCase==null){
			super.writeResult("");
		}else{
			SecurityContextHolderHelp.setCurrTaksId(testCase.getTaskId());
			List<TestCaseInfo> caseList = new ArrayList<TestCaseInfo>(1);
			caseList.add(testCase);
			this.setRelaUser(caseList);
			this.setRelaTaskName(caseList);
			this.setRelaType(caseList);
			StringBuffer sb = new StringBuffer();
			this.caseListToJson(caseList, sb);
			super.writeResult(sb.toString());
		}
		return super.globalAjax();
	}
	public View batchAuditInit(BusiRequestEvent req){
		CaseManagerDto dto = super.getDto(CaseManagerDto.class, req);
		Long currNodeId = dto.getCurrNodeId();
		String taskId = SecurityContextHolderHelp.getCurrTaksId();
		dto.setTaskId(taskId);
		StringBuffer hql = new StringBuffer();
		OutlineInfo outLine = null;
		if(currNodeId==null){
			String taskHql = " select new TestTaskDetail(outlineState,testPhase,currentVersion,testSeq,reltCaseFlag) from TestTaskDetail where taskId=? and companyId=?";
			List<TestTaskDetail> taskList = caseService.findByHql(taskHql, taskId,SecurityContextHolderHelp.getCompanyId());
			if(taskList==null||taskList.isEmpty()){
				dto.setListStr("1/10/0$");
				dto.setOutLineState(0);
				return super.getView();
			}
			TestTaskDetail taskDetal = taskList.get(0);
			dto.setOutLineState(taskDetal.getOutlineState());
			hql.append("select new OutlineInfo(moduleNum,moduleId) from OutlineInfo where superModuleId =0 and taskId=?");
			List<OutlineInfo> list = caseService.findByHql(hql.toString(), taskId);
			outLine = list.get(0);
		}else{
			hql.append("select new OutlineInfo(moduleNum,moduleId) from OutlineInfo where moduleId=? and taskId=?");
			List<OutlineInfo> list =caseService.findByHql(hql.toString(), currNodeId,taskId);
			outLine = list.get(0);
		}
		dto.setCurrNodeId(outLine.getModuleId());
		List caseList = caseService.loadAuditCase(dto,outLine.getModuleNum());
		if(caseList.size()!=0){
			this.setRelaUser(caseList);
			this.setRelaTaskName(caseList);
			this.setRelaType(caseList);
		}
		dto.setHql(null);
		dto.setHqlParamMaps(null);
		StringBuffer sb = new StringBuffer();
		this.caseListToJson(caseList, sb);
		if("true".equals(dto.getIsAjax())){
			super.writeResult(sb.toString());
			return super.globalAjax();
		}
		dto.setCountStr(impExpManagerService.getCaseCountStr(taskId));
		dto.setListStr(sb.toString());
		return super.getView();
	}
	public View batchAuditSub(BusiRequestEvent req){
		CaseManagerDto dto = super.getDto(CaseManagerDto.class, req);
		String taskId = SecurityContextHolderHelp.getCurrTaksId();
		dto.setTaskId(taskId);
		caseService.batchAudit(dto);
		//Long currNodeId = dto.getCurrNodeId();
		
		dto.setPageNo(1);
		return this.batchAuditInit(req);
	}
	

	public View viewCaseHistory(BusiRequestEvent req){
		
		String taskId = SecurityContextHolderHelp.getCurrTaksId();
		CaseManagerDto dto = super.getDto(CaseManagerDto.class, req);
		dto.setTaskId(taskId);
		List<CaseExeHistory> hisList = caseService.loadHistory(dto);
		List exeRecordList = hisList;
		this.setHisRealActor(hisList);
		this.setHisRealVer(hisList);
		StringBuffer sb = new StringBuffer();
		dto.toJson2(exeRecordList, sb);
		if(dto.getIsAjax()!=null){
			super.writeResult(sb.toString());
			return super.globalAjax();
		}
		dto.setListStr(sb.toString());
		return super.getView();
	}
	
	
	public View quickQueryLastExeCase(BusiRequestEvent req){
		CaseManagerDto dto = super.getDto(CaseManagerDto.class, req);
		TestCaseVo vo = caseService.quickQueryLastExeCase(dto.getTestCaseInfo().getTestCaseId());
		if(vo==null){
			super.writeResult("");
		}else{
			SecurityContextHolderHelp.setCurrTaksId(vo.getTaskId());
			List<TestCaseVo> exeList  = new ArrayList<TestCaseVo>(1);
			exeList.add(vo);
			List exeRecordList = exeList;
			this.setRealActor((List<TestCaseVo>)exeRecordList);
			this.setRealVer((List<TestCaseVo>)exeRecordList);
			this.setHisRelaTaskName(exeList);
			StringBuffer sb = new StringBuffer();
			dto.toJson2(exeRecordList, sb);
			super.writeResult(sb.toString());
		}
		return super.globalAjax();
	}	
	
	public View loadTree(BusiRequestEvent req) {
		CaseManagerDto dto = super.getDto(CaseManagerDto.class, req);
		String taskId = SecurityContextHolderHelp.getCurrTaksId();
		dto.setTaskId(taskId);
		if(taskId==null){
			if(dto.getIsAjax() != null){
				super.writeResult("0,1,无数据,0,1");
				dto = null;
				return super.globalAjax();
			}
			SecurityContextHolder.getContext().setAttr("nodeDataStr", "0,1,无数据,0,1");
			return super.getView();
		}
		String nodeDataStr = this.toTreeStr(outLineService.loadNormalNode(taskId, dto.getCurrNodeId()));
		if (dto.getIsAjax() == null) {
			SecurityContextHolder.getContext().setAttr("nodeDataStr", nodeDataStr);
			return super.getView();
		}
		super.writeResult(nodeDataStr);
		dto = null;
		return super.globalAjax();
	}

	public View index(BusiRequestEvent req){
		CaseManagerDto dto = super.getDto(CaseManagerDto.class, req);
		String taskId = dto.getTaskId();
		//System.out.println("case ==========index");
		if(taskId!=null){  
			StringBuffer hql = new StringBuffer("select new TestTaskDetail(outlineState,testPhase,")
			.append("currentVersion,testSeq,reltCaseFlag) from TestTaskDetail where taskId=? and companyId=?");
			List<TestTaskDetail> taskList =testTaskService.findByHql(hql.toString(), dto.getTaskId(),SecurityContextHolderHelp.getCompanyId());
			if(taskList==null||taskList.isEmpty()){
				throw new DataBaseException("非法提交的测试任务数据，不受理");
			}
			SecurityContextHolderHelp.setCurrTaksId(taskId);
			dto.setTaskId(taskId);
		}
		return super.getView();
	}
	public View getCaseStatInfo(BusiRequestEvent req){
		CaseManagerDto dto = super.getDto(CaseManagerDto.class, req);
		Long currNodeId = dto.getCurrNodeId();
		String hql = "select new OutlineInfo(moduleNum,moduleId) from OutlineInfo where moduleId=? and taskId=?";
		String taskId = SecurityContextHolderHelp.getCurrTaksId();
		List<OutlineInfo> list =caseService.findByHql(hql, currNodeId,taskId);
		if(list==null||list.isEmpty()){
			super.writeResult("测试需求不存在");
			return super.globalAjax();
		}
		OutlineInfo outLine = list.get(0);
		if(outLine.getModuleNum()!=null){
			super.writeResult(impExpManagerService.getCaseCountStr(taskId,outLine.getModuleNum()));
		}else{
			super.writeResult(impExpManagerService.getCaseCountStr(taskId));
		}
		return super.globalAjax();
	}
	public View  loadCase(BusiRequestEvent req){
		CaseManagerDto dto = super.getDto(CaseManagerDto.class, req);
		Long currNodeId = dto.getCurrNodeId();
		String taskId = dto.getTaskId();
		if(taskId==null){
			taskId = SecurityContextHolderHelp.getCurrTaksId();
			dto.setTaskId(taskId);
		}else{
			String taskHql = " select new TestTaskDetail(outlineState,testPhase,currentVersion,testSeq,reltCaseFlag) from TestTaskDetail where taskId=? and companyId=?";
			List<TestTaskDetail> taskList = caseService.findByHql(taskHql, taskId,SecurityContextHolderHelp.getCompanyId());
			if(taskList==null||taskList.size()==0){
				if(dto.getIsAjax()!=null){
					super.writeResult("1/10/0$");
					return super.globalAjax();
				}
				dto.setOutLineState(0);
				dto.setListStr("1/10/0$");
				return super.getView();
			}
			SecurityContextHolderHelp.setCurrTaksId(taskId);
		}
		dto.setIsReview(0);
		StringBuffer hql = new StringBuffer();
		OutlineInfo outLine = null;
		if(taskId==null){
			dto.setListStr("1/10/0$");
			dto.setOutLineState(0);
			return super.getView();
		}

		if(currNodeId==null){
			String taskHql = " select new TestTaskDetail(outlineState,testPhase,currentVersion,testSeq,reltCaseFlag) from TestTaskDetail where taskId=? and companyId=?";
			List<TestTaskDetail> taskList = caseService.findByHql(taskHql, taskId,SecurityContextHolderHelp.getCompanyId());
			if(taskList==null||taskList.isEmpty()){
				dto.setListStr("1/10/0$");
				dto.setOutLineState(0);
				return super.getView();
			}
			TestTaskDetail taskDetal = taskList.get(0);
			dto.setOutLineState(taskDetal.getOutlineState());
			hql.append("select new OutlineInfo(moduleNum,moduleId) from OutlineInfo where superModuleId =0 and taskId=?");
			List<OutlineInfo> list = caseService.findByHql(hql.toString(), taskId);
			if(list==null||list.isEmpty()){
				dto.setListStr("1/10/0$");
				return super.getView();
			}
			outLine = list.get(0);
		}else{
			hql.append("select new OutlineInfo(moduleNum,moduleId) from OutlineInfo where moduleId=? and taskId=?");
			List<OutlineInfo> list =caseService.findByHql(hql.toString(), currNodeId,taskId);
			if(list==null||list.isEmpty()){
				dto.setListStr("1/10/0$");
				return super.getView();
			}
			outLine = list.get(0);
		}
		dto.setCurrNodeId(outLine.getModuleId());
		List caseList = caseService.loadCase(dto,outLine.getModuleNum());
		if(caseList.size()!=0){
			this.setRelaUser(caseList);
			this.setRelaTaskName(caseList);
			this.setRelaType(caseList);
		}
		dto.setHql(null);
		dto.setHqlParamMaps(null);
		StringBuffer sb = new StringBuffer();
		this.caseListToJson(caseList, sb);
		if("true".equals(dto.getIsAjax())){
			super.writeResult(sb.toString());
			return super.globalAjax();
		}
		//设置用户能否进行用例审核权限
		if((!"true".equals(dto.getIsAjax()))&&dto.getCanReview()==1&&this.isReview(taskId)){
			dto.setIsReview(1);
		}
		dto.setCountStr(impExpManagerService.getCaseCountStr(taskId));
		dto.setListStr(sb.toString());
		return super.getView("loadCase");
	}
	
	private void setRelaType(List<TestCaseInfo> caseList){
		 Map<String,TypeDefine> typeMap= caseService.getRelaTypeDefine(caseList, "priId","caseTypeId");
		 if(typeMap.isEmpty()){
			 return;
		 }
		 for(TestCaseInfo testCase:caseList ){
			 if(typeMap.get(testCase.getCaseTypeId().toString())!=null){
				 testCase.setTypeName(typeMap.get(testCase.getCaseTypeId().toString()).getTypeName());
			 }
			 if(typeMap.get(testCase.getPriId().toString())!=null){
				 testCase.setPriName(typeMap.get(testCase.getPriId().toString()).getTypeName());
			 }
		 }
		 typeMap = null;
	}
	private void setRelaUser(List<TestCaseInfo> caseList){
		Map<String,User> userMap= caseService.getRelaUserWithName(caseList, "createrId","auditId");
		User own = null;
		for(TestCaseInfo testCase :caseList){
			own = userMap.get(testCase.getCreaterId());
			if(own!=null)
			 testCase.setAuthorName(own.getUniqueName());
			if(testCase.getAuditId()!=null){
				own = userMap.get(testCase.getAuditId());
				if(own==null){
					continue;
				}
				testCase.setAuditerNmae(own.getUniqueName());					
			}
			own = null;
		}
		own= null;
		userMap = null;
	}
	
	private void  caseListToJson(List<TestCaseInfo> list,StringBuffer sb){
		int i =0 ;
		if(list != null && list.size()>0){
			sb.append("{rows: [");
			for(JsonInterface obj:list){
				i++ ;
				if(i != list.size()){
					obj.toString(sb);
					sb.append(",");
				}else{
					obj.toString(sb);
				}
			}
			sb.append("]}");				
		}		
	    Object pageInfo = SecurityContextHolder.getContext().getAttr("pageInfo");
	    if(pageInfo != null){
	    	 sb.insert(0, pageInfo.toString());
	    }
	}
	public View logicDel(BusiRequestEvent req){
		CaseManagerDto dto = super.getDto(CaseManagerDto.class, req);
		dto.setIsAjax("true");
		caseService.exeCase(dto);
		super.writeResult("success");
		dto = null;
		return super.globalAjax();
	}
	public View exeCase(BusiRequestEvent req){
		CaseManagerDto dto = super.getDto(CaseManagerDto.class, req);
		dto.setIsAjax("true");
		caseService.exeCase(dto);
		super.writeResult("success");
		dto = null;
		return super.globalAjax();
	}
	public View auditCase(BusiRequestEvent req){
		CaseManagerDto dto = super.getDto(CaseManagerDto.class, req);
		dto.setIsAjax("true");
		caseService.exeCase(dto);
		super.writeResult("success");
		dto = null;
		return super.globalAjax();
	}
	public View blockCase(BusiRequestEvent req){
		CaseManagerDto dto = super.getDto(CaseManagerDto.class, req);
		dto.setIsAjax("true");
		caseService.exeCase(dto);
		super.writeResult("success");
		dto = null;
		return super.globalAjax();
	}
	public View addInit(BusiRequestEvent req){
		CaseManagerDto dto = super.getDto(CaseManagerDto.class, req);
		List<ListObject> typeList = drawHtmlListDateService.getTypeDefine("CaseType");
		List<ListObject> casePriList = drawHtmlListDateService.getTypeDefine("CasePri");
		TestCaseInfo testCaseInfo = new TestCaseInfo();
		dto.setTestCaseInfo(testCaseInfo);
		dto.getTestCaseInfo().setTypeSelStr(HtmlListComponent.toSelectStr(typeList,"$"));
		dto.getTestCaseInfo().setPriSelStr(HtmlListComponent.toSelectStr(casePriList,"$"));
		dto.getTestCaseInfo().setCreaterId(SecurityContextHolderHelp.getUserId());
		dto.getTestCaseInfo().setIsReleased(0);
		dto.getTestCaseInfo().setModuleId(dto.getCurrNodeId());
		String taskId = SecurityContextHolderHelp.getCurrTaksId();
		dto.setTaskId(taskId);
		dto.getTestCaseInfo().setTaskId(dto.getTaskId());
		dto.getTestCaseInfo().setPriId(new Long(-1));
		dto.getTestCaseInfo().setCaseTypeId(new Long(-1));
		dto.getTestCaseInfo().setExpResult("可在过程及数据中用表格形式写预期结果,或单独在此填写");
		String outlineHql = "select new OutlineInfo(moduleId,moduleLevel,moduleNum) from OutlineInfo where taskId=? and moduleId=?";
		OutlineInfo outline = (OutlineInfo)testTaskService.findByHql(outlineHql, taskId,dto.getCurrNodeId()).get(0);
		dto.getTestCaseInfo().setModuleNum(outline.getModuleNum());
		if(!this.isReview(taskId)){
			dto.getTestCaseInfo().setTestStatus(1);
		}else{
			dto.getTestCaseInfo().setTestStatus(0);
		}
		dto.getTestCaseInfo().setRemark(null);
		dto.getTestCaseInfo().setAttachUrl(null);
		dto.getTestCaseInfo().setTestData(null);
		dto.getTestCaseInfo().setTestCaseId(null);
		dto.getTestCaseInfo().setIsReleased(0);
		dto.getTestCaseInfo().setCreaterId(SecurityContextHolderHelp.getUserId());
		if("true".equals(dto.getIsAjax())){
			super.writeResult(dto.getTestCaseInfo().toStrUpdateInit());
			dto = null;
			return super.globalAjax();
		}
		return super.getView("addInit");
	}
	
	public View lastExeCase(BusiRequestEvent req){
		
		CaseManagerDto dto = super.getDto(CaseManagerDto.class, req);
		//作下面的检查，是为了防止前端篡改taskId
		if(dto.getTaskId()!=null&&!"".equals(dto.getTaskId().trim())){
			StringBuffer hql = new StringBuffer("select new TestTaskDetail(outlineState,testPhase,")
			.append("currentVersion,testSeq,reltCaseFlag) from TestTaskDetail where taskId=? and companyId=?");
			List<TestTaskDetail> taskList =testTaskService.findByHql(hql.toString(), dto.getTaskId(),SecurityContextHolderHelp.getCompanyId());
			if(taskList==null||taskList.size()==0){
				if(dto.getIsAjax()!=null){
					super.writeResult("1/10/0$");
					return super.globalAjax();
				}
				dto.setOutLineState(0);
				dto.setListStr("1/10/0$");
				return super.getView();
			}
		}
		List<TestCaseVo> exeList  = caseService.loadLastExeCase(dto);
		List exeRecordList = exeList;
		this.setRealActor((List<TestCaseVo>)exeRecordList);
		this.setRealVer((List<TestCaseVo>)exeRecordList);
		this.setHisRelaTaskName(exeList);
		StringBuffer sb = new StringBuffer();
		dto.toJson2(exeRecordList, sb);
		if(dto.getIsAjax()!=null){
			super.writeResult(sb.toString());
			return super.globalAjax();
		}
		dto.setListStr(sb.toString());
		return super.getView();
	}

	public View viewHistory(BusiRequestEvent req){
		String taskId = SecurityContextHolderHelp.getCurrTaksId();
		CaseManagerDto dto = super.getDto(CaseManagerDto.class, req);
		dto.setTaskId(taskId);
		List<TestCaseVo> exeList = caseService.loadExeRecord(dto);
		List exeRecordList = exeList;
		this.setRealActor((List<TestCaseVo>)exeRecordList);
		this.setRealVer((List<TestCaseVo>)exeRecordList);
		this.setHisRelaTaskName(exeList);
		StringBuffer sb = new StringBuffer();
		dto.toJson2(exeRecordList, sb);
		if(dto.getIsAjax()!=null){
			super.writeResult(sb.toString());
			return super.globalAjax();
		}
		dto.setListStr(sb.toString());
		return super.getView();
	}
	
	private void setRelaTaskName(List<TestCaseInfo> testCases){
		if(testCases==null||testCases.isEmpty()){
			return;
		}
		Map<String,ListObject> taskMap= caseService.getRelaTestTasks(testCases, "taskId");
		ListObject lstObj = null;
		for(TestCaseInfo testCase :testCases){
			lstObj = taskMap.get(testCase.getTaskId());
			if(lstObj==null){
				continue;
			}
			testCase.setTaskName(lstObj.getValueObj());
		}
		lstObj= null;
		taskMap = null;
	}
	private void setHisRelaTaskName(List<TestCaseVo> testCases){
		if(testCases==null||testCases.isEmpty()){
			return;
		}
		Map<String,ListObject> taskMap= caseService.getRelaTestTasks(testCases, "taskId");
		ListObject lstObj = null;
		for(TestCaseVo testCase :testCases){
			lstObj = taskMap.get(testCase.getTaskId());
			if(lstObj==null){
				continue;
			}
			testCase.setTaskName(lstObj.getValueObj());
		}
		lstObj= null;
		taskMap = null;
	}
	private void setRealVer(List<TestCaseVo> exeRecordList){
		if(exeRecordList==null||exeRecordList.isEmpty()){
			return;
		}
		Map<String,SoftwareVersion> verMap= caseService.getRelaVers(exeRecordList, "testVer");
		SoftwareVersion ver = null;
		for(TestCaseVo tcVo :exeRecordList){
			if(tcVo.getTestVer()!=null){
				ver = verMap.get(tcVo.getTestVer().toString());
				if(ver==null){
					continue;
				}
				tcVo.setTestVerName(ver.getVersionNum());
				ver = null;
			}
		}
		ver= null;
		verMap = null;		
	}
	private void setHisRealVer(List<CaseExeHistory> exeRecordList){
		if(exeRecordList==null||exeRecordList.isEmpty()){
			return;
		}
		Map<String,SoftwareVersion> verMap= caseService.getRelaVers(exeRecordList, "testVer");
		SoftwareVersion ver = null;
		for(CaseExeHistory his :exeRecordList){
			if(his.getTestVer()!=null){
				ver = verMap.get(his.getTestVer().toString());
				if(ver==null){
					continue;
				}
				his.setTestVerNum(ver.getVersionNum());
				ver = null;
			}
		}
		ver= null;
		verMap = null;		
	}
	private void setRealActor(List<TestCaseVo> exeRecordList){
		if(exeRecordList==null||exeRecordList.isEmpty()){
			return;
		}
		Map<String,User> userMap= caseService.getRelaUserWithName(exeRecordList, "testActor");
		User own = null;
		for(TestCaseVo tcVo :exeRecordList){
			if(tcVo.getTestActor()!=null){
				own = userMap.get(tcVo.getTestActor());
				if(own==null){
					continue;
				}
				tcVo.setTestActorName(own.getUniqueName());
				own = null;
			}
		}
		own= null;
		userMap = null;
	}
	private void setHisRealActor(List<CaseExeHistory> exeRecordList){
		if(exeRecordList==null||exeRecordList.isEmpty()){
			return;
		}
		Map<String,User> userMap= caseService.getRelaUserWithName(exeRecordList, "testActor");
		User own = null;
		for(CaseExeHistory his :exeRecordList){
			if(his.getTestActor()!=null){
				own = userMap.get(his.getTestActor());
				if(own==null){
					continue;
				}
				his.setTestActorName(own.getUniqueName());
				own = null;
			}
		}
		own= null;
		userMap = null;
	}
	private boolean isReview(String taskId){
		String flwHql = "select new TestFlowInfo(testFlowCode) from TestFlowInfo where taskId=? and testFlowCode=9";
		List flwList = caseService.findByHql(flwHql, taskId);
		if(flwList==null||flwList.size()==0){
			return false;
		}else{
			return true;
		}
		
	}
	public View initDropList(BusiRequestEvent req){
		List<ListObject> typeList = drawHtmlListDateService.getTypeDefine("CaseType");
		List<ListObject> casePriList = drawHtmlListDateService.getTypeDefine("CasePri");
		List<List<ListObject>> list = new ArrayList<List<ListObject>>();
		list.add(typeList);
		list.add(casePriList);
		super.writeResult("success"+HtmlListComponent.toSelectStrWithBreak(list));
		return super.globalAjax();
	}
	
	public View dropListWithVer(BusiRequestEvent req){
		List<ListObject> typeList = drawHtmlListDateService.getTypeDefine("CaseType");
		List<ListObject> casePriList = drawHtmlListDateService.getTypeDefine("CasePri");
		List<List<ListObject>> list = new ArrayList<List<ListObject>>();
		list.add(typeList);
		list.add(casePriList);
		if(SecurityContextHolderHelp.getCurrTaksId()!=null){
			StringBuffer hql = new StringBuffer();
			hql.append("select new cn.com.mypm.framework.common.ListObject(");
			hql.append(" versionId as keyObj ,versionNum as valueObj ) from SoftwareVersion ")
			.append(" where taskid=? and verStatus=0 order by seq desc ");
			List<ListObject> listDates  = testTaskService.findByHql(hql.toString(), SecurityContextHolderHelp.getCurrTaksId());
			list.add(listDates);
		}
		super.writeResult("success"+HtmlListComponent.toSelectStrWithBreak(list));
		return super.globalAjax();
	}

	public View upInit(BusiRequestEvent req){
		CaseManagerDto dto = super.getDto(CaseManagerDto.class, req);
		String hql="from TestCaseInfo where testCaseId=? and taskId=?";
		String taskId = dto.getTaskId();
		if(taskId!=null&&!"".equals(taskId)){
			
		}else{
			taskId = SecurityContextHolderHelp.getCurrTaksId();
		}
		
		Long caseId =  dto.getTestCaseInfo().getTestCaseId();
		List list = caseService.findByHql(hql,caseId,taskId);
		if("true".equals(dto.getIsAjax())){
			if(list==null||list.size()==0){
				super.writeResult("failed^当前记录己被删除");
				return super.globalAjax();
			} 
			SecurityContextHolderHelp.setCurrTaksId(taskId);
			TestCaseInfo testCase = (TestCaseInfo)list.get(0);
			List<ListObject> typeList = drawHtmlListDateService.getTypeDefine("CaseType");
			List<ListObject> casePriList = drawHtmlListDateService.getTypeDefine("CasePri");
			testCase.setTypeSelStr(HtmlListComponent.toSelectStr(typeList,"$"));
			testCase.setPriSelStr(HtmlListComponent.toSelectStr(casePriList,"$"));
			super.writeResult(testCase.toStrUpdateInit());
			return super.globalAjax();
		}
		return super.getView();
	}
	
	public View viewDetal(BusiRequestEvent req){
		CaseManagerDto dto = super.getDto(CaseManagerDto.class, req);
		String hql="from TestCaseInfo where testCaseId=? and taskId=?";
		String taskId = dto.getTaskId();
		if(taskId!=null&&!"".equals(taskId)){ 
		}else{
			taskId = SecurityContextHolderHelp.getCurrTaksId();
		}
		
		Long caseId =  dto.getTestCaseInfo().getTestCaseId();
		List list = caseService.findByHql(hql,caseId,taskId);
		if("true".equals(dto.getIsAjax())){
			if(list==null||list.size()==0){
				super.writeResult("failed^当前记录己被删除");
				return super.globalAjax();
			} 
			SecurityContextHolderHelp.setCurrTaksId(taskId);
			TestCaseInfo testCase = (TestCaseInfo)list.get(0);
			List<ListObject> typeList = drawHtmlListDateService.getTypeDefine("CaseType");
			List<ListObject> casePriList = drawHtmlListDateService.getTypeDefine("CasePri");
			testCase.setTypeSelStr(HtmlListComponent.toSelectStr(typeList,"$"));
			testCase.setPriSelStr(HtmlListComponent.toSelectStr(casePriList,"$"));
			super.writeResult(testCase.toStrUpdateInit());
			return super.globalAjax();
		}
		return super.getView();
	}
	public View exeCaseInit(BusiRequestEvent req){
		CaseManagerDto dto = super.getDto(CaseManagerDto.class, req);
		String hql="from TestCaseInfo where testCaseId=? and taskId=?";
		String taskId = dto.getTaskId();
		if(taskId!=null&&!"".equals(taskId)){
			
		}else{
			taskId = SecurityContextHolderHelp.getCurrTaksId();
		}
		Long caseId =  dto.getTestCaseInfo().getTestCaseId();
		List list = caseService.findByHql(hql,caseId,taskId);
		if("true".equals(dto.getIsAjax())){
			if(list.size()<0){
				super.writeResult("failed^当前记录己被删除");
				return super.globalAjax();
			} 
			SecurityContextHolderHelp.setCurrTaksId(taskId);
			TestCaseInfo testCase = (TestCaseInfo)list.get(0);
			List<ListObject> typeList = drawHtmlListDateService.getTypeDefine("CaseType");
			List<ListObject> casePriList = drawHtmlListDateService.getTypeDefine("CasePri");
			testCase.setTypeSelStr(HtmlListComponent.toSelectStr(typeList,"$"));
			testCase.setPriSelStr(HtmlListComponent.toSelectStr(casePriList,"$"));
			super.writeResult(testCase.toStrUpdateInit());
			return super.globalAjax();
		}
		return super.getView();
	}
	public View addCase(BusiRequestEvent req){
		CaseManagerDto dto = super.getDto(CaseManagerDto.class, req);
		caseService.addOrUpCase(dto);
		List<TestCaseInfo> caseList = new ArrayList<TestCaseInfo>(1) ;
		caseList.add(dto.getTestCaseInfo());
		this.setRelaTaskName(caseList);
		super.writeResult("success^"+dto.getTestCaseInfo().toStrUpdateRest());
		return super.globalAjax();
	}
	public View upCase(BusiRequestEvent req){
		CaseManagerDto dto = super.getDto(CaseManagerDto.class, req);
		caseService.addOrUpCase(dto);
		List<TestCaseInfo> caseList = new ArrayList<TestCaseInfo>(1) ;
		caseList.add(dto.getTestCaseInfo());
		this.setRelaTaskName(caseList);
		super.writeResult("success^"+dto.getTestCaseInfo().toStrUpdateRest());
		dto = null;
		return super.globalAjax();
	}	
	
	public View delCase(BusiRequestEvent req){
		CaseManagerDto dto = super.getDto(CaseManagerDto.class, req);
		Long caseId = dto.getTestCaseInfo().getTestCaseId();
		if(caseService.isCanDel(caseId)){
			caseService.delCase(caseId);
			super.writeResult("success");
			dto = null;
			return super.globalAjax();
		}else{
			super.writeResult("failed^该用例有测试记录不能删除");
			dto = null;
			return super.globalAjax();
		}
	}
	private String toTreeStr(List<OutlineInfo> list) {
		StringBuffer sb = new StringBuffer();
		for (OutlineInfo outLine : list) {
			sb.append(";").append(outLine.getSuperModuleId());
			sb.append(",").append(outLine.getModuleId());
			sb.append(",").append(outLine.getModuleName());
			sb.append(",").append(outLine.getIsleafNode());
			sb.append(",").append(outLine.getModuleState());
		}
		return sb.length() > 2 ? sb.substring(1).toString() : "";
	}

	public OutLineManagerService getOutLineService() {
		return outLineService;
	}

	public void setOutLineService(OutLineManagerService outLineService) {
		this.outLineService = outLineService;
	}

	public CaseManagerService getCaseService() {
		return caseService;
	}

	public void setCaseService(CaseManagerService caseService) {
		this.caseService = caseService;
	}

	public DrawHtmlListDateService getDrawHtmlListDateService() {
		return drawHtmlListDateService;
	}

	public void setDrawHtmlListDateService(
			DrawHtmlListDateService drawHtmlListDateService) {
		this.drawHtmlListDateService = drawHtmlListDateService;
	}

	public TestTaskDetailService getTestTaskService() {
		return testTaskService;
	}

	public void setTestTaskService(TestTaskDetailService testTaskService) {
		this.testTaskService = testTaskService;
	}
	
	public ImpExpManagerService getImpExpManagerService() {
		return impExpManagerService;
	}

	public void setImpExpManagerService(ImpExpManagerService impExpManagerService) {
		this.impExpManagerService = impExpManagerService;
	}

	

}
