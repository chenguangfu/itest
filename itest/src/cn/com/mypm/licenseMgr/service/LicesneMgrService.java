package cn.com.mypm.licenseMgr.service;

import cn.com.mypm.framework.app.services.BaseService;
import cn.com.mypm.licenseMgr.dto.LicesneMgrDto;

public interface LicesneMgrService extends BaseService {

	public void regReptLicense(LicesneMgrDto dto);

}
