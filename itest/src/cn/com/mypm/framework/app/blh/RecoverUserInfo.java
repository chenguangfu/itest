package cn.com.mypm.framework.app.blh;

import cn.com.mypm.framework.exception.BaseException;
import cn.com.mypm.framework.transmission.events.RequestEvent;

public interface RecoverUserInfo {

	public void recoverUserInfo(RequestEvent req) throws BaseException ;
	public void recoverUserInfo(String userId)  ;
}
