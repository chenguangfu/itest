package cn.com.mypm.framework.app.blh;

import cn.com.mypm.framework.common.util.Context;
import cn.com.mypm.framework.exception.BaseException;
import cn.com.mypm.framework.hibernate.HibernateGenericController;
import cn.com.mypm.framework.transmission.events.RequestEvent;
import cn.com.mypm.framework.transmission.events.ResponseEvent;

public abstract class BaseBizLogicHandler {

	
	public BaseBizLogicHandler() {

	}

	public abstract ResponseEvent performTask(RequestEvent requestevent)
			throws BaseException;

}
